# MathSTIC Latex Template Improved

Here is an improved version of the on-going [latex template made by the MathSTIC doctoral school](https://ed-mathstic.u-bretagneloire.fr/fr/8_documents-de-reference).

For a summary of the global latex template improvement, see the commit 5ad704062ed07bffdc5cf692a80c18c3d3fc1527.
Most of the improvements were inspired by [this blog post](https://www.semipol.de/2018/06/12/latex-best-practices.html).
I've also tried to remove most of the latex warnings to produce a clean compilation output from latexmk.

## Instructions for Using the Template

Here are the step by steps instructions to start and modify the template to make it yours.

__IMPORTANT__:

If you are writing your thesis in French and continue to use the cleveref and varioref packages, label names for cross-referencing sections and figures should not use a colon `:` but a dash instead: `\vref{fig-myfig}`.
If you do, you will encounter lots of errors about `Missing \endcsname inserted`.
See this [stackoverflow link](https://tex.stackexchange.com/questions/83798/cleveref-varioref-missing-endcsname-inserted) for an explanation why.

### main.tex

[main.tex](./main.tex) is the root latex file that contains the skeleton of the template.
In this file, you will have to:

* Add your custom bibliographical resources: `\addbibresource{./biblio/mybiblio.bib}`
    Of course, you will need to add your .bib file in the [biblio](./biblio) directory.
    One particularity of this template is that you have two bibliographical section at the end of the thesis: primary and secondary.
    For now, you can add your own papers as primary citations by changing the author match field in `\DeclareSourcemap` command.
    You can also add the `primary` keyword in your `keywords` field in your .bib file but that requires manually editing your .bib file.
* As you add chapters in your thesis, you can add them using the `\input` command.

### pagedegarde.tex

[pagedegarde.tex](./Couverture-these/pagedegarde.tex) will generate your cover page.
You will need to update various informations like your university logo/name, thesis title, authors...

### acknowledgement.tex

Add your acknowledgements to [acknowledgement.tex](Acknowledgement/acknowledgement.tex).

### Chapters

Each chapters has their own sub-folders which contains both latex code and resources like images for figures.

### resume.tex

[resume.tex](Couverture-these/resume.tex) contains the thesis summary both in English and French.
Don't forget to update the logo of your institution.

## General Recommendations

Read [this blog post](https://www.semipol.de/2018/06/12/latex-best-practices.html).
It contains excellent recommendations to write up a beautiful thesis.

* Bibliography:
    * Generate your citations for the biber backend
    * Use `\autocite` or `\textcite` instead of just `\cite`
* Cross-referencing:
    * Use `\vref` or `\Vref` command
    * If you are writing your thesis in French, labels should use a dash instead of a colon: `\label{fig-myfig}`
* Languages:
    * The thesis can use French or English
    * Use `\selectlanguage` command to switch between English and French

## Makefile

* Use `make` to build the thesis.
* Use `make continuous` to automatically rebuild the thesis after changes.
* Use `make chktex` to find typographic errors.
* Use `make textidote_report.html` to check grammar and style
    * Install [textidote](https://github.com/sylvainhalle/textidote)
    * You can change the checked language on the command line using: 
    make LANG=en textidote_report.html
    * Be aware that this tool is very heavy on cpu and memory usage.
    Do not run on the original template as it will generate a truck-load of errors on the lorem ipsum text.
    * When using the \input{} command in latex, make sure you specify the tex filename with the .tex extension like this: \input{Chapitre1/chapitre1.tex}
* Use `make git-latexdiff old=OLD_COMMIT new=NEW_COMMIT` to produce a pdf diff between two commits.
    * Install [git-latexdiff](https://gitlab.com/git-latexdiff/git-latexdiff)
    * See `git-latexdiff --help` for more information about how to specify commits
