\chapter*{Résumé en français}
\label{chap:résumé}
\addcontentsline{toc}{chapter}{Résumé en français}
\chaptermark{Résumé en français}

\section*{Introduction}
\label{sec:résumé:introduction}

% partition de musique + OMR
Une partition musicale est un type spécial de document permettant de retranscrire par écrit une chanson, une mélodie ou une musique.
La reconnaissance et le traitement automatique de telles images de documents est appelé la reconnaissance optique de la musique (Optical Music Recognition ou OMR).
Ce sous-domaine de la reconnaissance de document a été le sujet de travaux de recherche depuis les années 60 avec le travail de \textcite{pruslin_automatic_1966} et a récemment vu un intérêt renouvelé à cause d'avancées en matière de vision par ordinateur principalement dans le domaine du Deep Learning.

% étape de l'OMR
Le processus de reconnaissance d'une partition musicale, en partant d'une image pour arriver à l'extraction d'informations musicales de haut niveau comme la hauteur et la durée des notes de musique, est composé de multiples étapes successives.
Traditionnellement, les états de l'art comme celui de \textcite{fornes_analysis_2014} distinguent trois étapes principales:

\begin{enumerate}
  \item le prétraitement des images de partitions musicales;
  \item la localisation et la reconnaissance des symboles musicaux;
  \item la reconstruction de la notation musicale.
\end{enumerate}

% pourquoi travailler sur la détection de symboles
Dans ce travail, nous nous intéressons à la détection de symboles musicaux, étape charnière intervenant avant la reconstruction de la notation musicale, dans le but de localiser précisément et de reconnaitre la nature ou la classe des symboles.
Cette étape de détection est d'autant plus importante que la reconstruction de la notation musicale utilise la position relative d'un symbole par rapport à un autre pour en déduire leur relation.
De plus, cette étape de détection de symboles musicaux étant une tâche principalement graphique, elle a le plus à gagner des dernières avancées de Deep Learning dans le domaine de la vision par ordinateur.

% pourquoi travailler sur des partitions imprimées historiques
Le type de partition sur lequel nous allons principalement nous intéresser dans ce travail sont les partitions imprimées historiques allant du XVIII\ieme au XX\ieme siècle.
Nous nous intéressons plus particulièrement à ces partitions car elles présentent souvent une grande complexité dans l'utilisation de la notation musicale moderne avec parfois une grande densité de symboles dans des espaces très restreints.
Ces documents ont aussi la caractéristique d'avoir été imprimés en utilisant un processus de gravure manuelle sur plaque de cuivre présentant des défauts de placements uniques à ces documents.
Enfin, les partitions historiques ont souvent des défauts et du bruit dûs à l'âge de la partition et à la manière de numériser l'image de la partition.
Toutes ces caractéristiques contribuent à des difficultés de segmentation car beaucoup de symboles musicaux se touchent et présentent des défauts de formes et de bruits.
Beaucoup de travaux de recherche en OMR se sont concentrés sur la reconnaissance de partitions manuscrites et imprimées par ordinateur mais assez peu sur des partitions historiques imprimées.

C'est pourquoi nous nous proposons d'étudier dans ce manuscrit la détection de symboles musicaux dans des partitions historiques imprimées denses, bruitées et complexes.
Dans un premier temps, nous étudierons la détection de symboles musicaux avec des modèles de Deep Learning supervisés de détection.
Nous proposerons une nouvelle architecture de détection basée sur le Spatial Transformer mieux adapté à certaines situations de détection contrainte et nous comparerons cette nouvelle approche aux modèles de détection de l'état de l'art du Deep Learning.

L'utilisation de modèle de Deep Learning nécessite une large quantité de données manuellement annotées et dans le cadre de partitions historiques imprimées, il n'existe pas de jeu de données pour la détection de symboles antérieur à nos travaux.
C'est pourquoi, dans un premier temps, nous proposons un nouveau jeu de données de détection de symboles musicaux et, dans un second temps, nous présenterons une nouvelle méthode de détection de symboles musicaux non supervisée utilisant uniquement des symboles isolés comme source d'information.

\section*{Détection supervisée de symboles musicaux}
\label{sec:résumé:détection}

Dans cette première partie, nous explorons la détection supervisée de symboles musicaux car cette étape est centrale dans le processus de reconnaissance d'une partition musicale.
Pour cela, nous nous intégrons dans une méthode préexistante de reconnaissance de partition musicale appelée DMOS \autocite{couasnon_dmos_2001} basée sur une méthode syntaxique permettant de décrire la notation musicale moderne.
En utilisant cette approche hybride de méthode syntaxique et de Deep Learning, nous pouvons diriger l'utilisation de modèles de Deep Learning pour la détection de symboles en contexte.
L'utilisation du contexte nous permet de focaliser l'utilisation des modèles de détection dans des régions particulières pour un but précis comme la détection d'altérations, symboles utilisés dans la notation musicale à gauche des notes musicales pour modifier leur hauteur de note.
Nous utilisons cette tâche de détection d'altérations comme un cas d'étude pour le développement de notre méthode bien que celle-ci puisse s'appliquer sur d'autres symboles musicaux.

Pour cette tâche de détection d'altérations, nous avons construit un nouveau jeu de données de détection d'altération avec l'information de la boîte englobante et de la classe de chaque altération.
Ce jeu de données étant assez restreint, nous proposons d'utiliser une méthode d'augmentation de données en bougeant aléatoirement la région présentant le symbole à détecter.
Cela a pour effet de changer de manière aléatoire la position du symbole à détecter dans l'image.
Cette méthode d'augmentation de données, en soi très simple, nous a permis d'améliorer radicalement les résultats de détection, surtout avec le nouveau modèle de détection que nous présentons maintenant.

Notre nouveau modèle de détection est basé sur le Spatial Transformer \autocite{jaderberg_spatial_2015}.
Le Spatial Transformer est composé de deux parties: un réseau de localisation et un réseau de classification.
L'intention originelle de ce modèle de classification d'image est d'améliorer les résultats de classification en permettant au modèle d'avoir un mécanisme autosupervisé de localisation et d'isolation des zones intéressantes pour la tâche de classification.
Pour notre tâche de détection, nous détournons cette localisation autosupervisée et utilisons une fonction d'apprentissage multitâche permettant l'apprentissage simultané de la localisation et de la classification.
La localisation est apprise en deux étapes successives permettant à la première localisation de garder suffisamment d'information contextuelle pour la tâche de classification et la seconde localisation de produire une boîte englobante serrée autour du symbole à détecter.

Nous comparons notre nouvelle approche à trois différents détecteurs de l'état de l'art du Deep Learning: le Faster R-CNN, R-FCN et SSD ayant chacun des compromis différents de précision de détection et de rapidité de calcul.
Les résultats détaillés sont présentés dans le \cref{tab\string:detect_comparison_results} et nous montrons que notre nouveau détecteur produit un mean Average Precision (mAP) de 94,81\% tandis que le meilleur détecteur de l'état de l'art, le R-FCN, produit un mAP de 98,73\%.
Toutefois, notre détecteur est 40 fois plus rapide pouvant traiter 500 images à la seconde tandis que le R-FCN ne peut traiter que 12,5 images à la seconde.
Une grande part de cette différence est dûe au fait que nous avons conçu l'architecture du détecteur basé sur le Spatial Transformer par rapport à notre tâche de détection présentant une petite taille d'image d'entrée.
Une comparaison plus juste aurait été de réduire aussi la taille des architectures de l'état de l'art pour qu'elle soit plus adaptée à la taille des images d'entrées de notre tâche de détection.

Nous démontrons aussi que l'utilisation d'information contextuelle comme la tête de note associée au symbole à détecter et l'utilisation de technique d'augmentation de données nous a permis d'améliorer les résultats de notre nouveau détecteur de 30,8\% de mAP\@.
Ce travail a été publié dans l'article \textcite{choi_accidental_2018}.

Par la suite, nous avons aussi appliqué différents détecteurs de l'état de l'art du Deep Learning comme the Faster R-CNN, R-FCN et SSD sur le jeu de données de partitions musicales manuscrites MUSCIMA++ \autocite{hajic_muscima++_2017} basé sur le jeu de données MUSCIMA \autocite{fornes_cvc-muscima_2012} pour démontrer la capacité de ces modèles à détecter des symboles musicaux dans des régions plus grandes et avec un ensemble plus grand de symboles musicaux.
Nous démontrons aussi que les détecteurs de l'état de l'art du Deep Learning peuvent aussi être appliqués à des données manuscrites.
Notre travail, qui a été publié dans l'article \citeauthor{pacha_handwritten_2018} \autocite{pacha_handwritten_2018}, montre que nous achevons un mAP de plus de 80\% sur un large ensemble de symboles musicaux.
Nous appliquons ces détecteurs sur des régions suivant les lignes de portées des partitions car la page entière serait trop grande pour être directement traitée par les détecteurs.
Le principal problème que nous avons rencontré durant ces travaux est lié au déséquilibre des différentes classes de symboles dû à la notation musicale où des symboles comme les têtes de notes sont extrêmement fréquents tandis que d'autres symboles comme le double dièse sont très rares et utilisés uniquement dans des situations très précises.

\section*{Détection de symboles non supervisée avec le \Method{}}
\label{sec:résumé:détection_de}

L'utilisation de technique de Deep Learning pour la détection de symboles musicaux est un processus demandant une grande quantité de données annotées.
Ces annotations sont traditionnellement produites manuellement et sont longues et coûteuses à produire.
Lorsqu'un petit jeu de données existe, il est possible d'augmenter artificiellement la taille du jeu de données en utilisant des techniques comme le déplacement aléatoire de la région d'intérêt et c'est ce que nous avons fait dans nos travaux précédents.
Mais lorsqu'il n'existe aucunes données préalables, il ne reste que peu de stratégies pour amorcer un processus de reconnaissance automatique.
Une stratégie communément utilisée dans le domaine de la reconnaissance de document est l'utilisation de données synthétiques produites automatiquement.
Pour des partitions musicales, des logiciels de gravure de partitions musicales peuvent être détournés pour produire une grande quantité d'images de partition contenant des variations de musique, de notation, de police.
Cependant, il est difficile d'adapter ce processus de génération à la reconnaissance d'un nouveau corpus de données, car certaines caractéristiques de partitions historiques comme l'utilisation de la notation musicale de l'époque, la gravure manuelle sur plaque de cuivre et la dégradation à cause du temps ne sont pas pris en compte par ces logiciels.

C'est pourquoi, nous avons décidé de rechercher une méthodologie plus simple pour la détection de symboles non supervisée.
Au cœur de nos travaux, nous proposons une nouvelle méthode de détection de symboles musicaux non supervisée appelée \Method{} utilisant uniquement des symboles isolés et basée sur une combinaison de méthodes syntaxiques et de modèle génératif de Deep Learning (GAN).
Une vue d'ensemble de notre méthode est présentée dans la \cref{fig\string:isolating-gan} et se définit donc en trois étapes:
\begin{enumerate}
  \item l'identification des régions d'intérêt;
  \item l'isolation des symboles;
  \item la détection des symboles isolés.
\end{enumerate}

\paragraph{L'identification des régions d'intérêt}
\label{par:résumé:détection_de:l'identification}

Notre méthode reposant sur un système génératif instable de type GAN, il est vital de réduire la taille des images à générer.
Pour cela, nous utilisons la méthode syntaxique DMOS pour isoler des régions d'intérêt en utilisant des indices contextuels.
Dans notre tâche de détection d'altérations, nous utilisons les têtes de notes précédemment reconnues pour décider d'une zone carrée de $4\times4$ interlignes située à gauche de la tête de note.
Cela étant, notre méthode est conçu de façon à pouvoir être appliquée à d'autres type de symboles musicaux.

\paragraph{L'isolation des symboles}
\label{par:résumé:détection_de:l'identification:l'isolation}

L'idée centrale de notre méthode est de construire un domaine de représentation simplifiée utilisant uniquement des symboles isolés et servant d'intermédiaire entre la représentation complexe des partitions historiques réelles et la tâche de détection.
Concrètement, ce domaine de représentation se résume à des symboles isolés de tailles aléatoires, positionnés de manière aléatoire dans une image à fond blanc.
Nous proposons de réaliser ce transfert entre la représentation complexe des partitions historiques réelles et le domaine de représentation simplifiée de symboles isolés sur fond blanc en utilisant un réseau génératif antagoniste (GAN) \autocite{goodfellow_generative_2014}.
Ce modèle de GAN est composé de deux parties: une partie générative de type auto-encoder utilisant une architecture existante appelé U-Net \shorthandoff{:}\autocite{ronneberger_u-net:_2015}\shorthandon{:} capable de transformer une image en une autre et une partie discriminante permettant la mise en place d'un apprentissage antagoniste.
Ce réseau est entraîné par une fonction d'apprentissage hybride comprenant la fonction d'apprentissage antagoniste originelle du GAN et une fonction d'apprentissage de reconstruction d'image appliqué à la partie générative.
Cet apprentissage permet au générateur du GAN de transformer des images de partitions réelles en images contenant uniquement des symboles isolés à détecter sur un fond blanc.
Ces symboles isolés sont ensuite détectés par un détecteur préentrainé que nous présentons maintenant.

\paragraph{La détection de symboles isolés}
\label{par:résumé:détection_de:la}

En utilisant uniquement les symboles isolés, il est trivial d'entrainer un détecteur pour réaliser une tâche de détection synthétique dans ce domaine de représentation simplifiée de symboles isolés sur fond blanc.
Une fois entrainé, nous appliquons ce détecteur sur les images générées de l'étape précédente pour détecter les positions et la classe des symboles dans les images réelles de partitions historiques.

Pour évaluer la robustesse de notre méthode, nous montrons l'application de notre méthode sur deux jeux de données de détection d'altérations dans des partitions historiques: l'un homogène contenant 70 pages de 5 partitions historiques et 2150 symboles annotés et l'autre plus large et plus hétérogène contenant 1812 pages de 58 partitions avec 818 symboles annotés.
La méthode de génération étant instable, nous proposons un processus d'arrêt de l'entraînement et une méthode de sélection du meilleur entraînement parmi 10 entraînements identiques en utilisant un petit jeu de validation de 20 exemples par classe manuellement annotés.
Nous obtenons un mAP de 94,8\% sur le premier, petit jeu de donnés et un mAP de 82,5\% sur le second jeu de donnés plus hétérogène.
Pour démontrer l'utilité de la partie générative de notre méthode nous comparons les résultats de notre méthode en faisant un test d'ablation de la partie générative et montrons que l'utilisation de la partie générative permet d'améliorer de 30\% la précision sur le premier petit jeu de données et de 66\% la précision sur le deuxième jeu de données plus hétérogène.

\section*{Conclusion}
\label{sec:résumé:conclusion}

Dans ce travail de recherche sur la détection de symboles musicaux dans le cadre de partitions imprimées historiques denses, complexes et bruitées, nous avons tout d'abord exploré la tâche de détection de symboles musicaux en utilisant des modèles de détecteur de l'état de l'art du Deep Learning et avons aussi proposé une nouvelle architecture de détecteur adaptée pour une tâche de détection de symboles musicaux dans une zone restreinte.
Après cette exploration de modèles de détections supervisés, nous avons fait le constat du manque crucial de données annotées permettant l'utilisation direct de modèles supervisés de Deep Learning.
C'est pourquoi, nous proposons notre nouvelle méthode appelée \Method{} de détection de symboles musicaux non supervisée utilisant uniquement des symboles isolés.
Cette méthode utilise une combinaison d'une méthode syntaxique et de Deep Learning pour graduellement simplifier la tâche de détection en trois étapes.
Tout d'abord, la méthode syntaxique est chargée d'identifier des régions d'intérêt en utilisant des éléments contextuels reconnus apriori, puis notre modèle génératif est en charge de simplifier la représentation graphique en isolant les symboles à détecter et en effaçant le fond bruité.
Enfin, un détecteur préentrainé en utilisant des symboles isolés préexistants est utilisé pour détecter les symboles présents dans les images générées à l'étape précédente.

Nous appliquons notre méthode sur deux jeux de données, l'un petit et homogène et l'autre plus large et hétérogène et obtenons des résultats de détection de 94,8\% de mAP et de 82,5\% de mAP respectivement.
Cela nous a aussi permis de traiter de manière automatique le jeux de données large et hétérogène en détectant 38908 symboles dans 1774 pages de partitions historiques imprimées.
L'ensemble de nos expérimentations a nécessité 830 entrainements différents, ce à quoi nous estimons avoir pris environ 2 mois et demi de temps d'entrainement.
Dans de futurs travaux, nous projetons d'appliquer notre méthode à de nouveaux types de symboles musicaux, tout en stabilisant le processus d'apprentissage en utilisant des avancées de l'état de l'art sur les réseaux antagonistes génératifs comme le Wasserstein GAN \autocite{arjovsky_wasserstein_2017}.
L'application à d'autres types de documents comme des partitions manuscrites est aussi envisagée.

Nous pensons que notre nouvelle méthode de détection de symboles non supervisée est un premier pas dans la construction de méthode de détection entièrement autonome, pouvant s'adapter à de nouveaux corpus sans effort d'annotations manuelles.
