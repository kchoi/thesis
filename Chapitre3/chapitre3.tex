\chapter{Learning Symbol Detection Without Manual Annotations}
\label{chap:learning}

\section{Introduction}
\label{sec:learning:introduction}

In the previous chapter, we have demonstrated the application of Deep Learning-based detectors for the task of music symbol detection.
We trained small (ST detector, SSD) and large (Faster R-CNN, R-FCN) detector models on a constrained detection task (single accidental in a small image patch) as well as a more general detection task (large set of music symbol classes in medium size images) and produced highly accurate detection suitable for an OMR system.
While much more work still is needed for improving the detection of music symbols in larger images (whole page detection) and different music scores types, we believe we have shown solid baseline results that demonstrate the interest of using Deep Learning-based detectors that are able to seamlessly adapt to a new corpus of music scores documents.

However, during our work on accidental detection in dense and noisy historical printed music scores presented in \cref{sec:supervised:hybrid}, we faced the major problem of not having an already available music symbol detection dataset suitable for our task.
With our hybrid approach of using a syntactical method driving our Deep Learning-based detector and using data augmentation techniques like bootstrapping explained in \cref{par:supervised:hybrid:dataset:bootstrapping}, we managed to reduce the amount of manual annotation to a minimum.
Nonetheless, this approach required the manual annotation of $\sim10,000$ small image patches by drawing a bounding box around accidental symbols and affecting a label to each bounding boxes.

For the document recognition community, this lack of annotated dataset is quite a common challenge faced for each new type of documents we want to process.
The general solution to this problem has often been to use synthetically generated data to train Deep Learning model by using software such as DocCreator \autocite{labri_doccreator_2018}.
Then the recognition process is bootstrapped by applying these pretrained models to the real documents.

In this chapter, we review how synthetic data can be used to bootstrap the recognition of historical printed scores with no previously existing dataset.
We first consider how synthetic music scores can be used to train Deep Learning models in \cref{sec:learning:train} and discuss the advantages and shortcomings of using whole page synthetic scores.
In \cref{sec:learning:method}, we argue for a simpler approach for synthetic data generation that can be coupled with a generative method in order to do unsupervised music symbol detection.

\section{Train With Synthetic Data}
\label{sec:learning:train}

As mention previously, the lack of annotated dataset is quite a common challenge for the document recognition community.
Existing Deep Learning models are often reused for a wide variety of task, going from document structure recognition to Handwritten Text Recognition (HTR).
With the growing need for annotated data, the document recognition community has already constituted several large annotated dataset such as the IAM database \autocite{marti_iam-database_2002}, the RIMES dataset \autocite{grosicki_rimes_2008} or the READ dataset \autocite{sanchez_icfhr2016_2016} that can be used mainly for HTR\@.
However, it is unrealizable to manually constitute datasets of such magnitude for every type of documents and every kind of document recognition tasks.

One of the particularity of the document recognition domain is the fact that the heart of our work is to process automatically human artifacts.
Indeed, documents are physical objects that are produced by humans and therefore often follows some notations or logic in their creation.
For example, a letter will often follow the same structure, handwritten text will follow the writing convention of its language, mechanical drawing will use a certain set of standardized graphical components.
Paradoxically, rules that define how a document should be formatted will be broken because of human unintentional or intentional mistakes or by natural degradation artifacts often present in historical documents.

Therefore, a lot of research efforts of the document recognition community has been directed towards understanding the production rules of document we want to recognize and capturing the possible variability of such rules. 
With the expert knowledge of how a document was produced and transformed, it is then possible to automatically produce synthetic documents that present the same format, content and variability as the original documents.
The advantages of synthetically produced documents is the automatic production of associated annotations useful for training Deep Learning models for task like HTR or document structure recognition.
Once a recognition model is trained on synthetic data, the model can then be applied to the original documents we wanted to process in the first place.
An illustration of the whole process is given in \cref{fig:unsup_detect_synth_method}.

\begin{figure}[htpb]
  \centering
  \includegraphics[width=\linewidth]{ressources/dl-detector-synthetic-data.pdf}
  \caption[Training with synthetic data]{\label{fig:unsup_detect_synth_method}Training Deep Learning detector with synthetic data and process real data using the final trained detector.}
\end{figure}

Following the need for synthetic data generation, we explore in the next section how synthetic data could be generated in the context of music score documents.

\subsection{Synthetic Music Score Generation}
\label{ssec:learning:train:synthetic}

In the context of printed music score creation, the production of such documents has been digitized since the advent of personal computers in the 1980s.
Multiple high-quality music typesetting software exists such as Finale, Sibelius or MuseScore as explained in \cref{ssec:state:music:music}.
These computer programs propose a graphical user interface with which the user is able to interact and input music notes and symbols, either using a computer keyboard and mouse or by using a MIDI keyboard for a more intuitive process.
To produce a digitized music score page, the music typesetting software uses a combination of drawing primitives like lines and curves to draw staff lines and slurs and glyphs from music fonts that follows the Standard Music Font Layout (SMuFL) standard \autocite{smufl_standard_2013}.
The software is also able to automatically format the document so that it can maximize the readability of the score while respecting the complex set of rules of the music notation.

With the objective of producing synthetic printed music scores for OMR, the use of music typesetting software is a self-evident idea to produce potentially an infinite amount of synthetic data and we explored this idea in \textcite{choi_music_2018}.
However, given that the focus of such music typesetting software is to produce readable and beautiful music score document, it is not their explicit goal to imitate existing historical music score document.
This leads to various limitation when trying to use such music typesetting software for OMR research.

The modern music notation is a loosely defined set of rules, giving lots of freedom to the engraver for the placement of music symbols.
This leads to subtle biases in the position of music symbols either in historical printed music scores or in digitally produced music scores.
Depending on the typesetting software used, it is possible to alter the parameters regulating the position of most symbols and therefore potentially adjust positional biases to match historical printed score.
However, this process has never been studied and is very difficult to carry out without the use of an already existing real historical printed music score dataset annotated with the position of every symbol.
We believe it is in our interest to propose an unsupervised music symbol detection method that is robust to such positional biases.

Moreover, music typesetting software produces document to be read by humans and often produces music score document in the PDF or MusicXML format.
For tasks like music symbol detection or music notation reconstruction, such file formats do not contain sufficient information such as the label and position of music symbols, or the relations between symbols.
However, the music typesetting software itself is able to produce such detail information about the music score and the open source MuseScore typesetting software \autocite{schweer_musescore_2018} has already proposed some ways of exporting detailed internal information for use by OMR software and researcher.

Finally, historical printed music scores present artifacts and degradation introduced by a manual engraving errors, bad preservation through time and scanning process.
All of these transformations impact the final image to process and is often the biggest source of errors in the OMR process.
The document recognition community has studied extensively artifacts and degradation present in historical document and software such as DocCreator \autocite{labri_doccreator_2018} exists for the purpose of artificially introducing noises and degradation artifacts in document images.
However, it is still a difficult and manual process of choosing and adapting the correct noise models and degradation artifacts types that will correspond to the noises and degradations present in historical music scores.
We also believe it is in our interest to propose an unsupervised symbol detection method robust to any kind of noises and artifacts present in historical printed music scores.

\subsection{Problematic}
\label{ssec:learning:train:problematic}

The second half of the 18th century is often considered as the golden age of classical music and has produced a vast amount of printed music scores from the most famous compositors like Mozart, Haydn and Beethoven.
We believe that the automatic processing of such historical scores is very important to further the conservation and utility of such document for musicologist and music enthusiasts alike.
However, the recognition process of such historical printed scores is a very difficult challenge for OMR due to the complexity of the modern music notation, their manual production method (manual engraving) and degradation inflicted by time and scanning process.
Moreover, the absence of existing annotated dataset of dense and noisy historical printed scores prevents the use of state-of-the-art Deep Learning recognition method for tasks like music symbol detection.

While the use of music typesetting software is an enticing path for synthetic music score generation, we have shown that there is a discrepancy between digital music scores and historical printed scores in the music notation, symbol shape and position and the document general appearance influenced by the degradation state of the historical document image.
However, the use of synthetic data is not limited to the previously presented workflow in \cref{fig:unsup_detect_synth_method}.
Synthetic data can be used in a weakly-supervised or unsupervised fashion as previously presented in \cref{sec:state:generative} with a data augmentation strategy or domain transfer strategy coupled with generative methods.

Following the general focus of our work, we believe that a new unsupervised symbol detection methodology is needed that can either replace fully supervised method or at least replace the need for manually annotated data.
This method should be able to automatically adapt to a new corpus of documents and use a simple synthetic generation strategy to minimize the possible discrepancies between real data and synthetic data.

In the next section of this work, we propose an answer to this very difficult problem by presenting our new \Method{} method.

\section{\Method{} for Unsupervised Symbol Detection}
\label{sec:learning:method}

In this work, we propose a new unsupervised music symbol detection method called \Method{} that is able to learn the task of music symbol detection in historical printed scores without using any manual annotations.
In contrast to using a whole page of synthetic music score and complex synthetic generation procedure, our method uses a simpler synthetic data generation procedure needing only a small isolated music symbols dataset.
Our strategy is to gradually simplify the complexity of the detection task by using the following iterative three steps method that we also illustrate graphically in \cref{fig:isolating-gan}:

\begin{enumerate}
  \item Identify Region of Interests (RoIs) possibly containing music symbols
  \item Simplify the graphical representation by isolating music symbols to detect
  \item Detect isolated music symbols
\end{enumerate}

Under the hood, two main ideas form the basis of our method: \Cref{ssec:learning:method:simple} present the use of isolated music symbol to form a simple graphical representation for symbol detection while \cref{ssec:learning:method:using} explains how to transform complex, dense and noisy images of historical printed music scores into a simple graphical representation.

\subsection{Simple Graphical Representation for Symbol Detection}
\label{ssec:learning:method:simple}

The fundamental idea behind the design of our method is to create an intermediate graphical representation as a platform for a trivial detection task.
A symbol detection task is defined by the objective of predicting a bounding box and class label for each symbol present in an image.
Therefore, the simplest detection task that we can artificially create is to synthetically generate an image only containing the symbols to recognize in a white empty background.
Such images encode all and only the information needed for detection such as the shape and position of symbols to detect.

Such images can be automatically crafted using only isolated symbols randomly positioned and scaled in a blank image.
However, the remaining problem is to know if we are able to adapt real images of historical printed scores containing background, noise and degradations signals to this simple graphical representation while preserving the shape and positional information required for accurate symbol detection predictions.
In the next section, we present how we tackle this adaptation using a GAN-based generative method.

\subsection{Using GAN for Isolated Symbols Domain Transfer}
\label{ssec:learning:method:using}

The use of a generative method is inspired by the previous cross-domain adaptation work like the Cycle-GAN previously presented in \cref{par:state:generative:cross:cycle} that presented a way of translating images of one representation domain into another representation domain while keeping essential characteristics common to the two graphical domains.

In our case, we propose to use a GAN-based generative method capable of adapting images of historical printed music scores into a simpler graphical representation containing only the isolated symbols to recognize and removing the noise and background information.
To simplify, our generative process can be seen as a graphical filter isolating important music symbols we want to detect.
However, this adaptation has to be done while keeping identical the size and position of the symbols of interests.

Knowing the relatively low limit in the image size that generative method such as GAN have, we also propose to limit the size of the image patches seen by the generative model using the DMOS syntactical method previously used in \cref{ssec:supervised:hybrid:dataset} and identify small Regions of Interest (RoIs) with higher probability of containing the symbols we want to detect.

Once a small image patch of a real historical printed music score has been transformed into our simpler graphical representation containing only isolated music symbols in a white background, it is then trivial to detect such symbols using an isolated music symbol detector trained using synthetic data automatically produced using an already existing isolated music symbol dataset.

\section{Conclusion}
\label{sec:learning:conclusion}

To summarize, the recognition of dense and noisy historical printed music scores is a very complex task lacking the required manually annotated detection dataset for applying existing fully-supervised state-of-the-art detector models.
While the use of synthetic music scores as a replacement for manually annotated dataset is tempting, we believe a simpler synthetic generation method using only isolated music symbols coupled with a GAN-based generative method can better adapt to new unseen corpus of music score documents.
We believe that this work is the first OMR work focusing on unsupervised music symbol detection and therefore we preferred simplifying our synthetic generation strategy to a maximum.
We also believe that synthetic music scores generated typesetting music software could in the future be used together with our approach to improve and extend the reach of our work.

At the end of this chapter, we only have introduced the main ideas and concept underlying our method.
In the next chapter, we present in details our new three steps iterative \Method{} method and in \cref{chap:experiments} evaluate in various experiments the detection accuracy and robustness of our method.
