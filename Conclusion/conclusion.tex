\chapter*{Conclusion}
\label{chap:conclusion}
\addcontentsline{toc}{chapter}{Conclusion}
\chaptermark{Conclusion}

We believe the recognition of historical printed music scores is essential to ease the use and study of such scores by musicians and musicologist.
However, the difficulty of the detection of music symbols and reconstruction of the music notation caused by the high density of music symbols, the complexity of the music notation and degradations due to printing methods and age of the document has prevented the use of traditional OMR method.
With the introduction of new Deep Learning-based computer vision model, we show in this work how such Deep Learning models can be beneficial for the early graphical recognition OMR step that is music symbol detection.

During this work, we studied the detection of music symbols in the context of Optical Music Recognition of historical printed music scores.
In \cref{chap:state}, we gave an overview of what is a music score, followed by a presentation of the different steps that constitute an OMR system.
We especially focused on thoroughly presenting the task of music symbol detection since this is the main task we are interested in this work.
We then propose to explore the various Deep Learning model that will help us achieve our music symbol detection task, presenting both fully supervised detection models such as the Faster R-CNN as well as generative models that can be used in for training unsupervised tasks.

\Cref{chap:supervised} presents the beginning of our work where we present the use of state-of-the-art Deep Learning detection model as well as a custom original detection model for a small and focused accidental detection task.
For this task, we show that state-of-the-art detectors can produce very good detection results such as 98.73\% of mAP with an IoU threshold of 75\%.
Therefore, we broaden the scope of the detection task on applying state-of-the-art detectors on a much more complicated and difficult handwritten music score dataset: MUSCIMA++.
Even on such a broad and complicated dataset, we also show that the detection results can be very good at 80\% of mAP.
However, one of the main remaining difficulties in a general music symbol detection task is to account for the imbalance in the frequency of symbol classes to detect.

While we have shown that Deep Learning-based detectors can produce highly accurate detection of music symbols, we discussed in \cref{chap:learning} the impact of manual annotations in the process of training such Deep Learning methods.
Indeed, the production of such manual annotations are very costly and slow to produce.
In the document recognition community, this bootstrapping problematic of producing ground truth has often been solved by generating synthetic data, often involving complex generation procedures.
For music scores, one could use music typesetting software to produce almost an infinite amount of varying music scores.
However, there is no guaranty that trained music symbol detection model on synthetic scores would be able to perform as well on real historical printed music scores since a lot of variation that we can see in historical music scores due to the engraving techniques and age of the score are not taken into account by typesetting software.
Therefore, we propose a hybrid method of using a small amount of synthetic data generated using only isolated music symbols on a white background and a generative method able to transform real images of historical music scores into a simpler graphical representation.

In order to avoid as much as possible the use of costly manual annotations, we proposed in \cref{chap:unsupervised} our new \Method{} method for unsupervised music symbol detection.
Our new method consists of three iterative steps that gradually simplify the tasks without ever needing manually annotated ground truth for symbol detection:

\begin{enumerate}
  \item Step 1: Identify Region of Interests
  \item Step 2: Isolate music symbols using \Method{}
  \item Step 3: Detect isolated music symbols
\end{enumerate}

At the heart of the method, we use isolated music symbols to create a simplified domain of representation where isolated symbols are printed in a blank image at varying position and size.
This simplified representation allows us to pretrain a fully supervised detector on this trivial detection task and transform real image of historical scores into generated images containing only symbols to detect in an empty background.
Then, we also discussed how we can use and evaluate our method in a real use setting.
The goal of our method is to be used in a situation where no annotated data is available to evaluate our model.
Therefore, we design an evaluation method using only a few manually annotated examples and augmented using a simple bootstrapping technique.

In \cref{chap:experiments}, we demonstrated the effectiveness and robustness of our new method by presenting an extensive set of experiments evaluating the development of our method and the accuracy of the method on two music symbol detection datasets of varying difficulty.
The first small accidental dataset, we limited the difficulty of the task by artificially removing and later limiting images without symbols to detect.
While the training of our method is done entirely without manually annotated data, the manually produced ground truth of this small dataset allowed us to develop, evaluate and tune our method architecture, as well as design an effective way to tune our method with almost no ground truth.
For the second larger accidental dataset, we constituted a 100 times larger dataset and annotated 20 pages of the dataset for evaluation.
This dataset constitutes a much more challenging detection task, since the dataset is larger, more heterogeneous and we did not restrict the amount of images with no symbols to detect.
In order to synthesize our simplified representation of isolated symbols in a blank canvas, we use a small isolated symbol dataset of 541 symbols manually annotated with class label information.
With our approach, we obtained a mAP of 94.8\% on the small simplified dataset and a mAP of 82.5\% on the large difficult dataset for a detection task with three accidental classes.
We also demonstrated that using the \Method{} to filter and isolate symbols before the detection operation reduces the number of false positives from 2,696 to 57 on the large dataset.
We bootstrapped the annotations of our large difficult dataset by applying our method on 1,774 pages of historical music scores and detecting automatically 3,8908 new accidentals.
In order to complete this whole set of experiments, a total of 102 different hyperparameter combinations were explored for which 810 trainings had to be done and took around 2.5 months of pure training time.

Finally, in \cref{chap:method}, we discussed various improvements of our method that could be made in the future.
Indeed, much work is still needed to improve the training efficiency and stability of our method.
We believe that new GAN models from the literature such as the Wasserstein GAN could help stabilizing our training methodology.
We also plan to expand our method to a larger class set, larger image sizes but also to other kind of structured documents such as handwritten music scores or even electrical circuit design documents in order to reduce the computational cost and generalize the application of our method.

From our very focused study of the task of detecting music symbols, we believe that this work is only the first step towards for the exploration of unsupervised detection methods.
Moreover, many fields of application, especially niche types of documents with less manual annotations efforts, can benefit from the use of our unsupervised detection method.
Finally, entirely autonomous systems could be designed using our method that can adapt the model to detect entirely new class of symbols using no ground truth and only isolated symbols.
This system could be bootstrapped in successive recognition stages by a syntactical method such as DMOS which would be able to construct layer by layer the syntactical structure of the document.
