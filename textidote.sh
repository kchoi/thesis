cat Chapitre$1/chapitre$1.tex \
  | sed 's/vref/ref/g' \
  | sed 's/Vref/ref/g' \
  | sed 's/autocite/cite/g' \
  | sed 's/\\Method{}/Isolating-GAN/g' \
  | sed 's/\\Method/Isolating-GAN/g' \
  | sed 's/\\shorthand.*//' \
  | sed 's/\\citeurl{.*}//' \
  > Chapitre$1/chapitre$1_textidote.tex; \
  textidote --read-all --output html --check en --dict dict.txt \
  --ignore lt:en:PUNCTUATION_PARAGRAPH_END,sh:seclen,sh:secskip \
  Chapitre$1/chapitre$1_textidote.tex > textidote_report.html
