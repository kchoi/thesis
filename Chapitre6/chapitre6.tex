\chapter{\Method{} Usage and Future Works}
\label{chap:method}

\section{Introduction}
\label{sec:method:introduction}

We have now presented the entirety of the work accomplished during this thesis.
Starting from the study of fully supervised music symbol detectors in \cref{chap:supervised}, we introduced a new unsupervised music symbol detection method that we called \Method{} in \cref{chap:unsupervised} combining a generative model and a detector model.

In \cref{chap:experiments}, we presented the whole set of experiments that guided our method design together with an extensive evaluation of our method relative to important hyperparameters and symbol detection datasets of varying difficulty.
In these experiments, we demonstrated the high precision of the detections produced by our method and showed that our method can approach very closely the detection quality of fully supervised detectors without using any manually annotated ground truth.

Nonetheless, much work is still needed to improve the quality of our method, which we discuss in the next section

\section{Improving \Method{}}
\label{sec:method:improving}

%Architecture: U-Net Generator + SSD

%Input data: whole real dataset

%Merge predictions for multiple classes

%Keep high confidence detection prediction: optimize precision instead of recall

%If needed: train a supervised detector (Faster R-CNN) using high confidence detection prediction

\paragraph{Improving Training Objectives}
\label{par:method:improving:improving}

While our multi-loss objectives for the \Method{} training combining the classical GAN losses and our image reconstruction loss is able to train our GAN model, the implementation could be simplified by combining the adversarial loss and the reconstruction loss into a single loss using a single loss balancing factor.
This would reduce the amount of hyperparameters to tune, reduce training time and simplify the implementation and hyperparameters tuning of our model.

\paragraph{Training Instability}
\label{par:method:improving:improving:training}

The most problematic aspect of our method is the instability of the training, producing models with large variation in the generation quality while being trained with the exact same hyperparameters.
In this work, we mitigate this undesirable aspect by training multiple models with the same hyperparameters and choosing the best performing model using a small validation dataset.
However, this strategy is very wasteful in computation effort, multiplying the training time by the number of duplicated model.

The search for stability of GAN models was already discussed by the literature and presented in \cref{ssec:state:generative:generative_adversarial}.
GAN architectures like the Wasserstein GAN, aims to stabilize the training using an improved training objective and some architectural changes guarantying non-zero gradients and therefore avoiding gradient vanishing effects.
Another approach to improve stability is discussed by \textcite{karras_progressive_2018} where the GAN architecture is trained layer by layer while also using statistics of the minibatch training data to guaranty that the generated data has the same variation as the real data to mimic.
However, it is not clear if the increase in stability of our method will correlate with improved detection results.
The fact is that the actual training objective of the GAN, which is to transform images extracted from real historical printed music scores into images containing only isolated symbols on a white background, is not the same as our symbol isolating task we want the GAN generator to accomplish.
The differences in symbols size and shape between the real historical printed scores and the isolated symbol dataset leads to irreconcilable differences, and it is yet to be proved that an actual stable training can be done using an adversarial learning strategy.

Given the unknowns of using yet another GAN architecture, we kept in this work the use of a classical GAN architecture and propose to explore the use of newer and possibly stabler GAN model in future works.

Another path to improve the stability of our training method would be to better regroup our training data and presenting a more uniform corpus of real data during the GAN training.
This uniformity would simplify the image-to-image translation task of the generator and therefore improve the generation quality and detection quality of our method.
However, this approach have the main drawback of increasing the computation cost of our method because of the retraining of the GAN for each new corpus of music scores.

\paragraph{Larger Symbol Class Set}
\label{par:method:improving:larger}

During this work, we exclusively worked on a reduced accidental class set with three accidental symbol class: Flat, Natural and Sharp.
Now that we have shown that our method can produce highly precise detection on a relatively small class set of symbols, we believe that our method can be adapted to a much larger class set of symbols, including note heads, flags, attack signs, rests, clefs\dots
To adapt to new symbol classes, the first step of our method explained in \cref{sec:unsupervised:step} will have to be adapted but thanks to the flexibility of the grammatical description of the musical notations, these changes will be simple and straightforward.

We also believe that our method could be use for symbol recognition in handwritten music scores.
However, the variability of each writer would have to be taken into account, maybe by using isolated symbol examples of the same writers.
Moreover, we believe that any kind of structured documents with segmentation problems such as electrical circuit design documents could be the target of our method.

\paragraph{Larger Image Size}
\label{par:method:improving:larger:larger}

For now, we only have tested our method with a relatively small image patches of $128\times128$ pixels.
We started with a small image because of the known difficulty to train a generative model with large images.
However, the literature has now shown that GAN model can scale to larger images and produce high resolution images.

Future work could be done to improve this aspect of our method coincidentally reducing the effect of symbols on the edge of images, which we found was a significant cause for detection errors.
Larger images also mean that fewer images can be used to evaluate a whole page of document, reducing the total time needed to process music score page.

Another approach to expand the use of our method to a window larger than $128\times128$ pixels would be to process the entire page of a music score with the trained generator using a sliding window method before doing the detection stage.
This in turn would resolve the problem of symbols on the edge for the generator, although we would still have to find a strategy to apply the detector on the entire page of music score.
From preliminary testing, we found out that the generator does generalize well to unseen background shapes during training and manage to remove most of the music score background while keeping the relevant symbols to detect.
In our specific study of accidental symbols, we found out that after some preliminary testing that while we trained our generator on examples of accidentals always attached to a note head, the generator was able to correctly filter and isolate accidentals used in the key signature.
However, by using a sliding window method with a stride smaller than the window itself and adding the intensity of resulting images, we intensified all responses of the generator, the correct isolating behavior of the generator as well as the incorrect hallucination behavior of the generator.

\section{Autonomous Symbol Detection System}
\label{sec:method:autonomous}

Taking a step back, we believe our work is a first step toward designing an entirely autonomous symbol detection system, where no or very few ground truth examples are needed to apply our detection framework to a new type of documents.
By only using isolated symbols for the training of the detection model, we are able to bootstrap the use of data hungry Deep Learning model while maintaining sufficiently accurate symbol detection results.
Even if the detection results are not accurate enough, we hypothesize that the information gathered using our \Method{} method could be used in a second stage training using a fully supervised Deep Learning detector of high precision such as a Faster R-CNN.

Finally, we could entirely automatize the full recognition of a new corpus of music scores using a syntactical method as the DMOS method, where the parsing of the music notation could be done in multiple stages, with each stages concentrating on a few symbol classes and using previously detected symbols.
Each stages would produce relevant RoIs to be used by our \Method{} method together with a few isolated symbol examples.
