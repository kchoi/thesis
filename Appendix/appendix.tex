\begin{appendices}
\crefalias{chapter}{appendix}

\chapter{Experiments}
\label{chap:experiments2}

\section{\Method{} Robustness Evaluation}
\label{sec:experiments2:method}

We now measure the impact of some of the most important hyper-parameters of our method.
First, we measure the impact on the amount of images in the real dataset that does not contain any symbol we want to detect.
Then, we also show the impact of the isolated symbols sizes generated using the minimum and maximum size bounds hyper-parameters.

\subsection{Impact of Symbol Frequency in Real Dataset}
\label{ssec:experiments2:method:impact}

%expe 18: robustness to junk
%expe 49: better distribution of accidental vs junk
%expe 60: 10, 25, 50, 90%
%expe 65: 1, 5%

The stability of our GAN model depends not only on the architecture but also on the data used for training the model.
In this section, we study the impact of the amount of rejection images in the real dataset used as input to the generator on the stability of the GAN model.

\paragraph{Objectives}
\label{par:experiments2:method:impact:objectives}

What we call rejection images are empty images produced by our first preprocessing step of simplifying the music notation of real music scores explained in \cref{ssec:unsupervised:objectives:real} by identifying RoIs that has a high probability of containing the type of symbols we want to detect.
This step will obviously always have false-positive examples with images that do not contain any symbols to detect.
Since there are no way to know in advance of the ratio between the amount of rejection images and images that do contains symbols to detect, we investigate different ratios where we artificially modified the amount images that do contain symbols to detect and rejection images.
This experiment is based on the experimental settings of the \Method-ReconsLr+Neg experiment in \cref{sssec:experiments:architectural:stabilize:improved_discriminator} and only modify the composition of the real dataset used as input to the generator.

\paragraph{Datasets}
\label{par:experiments2:method:impact:objectives:datasets}

We artificially modify the ratio between images that contains a symbol to detect and rejection images.
We explore five different ratio values of 1\%, 5\%, 10\%, 50\% and 90\% of images with symbols to detect while keeping in mind that after manually annotating a lot of music score pages, we believe that around 10\% of images of a real dataset contains symbols to be detected.
Images with accidental symbols that we do not want to detect were also removed, since we noticed that the GAN model can sometimes confuse symbols with different accidental classes as the same class.
This simplification is done in order to isolate the sole effect of rejection images on the training of the GAN model.

\paragraph{Evaluation}
\label{par:experiments2:method:impact:evaluation}

In order to evaluate our method with different rejection ratios, we use our early stopping mechanism as presented in \cref{sssec:experiments:architectural:method:early} where we use both a small validation dataset of 20 examples per symbol classes with bootstrapping and the fully annotated training dataset for evaluation.
We report results using a mAP metric with IoU > 0.75 computed on the fully annotated training dataset but at epoch that maximized the results on the small validation dataset.
The training is repeated using the same rejection ratio 10 times with a different random seed and present the median, first and third quartile mAP results.
We also show the mAP computed on the full training dataset of the model that had the best results on the small validation dataset.

\paragraph{\Method-RejectRatio Results}
\label{par:experiments2:method:impact:evaluation:method}

\Cref{fig:reject_ratio} shows the results of using different rejection ratio where we observe different behavior for the three different accidental classes.
The model produces the most stable results for the sharp and natural classes when at least 5\% of all images contains symbols to be detected.
For the Flat class, the model needs at least 25\% of all images to contain symbols for the training to be stable.

However, this instability can be overcome using our early-stopping mechanism with our small validation set by selecting the best performing model out of a pool of 10 trained model.
Using this selection mechanism, we can maintain a good detection performance of $\sim$91\% of mAP for the Flat class for ratios of 5\% and 10\%.

In our experimental settings and with the dataset used here, we show that the detection performance degrades significantly when only 1\% of the images contain symbols to detect.
We believe this is due to the discrepancy in its training objective where the synthetic isolated dataset has systematically an isolated symbol inside the blank image while the real images seen by the generator has very few images with symbols to isolate.
This experiment shows that with our experimental settings and our specific set of datasets, our method is able to cope with this discrepancy when at least 5\% of images contains symbols to detect which should be enough considering that we estimate that around 10\% of images of a real dataset will contain symbols to detect.
For this experiment, we repeated each training 10 times, on 3 different accidental classes and explored 5 different ratio values and resulted in a total of 150 trainings to do.

\begin{figure}[htpb]
  \centering
  \input{ressources/reject_ratio.tex}
  \caption[Rejection impact study]{\label{fig:reject_ratio}Study of the impact of rejection on the detection of accidental symbols using \Method{}. We show detection results using 1\%, 5\%, 10\%, 25\%, 50\% and 90\% of symbol to detect out of the total amount of real data, see \cref{par:experiments2:method:impact:objectives:datasets}. Each experiments are repeated 10 times and we report the median, first and third quartiles of the real mAP at the best epoch found using our early stopping mechanism. We also show the mAP of the model chosen by our early stopping mechanism which gave the best results on the small validation dataset.}%
\end{figure}

\subsection{Sensibility to Generation Sizes of Isolated Symbols}
\label{ssec:experiments2:method:sensibility}

%expe 45: new min max size with high deviation
%expe 46: expe 44 with less deviation for natural

%GAN is very sensitive to isolated symbol sizes.

\paragraph{Objectives}%
\label{par:experiments2:method:sensibility:objectives}

One of the standing stone of our method is the use of isolated symbols to drive both the image translation done by the GAN model and symbol detection done by the SSD detector.
However, isolated symbols can not be used as provided since they have to be similar to the symbols we want to detect in real historical music scores.
We also use these isolated symbols in conjunction with simple data augmentation techniques which are commonly used in deep learning experiments to introduce more variation to the data and normally improve the performance of the trained model.
One basic modification to be made is the size that a symbol will have since isolated symbols and real symbols won't probably be of the same resolution.
Since no ground truth exists for the real music symbols we want to detect, we can only guess the ranges of sizes for each class of symbols.
Fortunately, music symbols follows strict typesetting rules, and we can often guess the possible range of sizes that a symbol class will have.
On the other hand, a lot of variation in sizes can be present by using different music typefaces which will have different sizes and width/height ratio for the same symbol class.
During the various experiments made while investigating the use of a GAN model for symbol detection, we noticed that the GAN model can be very sensitive to the variation in sizes that isolated symbols can take.
Therefore, we propose to evaluate in this section the robustness of our \Method{} in regard to isolated symbol sizes.

As usual, we base our experiment on the \Method-ReconsLr+Neg shown in \cref{sssec:experiments:architectural:stabilize:improved_discriminator} and reuse the same model and datasets except for one modification for the isolated symbol dataset.

\paragraph{Datasets}%
\label{par:experiments2:method:sensibility:objectives:datasets}

We use the same dataset setup as our baseline experiment \Method-ReconsLr+Neg except for one modification to the minimum and maximum possible width and height of isolated symbols.
We show in \cref{tab:iso_sizes} the detailed minimum and maximum width/heights values per class for the baseline and this new experiment.
For each class, we reduce the minimum width/height and augment the maximum width/height that accidental symbols can take.

\begin{table}[htpb]
  \centering
  \scriptsize
  \caption[Min/max width/height isolated symbols]{\label{tab:iso_sizes}Minimum and maximum width/height settings of isolated symbols for the \Method-ReconsLr+Neg and \Method-Sizes experiments. The values are in pixels and symbols are pasted in canvas of 128$\times$128 pixels.}
\begin{tabular}{@{}lcccccccccccc@{}}
\toprule
\multirow{3}{*}{Experiment}  & \multicolumn{4}{c}{Flat} & \multicolumn{4}{c}{Natural} & \multicolumn{4}{c}{Sharp} \\ \cmidrule(l){2-5} \cmidrule(l){6-9} \cmidrule(l){10-13}
                             & \multicolumn{2}{c}{height} & \multicolumn{2}{c}{width} & \multicolumn{2}{c}{height} & \multicolumn{2}{c}{width} & \multicolumn{2}{c}{height} & \multicolumn{2}{c}{width} \\ \cmidrule(lr){2-3} \cmidrule(lr){4-5} \cmidrule(lr){6-7} \cmidrule(lr){8-9} \cmidrule(lr){10-11} \cmidrule(l){12-13}
              & min & max & min & max & min & max & min & max & min & max & min & max         \\ \midrule
\Method-ReconsLr+Neg (low)  & 79  & 112 & 25  & 37  & 98  & 128 & 16  & 36  & 72  & 112 & 27  & 45          \\
\Method-Sizes (high) & 64  & 128 & 16  & 56  & 64  & 128 & 8   & 48  & 64  & 128 & 16  & 64          \\ \bottomrule
\end{tabular}
\end{table}

\paragraph{Evaluation}%
\label{par:experiments2:method:sensibility:evaluation}
As usual, we evaluate our method with our early stopping mechanism presented in \cref{sssec:experiments:architectural:method:early} by computing the mAP metric with IoU > 0.75 computed on the fully annotated training dataset at the epoch that maximized the results on the small validation dataset.
We repeat the training 10 times with different random seeds and present box plots of the 10 runs.

\paragraph{\Method-Sizes Experimental Results}%
\label{par:experiments2:method:sensibility:evaluation:method}

We show the results of this experiment in \cref{fig:sizes_high} where we can see that using a high variation in symbols sizes results in slightly worse results.
For example, we can see that for the Flat class, the variation in the first and third quartile is reduced by half by using a lower variation of symbol sizes.
For the Natural and Sharp class, the stability of the results are significantly better by using a lower variation of symbol sizes.

On the other hands, the maximum mAP obtain by one of the ten trained models is similar in either high or low variation of symbol sizes experimental settings.
This shows that our \Method{} method can still achieve find the best available detection performance even with badly adjusted size parameters.

For this experiment, we repeated each training 10 times, on 3 accidental classes and two set of size parameters for isolated symbols, totaling to 60 trainings.

\shorthandoff{:}\shorthandoff{!}
\begin{figure}[htpb]
  \centering
  \input{ressources/sizes_high.tex}
  \caption[Isolated symbols size impact study]{\label{fig:sizes_high}Study of the impact of isolated symbols sizes. We show detection results using a larger range of possible sizes for isolated symbols. Each experiments are repeated 10 times and we report the real mAP at the best epoch found using our early stopping mechanism.}%
\end{figure}
\shorthandon{:}\shorthandon{!}


\end{appendices}
