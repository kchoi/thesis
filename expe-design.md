---
secnumdepth: 4
---

Make a table of dataset with labels to be used as references.
how to evaluate detector trained on synthetic data and applied on real data.
evaluate gan with kfold cross-validation validation.
explain what is training, validation, test set with the task at hand which is annotations.
with new nomenclature.
2 pages explaining the new task comparing with old supervised task with
find a vocabulary to describe our problem.

# Chronological Work Summary

## 06/06/2018

* Use U-Net GAN to extract symbol, use detector to detect extracted symbol
* Noticed training instability

## 03/07/2018

* Grid-search training for sharp implementation

* SMUFL class set selection

## 12/07/2018

* GAN detection grid-search training evaluation with weighted pixel accuracy
    * grid-search on adv, disc lr, batch size, adv, disc k (num training)
* GAN literature review

## 19/07/2018

* grid-search results
    * typical bell like curves for lr parameters
    * adv lr should be smaller than disc lr

## 30/07/2018

* qualitative grid-search results with images
    * quite clean generation for best results
* use ssd to detect symbol
    * 0.99 mAP with IoU > 0.5
    * 0 mAP with IoU > 0.75
    * this means good spotting rate but detected boxes are not precise enough
* idea: use synthetic data in generator training

## 06/09/2018

* PRL paper review
* LR-GAN

## 13/09/2018

* PRL paper review

## 20/09/2018

prl paper review

## 27/09/2018

prl paper review

## 31/01/2019

* Results on natural class
    * ~0.8 of mean_weighted_pixel_acc
    * lots of mode collapse with bad parameters
    * adversarial loss divergence
    * hallucinations
* Rejection + improvement implementation
    * modify input data distribution
    * use other class + rejection images as rejection

## 07/02/2019

U-Net GAN training strategy to get to equilibrium and avoid divergence or mode collapse.

Main parameters research: disc, adv, generator lr and number of training steps.

reflexion on pixel accuracy metric (that was bad...)

## 14/02/2019

using pixel iou score as metric.
study on white and black pixels contributions.
work on rejection: show that adding 90% of images that does not contain the target class does not influence badly the gan results.

## 21/02/2019

Using weighted rejection accuracy metric for rejection qualification.

## 28/02/2019

rejection results with sharp class

idea: use synthetic data to improve generator training

## 07/03/2019

Study if synthetic data training for generator is worth it by looking at confusion + rejection task

## 14/03/2019

using synthetic data improved flat iou results by 20%

## 21/03/2019

Balance training using synthetic data with classic gan loss
Do not train the generator too much!

## 12/04/2019

Start to work on early stopping problem.
Study the behavior of training curves on iou metric between real and synthetic data.

Training curve for real dataset usually first reach a maximum at the beginning of the training and then decrease.
Training curve for synthetic dataset continually increase, which shows overfitting of the gan to the synthetic dataset.
Some local peak (min or max) are correlated between the two dataset.

## 10/05/2019

Cleaned up isolated symbol dataset
Added mAP metric
use a font as isolated music symbols

synthetic detection using musescores.

## 23/05/2019

early stopping results using various fold size: 20/50/100/400/2955

## 04/07/2019

Early stopping using reduced evaluation set of 10/20 manually annotated examples (without/with bootstrapping).

Using 20 manually annotated examples + bootstrapping seems sufficient for correct early-stopping mechanism.

However, remaining problem with bad results for natural class

# July 2019 work summary

expe-38 to 41: early stopping experiments with 10/20 with/without bootstrapping.

expe-42: retrained detector (make sure that the detector is not overfitted)

expe-43: manual natural correction by adding max 30 pixel on height

expe-44: automatically adjusted size of symbols using real accidental dataset

expe-45: use bigger size for symbols

expe-46: remove outlier for natural class in expe-44

expe-47: add negative examples in discriminator synthetic distribution

# Future plan of experiments

modelize width height distribution using 2D gaussian model (with more or less adjustements).

does staffline removal affect detections?

confusion of generated symbols:

* do a multi class gan generator
* modify synthetic discriminator distribution by adding false synthetic examples

extend usage of gan on other classes: note heads, attack signs, armor, flags, silences...

# Experiments Work summary

## expe-46

Improves natural results by removing single outlier with very small height.
Rejection is still very lacking, produces lots of false-positive detections by letting through parts of sharps and flats.

But overall, mAP on naturals improves by ~10%

Maybe we could use the reduced evaluation set to improve the generator.

# Thesis experiment design

General Subject: Optical Music Recognition of noisy, dense historical orchestral music scores with modern notation

Subject Background: Study interaction between syntactical methods and Deep Learning methods for OMR

* Syntactical methods used to formalize music notation
* Deep Learning used for graphical pattern recognition tasks

PhD Focus: Music symbol detection

PhD Contributions:

* Use syntactical method to define music symbol detection task
    * Define regions to be used for detection
    * Semi-automatic generation of data annotation
* Evaluate Deep Learning detectors for music symbol detection task
    * For simple, constrained single symbol detection task:
        * Data augmentation using bootstrapping strategy
        * Train and compare different Deep Learning detector architectures
    * For wholistic, global approach:
        * MUSCIMA++ dataset
        * Faster R-CNN, RFCN, SSD detectors
* Reduce manual annotations need for music symbol detection
    * Synthetic data generation using real isolated music symbol dataset
    * GAN model for translating real images of music scores into synthetic images
    * Detect symbols in generated images using pre-trained detector on synthetic data
    * Compare GAN results with detector trained using synthetic data generated using typesetting software

## Music Symbol Detection Experimental Design

### Small Scale Single Accidental Detection

Dataset: Accidental Dataset

Semi-automatic production of detection annotations:

* Classify connected-components using DL classifier trained on isolated music symbols
* Define small regions to be used for detection using syntactical method
* Manual corrections of ~3k detection annotations

Data augmentation method: Bootstrapping strategy

* Randomly move small region window with different vertical and horizontal constraints
* Variable augmentation quantity: 50k, 100k, 200k, 400k

Detectors:

* New architecture: Spatial Transformer-based detector
    * Use an explicit localization network to detect a symbol
* State of the art DL detectors: Faster R-CNN, RFCN, SSD

Experiments: Compare detectors performance using different bootstrapping quantity/constraints

Conclusion: Solved task with 97% mAP with IoU > 0.75

### Large Scale Global Music Symbol Detection

Dataset: MUSCIMA++

Dataset Preprocessing:

* Remove very rare music symbol classes
* Cut page images into smaller images following staves
* Remove very large symbols that does not fit in cut images
* Test impact of the removal of staff lines

Detectors: Faster R-CNN, RFCN, SSD pretrained on ImageNet

Experiments: Compare different architectures and impact of staff lines removal

Conclusion: Acceptable results with 87% mAP with IoU > 0.5

## Reduce Manual Annotations For Music Symbol Detection

The goal of music symbol detection is to accurately locate and classify a music symbol in an image of a music score.
The main interest of this task is to be able to track down the relationship of a music construct down to the bounding box location of a music symbol in an image of music score.
This information can then be used in the later OMR tasks like music notation reconstruction and for end-users like musicians or musicologists for tasks like identical reprints of historical music scores.

The literature in the OMR has already started to handle this task using fully supervised Deep Learning models.
These models can accurately handle the complexity of bi-dimensional music symbol constructs, both in handwritten and printed music scores.
The downside of these models is their need of huge amount of manual annotations where each symbol is described with a bounding box and label information.
As an example, we can cite as an example the MUSICMA++ dataset, which is a fully annotated version of the MUSCIMA dataset and took around 400 hours to produce.
Reducing the need for such manual annotations is an important and current problem for all domains of application of deep learning techniques where fully annotated datasets are scarce.

Since documents are pure human artefacts, the natural approach to solve the lack of manual annotations in the document recognition community is to use synthetically generated data and annotations.
This approach is mainly based on specific knowledge of the document's domain, e.g. music notations for music scores, letter format for letters, ...
The document recognition community has also build up a lot of expert knowledge on all the different processes that can happen after a document creation like scanning artefacts, document aging, paper deformations, ...
A huge amount of work has been done in creating tools that can produce realistic documents (e.g.: DocCreator).
While all the work done is invaluable for the community, this approach can not guarantee that a deep learning model trained on synthetic data will be able to transfer correctly to real data.
It is also a very complex task to identify the variables that condition the synthetic generation process of a document that will also significantly impact a deep learning model and its application to real documents.

Here, we propose to simplify this process with a method that will be able to seamlessly 



``` {.table caption="Dataset labels and description"}
Name, Image Example, Description, Origin, Annotation types
Synthetic Typeset Dataset, ![](ressources/synthetic-typeset-dataset.pdf), Synthetic dataset produced using music typesetting software, MuseScore, Automatic
Real Accidental Dataset, ![](ressources/real-accidental-dataset.pdf), Real dataset manually annotated, IMLSP, Manual
Isolated Music Symbol Dataset, ![](ressources/isolated-music-symbol-dataset.pdf), Isolated music symbols extracted from real historical music scores, IMLSP, Manual
Synthetic Single Symbol Dataset, ![](ressources/synthetic-single-symbol-dataset.pdf), Synthetic dataset produced using isolated music symbols, Isolated Music Symbol Dataset, Automatic
```

``` {.table caption="Parameter grid using bootstrapped real accidental dataset"}
bootstrapping, 
```

### Music Score Typesetting Software Synthetic Data-Based Detection

Classical approach to DL-based detection without manual annotations.

1. Use domain specific complex synthetic data generation strategy
    * For music scores: software generated scores
2. Train DL detectors using synthetic data
3. Apply trained detector on real non-annotated images

![Training Deep Learning Detector with Synthetic Data](ressources/dl-detector-synthetic-data.pdf)

Synthetic Dataset: Gather as much software generated music scores as possible

* Data sources:
    * MuseScore Database
    * PDF: lilypond, closed-source typesetting software
* Common dataset representation: piff format
* Document-based data augmentation: Labri DocCreator

Real Dataset:

* noisy, dense historical orchestral music scores with modern notation
* Significant amount of page wise manual annotations required for evaluations

Detectors: Faster R-CNN, RFCN, SSD pre-trained on ImageNet

Experiments:

* Train detectors using synthetic data
* Compared different trained detector architectures on real dataset
    * Evaluate using manual annotations

### GAN-based Music Symbol Detection

#### Synthetic Dataset

Problem: Complex generation strategy

* Contains biased specific to each generation strategy
* Need to be specific to the type of document to recognize

Goal: Use simple generation strategy

* Remove the need of complex synthetic generation strategy
* Simplest synthetic data generation strategy for symbol detection
    * Single isolated symbol of random size and position in blank canvas
* Requirement
    * Dataset of isolated music symbols annotated with their class
    * Use of real isolated music symbols adds variety to the symbol shapes

![Synthetic image generated using real isolated music symbols](/home/kwon-young/Seafile/thomas phd/CSID_2019/presentation/img/synthetic_data.jpg "synthetic data")

#### Synthetic Symbol Detection

Train sota detector on synthetic dataset.

Goal:

* Detect symbols in generated images from GAN
* Used to evaluate mAP results of a trained GAN

#### Real Dataset

Dataset:

* Use syntactical method to define music symbol detection task
    * Define regions to be used for detection
    * Semi-automatic generation of data annotation
* Fully annotated small scale accidental dataset
    * Used for preliminary experiments
    * Only contains 3 accidental classes + rejection
    * Validate GAN framework proof of concept
        * U-Net GAN training with real/synthetic data
        * Additional generator loss
        * Early-stopping with reduced validation set
* Partially annotated full scale dataset of dense, complex, historical orchestral/piano music scores
    * Used for final validation of our method
    * Compare with concurrent approach of using synthetic data generated from typesetting software
    * Extend symbol class set (note heads, flags, keys, silences, attack signs, ...)
    * Annotate few pages of real music scores to evaluate the approach

#### Preliminary Experiments with Fully Annotated Small Scale Accidental Dataset

Goal: Proof of concept of the GAN approach

Experiment 1:

* Goal: Show that a GAN can translate real images of music score into a synthetic image
* Dataset:
    * Generator: Fully annotated small scale accidental dataset
    * Discriminator: Single class synthetic dataset (sharp or flat or natural)
* Architecture: Original U-Net architecture
* Experimental settings:
    * Grid-search on learning rates for discriminator/generator
* Evaluation:
    * Stability of the GAN during training
    * Compare generated images with synthetic images
    * Pixel IoU results to evaluate image generation quality
    * Baseline detection results in mAP with IoU > 0.75

![Experiment 1: U-Net GAN training to transform real images into synthetic images](ressources/expe1-unet-gan-training.pdf "GAN training expe1")

![Experiment 1: U-Net GAN detection performance evaluation using pre-trained detector](ressources/expe1-unet-gan-eval.pdf "GAN eval")

Experiment 2:

* Goal: Show benefit of additional generator loss using synthetic data
* Dataset:
    * Generator:
        * Fully annotated small scale accidental dataset for GAN loss
        * Multi-class synthetic dataset (accidentals + rejection)
    * Discriminator: Single class synthetic dataset (sharp or flat or natural)
* Architecture: Original U-Net architecture + generator loss using synthetic data
* Experimental settings:
    * Grid-search on learning rates for discriminator/generator
* Evaluation: Compare results with experiment 1
    * Compare generated images with synthetic images
    * Pixel IoU results to evaluate image generation quality
    * Detection results in mAP with IoU > 0.75

![Experiment 2: Improve U-Net GAN training using additional generator loss with synthetic data](ressources/expe2-unet-gan-training.pdf "GAN training expe2")

Experiment 3:

* Goal: Evaluate the use of a reduced manually annotated validation set for early-stopping of GAN training
* Dataset:
    * Training:
        * Generator:
            * Fully annotated small scale accidental dataset for GAN loss
            * Multi-class synthetic dataset (accidentals + rejection)
        * Discriminator: Single class synthetic dataset (sharp or flat or natural)
    * Validation:
        * Fully annotated small scale accidental dataset for GAN loss
            * Reduced dataset with a few examples per class (10, 20, ...)
* Architecture: Original U-Net architecture + generator loss using synthetic data
* Experimental settings:
    * Use best parameters from experiment 2
* Evaluation: Compare results with experiment 2
    * Detection results in mAP with IoU > 0.75
    * Compare results between using validation set and training set for early-stopping

Figure of experiment 3

#### Experiments with Partially Annotated Full Scale Dataset

Experiment:

* Goal: Show real use-case of the method on a big dataset of non-annotated music score
* Dataset:
    * Training:
        * Generator:
            * Partially annotated full scale dataset
            * Multi-class synthetic dataset
        * Discriminator: Single class synthetic dataset
    * Validation:
        * Partially annotated full scale dataset
            * Reduced dataset with a few examples per class
* Architecture: Original U-Net architecture + generator loss using synthetic data
* Experimental settings:
    * Use previously found best learning rate parameters
    * Symbol class to detect (accidentals, noteheads, attack signs, ...)
* Evaluation: 
    * Detection results in mAP with IoU > 0.75
    * Show results for each symbol class
    * Compare results with Music Score Typesetting Software Synthetic Data-Based Detection results
        * Apply detector pre-trained on typesetting software synthetic data on full scale dataset of real music scores
        * Make class-wise comparison with GAN results

Figure of experiment
