"""
File: format.py
Author: Kwon-Young Choi
Email: kwon-young.choi@irisa.fr
Date: 2020-02-05
Description: Format the labels of chapter, section, subsection and
subsubsection of a multi-file latex project.
The label will contain its parent labels and be shorten with the least
possible amount of words to distinguish it from other titles from the same
levels.
If the script modified something it will return with exit code 1.
If it found nothing, it will return with exit code 0.
Use with caution as it probably contains bug and overwrite your tex files!!!
"""
import sys
import argparse
import warnings
import autodebug


def make_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("input_texfile")
    args = parser.parse_args()

    return args


def content(line):
    # find the content in the first pair of {}
    line = line[line.find(']') + 1:]
    name = line[line.find('{') + 1:line.rfind('}')]
    if name.startswith('\\textcolor'):
        name = name.split(': ')[1]
    return name


def contents(line, command):
    # find all content in all pairs of {}
    labels = []
    for token in line.split(command)[1:]:
        label = token[:token.find('}')]
        labels.append(label)
    return labels


prefix_level = {
    'root': -1,
    'chap': 0,
    'sec': 1,
    'ssec': 2,
    'sssec': 3,
    'par': 4,
}


def new_node(lvl, title, label, parent):
    """ Create a new title node
    A node has a level (chapter, section, ...), a latex title, a label and a
    link to its children and parent.
    """
    return {'lvl': lvl, 'title': title, 'label': label, 'children': {},
            'parent': parent}


def make_label(name):
    label = name.lower().replace(' ', '_').replace('-', '_').replace(':', '')
    label = label.replace('\\', '').replace('{', '').replace('}', '')
    return label


def collect_hierarchy(filename, hierarchy, cpt=0):
    """ Recursively process tex files to construct a hierarchy of chapters,
    section, subsection and subsubsection.
    The hierarchy is a bidirectional tree as each nodes has a link to its
    children and parent.
    Children are named with their latex title prefixed with a unique number.
    """
    cur_hierarchy = hierarchy
    
    def set_cur_hierarchy(lvl):
        nonlocal cur_hierarchy
        if prefix_level[lvl] - 1 > prefix_level[cur_hierarchy['lvl']]:
            cur_key = [key for key in cur_hierarchy['children'].keys()][-1]
            cur_hierarchy = cur_hierarchy['children'][cur_key]
            return
        while True:
            if prefix_level[lvl] - 1 < prefix_level[cur_hierarchy['lvl']]:
                cur_hierarchy = cur_hierarchy['parent']
            else:
                break

    with open(filename) as f:
        for i, line in enumerate(f.readlines()):
            lvl = None
            if line.startswith('\\input'):
                newfilename = content(line)
                cpt = collect_hierarchy(newfilename, hierarchy, cpt)
            elif line.startswith('\\chapter{') or \
                    line.startswith('\\chapter*{') or \
                    line.startswith('\\chapter['):
                lvl = 'chap'
            elif line.startswith('\\section{') or \
                    line.startswith('\\section[') or \
                    line.startswith('\\section*{'):
                lvl = 'sec'
            elif line.startswith('\\subsection'):
                lvl = 'ssec'
            elif line.startswith('\\subsubsection'):
                lvl = 'sssec'
            elif line.startswith('\\paragraph'):
                lvl = 'par'
            if lvl:
                # lvl between chap and sssec should increment 1 by 1
                # but paragraphs can happen in any lvl.
                if prefix_level[lvl] < 4 and \
                        prefix_level[lvl] - 2 > prefix_level[cur_hierarchy[
                            'lvl']]:
                    message = f"{filename}:{i+1}: Bad hierarchy"
                    raise RuntimeError(message)
                set_cur_hierarchy(lvl)
                name = content(line)
                label = make_label(name)
                cur_hierarchy['children'][str(cpt) + name] = new_node(
                    lvl, name, label, cur_hierarchy)
                cpt += 1
    return cpt


def shorten_labels(labels):
    """ Iteratively shave off words until two labels become identical or only
    one word is left.
    """
    list_labels = {key: label.split('_') for key, label in labels.items()}
    continue_list = [True for _ in labels]
    new_labels = labels.copy()
    while any(continue_list):
        for i, (key, list_label) in enumerate(list_labels.items()):
            if len(list_label) == 1:
                continue_list[i] = False
                continue
            list_labels[key].pop()
            new_label = '_'.join(list_labels[key])
            for j, other in enumerate(new_labels.values()):
                if i == j:
                    continue
                if new_label == other:
                    continue_list[i] = False
                    break
            else:
                new_labels[key] = new_label

    # if two titles are the same and have only one word
    cpt = 1
    for key in new_labels:
        for prev_key in new_labels:
            if prev_key == key:
                break
            if new_labels[key] == new_labels[prev_key]:
                cpt += 1
                new_labels[key] += str(cpt)
    for i, l1 in enumerate(new_labels.values()):
        for j, l2 in enumerate(new_labels.values()):
            if i != j:
                assert l1 != l2, f"{l1} == {l2}"
    return new_labels


def shorten_hierarchy(hierarchy):
    """ Use shorten_labels to shorten labels of a hierarchy.
    """
    shortened_labels = {}
    for key, child in hierarchy['children'].items():
        shortened_labels[key] = child['label']
    shortened_labels = shorten_labels(shortened_labels)
    for key, label in shortened_labels.items():
        hierarchy['children'][key]['label'] = label
    for child in hierarchy['children'].values():
        shorten_hierarchy(child)


def gen_full_labels(hierarchy):
    """ Create full labels for each node of the hierarchy as they
    will appear in the final latex output.
    full_label structure: lvl:prefix:label
    lvl is the title level (chap, sec, ...)
    prefix is the label of the parent
    label is the shortened standardized (see make_label) form of the title
        node.
    """
    for name, child in hierarchy['children'].items():
        prefix = hierarchy['prefix']
        prefix += ':' if prefix != '' else ''
        prefix += child['label']
        child['prefix'] = prefix
        full_label = child['lvl'] + ':' + prefix
        child['full_label'] = full_label
        gen_full_labels(child)


def hierarchy_generator(hierarchy):
    """ Iterate the hierarchy as they appeared in the latex files
    """
    for name, child in hierarchy['children'].items():
        yield (child['title'], child)
        yield from hierarchy_generator(child)


def replace_labels(filename, hierarchy_iterator, replaced_labels):
    """ Replace label of a title or add if label is not present.
    """
    # replace_flag is True: found a new title
    # replace_flag is False: replaced a label
    replace_flag = False
    add_flag = False
    lines = []
    current_title_line = 0
    line_number = 0
    replaced_something = 0

    with open(filename) as f:
        for line in f.readlines():
            if line.startswith('\\input'):
                newfilename = content(line)
                replaced_something |= replace_labels(
                    newfilename, hierarchy_iterator, replaced_labels)
            elif replace_flag and line == "\n":
                replace_flag = False
            elif line.startswith('\\label{') and replace_flag:
                # found label directive, replace label
                label = content(line)
                new_label = cur_hierarchy['full_label']
                if label != new_label:
                    msg = f'{filename}:{line_number}: replace {label} with {new_label}'
                    warnings.warn(msg)
                    replaced_labels[label] = new_label
                    line = '\\label{' + new_label + '}\n'
                    replaced_something = 1
                replace_flag = False
                add_flag = False
            elif line.startswith('\\chapter{') or \
                    line.startswith('\\chapter*{') or \
                    line.startswith('\\chapter[') or \
                    line.startswith('\\section{') or \
                    line.startswith('\\section[') or \
                    line.startswith('\\section*{') or \
                    line.startswith('\\subsection') or \
                    line.startswith('\\subsubsection') or \
                    line.startswith('\\paragraph'):
                if add_flag:
                    # did not found label directive, add label
                    new_label = cur_hierarchy['full_label']
                    msg = f'{filename}:{current_title_line + 1}: add {new_label}'
                    warnings.warn(msg)
                    new_line = '\\label{' + new_label + '}\n'
                    lines.insert(current_title_line + 1, new_line)
                    line_number += 1
                    replaced_something = 1
                cur_name, cur_hierarchy = next(hierarchy_iterator)
                name = content(line)
                assert name == cur_name, f'{name} != {cur_name}'
                current_title_line = line_number
                replace_flag = True
                add_flag = True
            lines.append(line)
            line_number += 1
    if replace_flag or add_flag:
        # last title did not have a label directive
        new_label = cur_hierarchy['full_label']
        msg = f'{filename}:{current_title_line + 1}: add {new_label}'
        warnings.warn(msg)
        new_line = '\\label{' + new_label + '}\n'
        lines.insert(current_title_line + 1, new_line)
        line_number += 1
        replaced_something = 1
    with open(filename, 'w') as f:
        f.write(''.join(lines))
    return replaced_something


def replace_refs(filename, replaced_labels):
    """ Replace references to labels that we just changed.
    """
    lines = []

    with open(filename) as f:
        for i, line in enumerate(f.readlines()):
            if line.startswith('\\input'):
                newfilename = content(line)
                replace_refs(newfilename, replaced_labels)
            elif '\\ref{' in line or '\\cref{' in line or '\\Cref{' in line \
                    or '\\vref{' in line or '\\Vref' in line:
                # found label directive, replace label
                for label in contents(line, "ref{"):
                    if label in replaced_labels:
                        new_label = replaced_labels[label]
                        msg = f'{filename}:{i}: replace {label} with {new_label}'
                        warnings.warn(msg)
                        line = line.replace(label, new_label)
            lines.append(line)
    with open(filename, 'w') as f:
        f.write(''.join(lines))
    return


def print_hierarchy(hierarchy):
    prev_lvl = None
    for name, cur_hierarchy in hierarchy_generator(hierarchy):
        if cur_hierarchy['lvl'] != 'par':
            prev_lvl = prefix_level[cur_hierarchy['lvl']]
        else:
            prev_lvl += 1
        print("\t" * prev_lvl, cur_hierarchy['lvl'], name)
        print("\t" * prev_lvl, "lab:", cur_hierarchy['label'])


def main(args):
    hierarchy = new_node('root', 'root', None, None)
    collect_hierarchy(args.input_texfile, hierarchy)
    # print_hierarchy(hierarchy)
    # __import__('pdb').set_trace()
    shorten_hierarchy(hierarchy)
    # print_hierarchy(hierarchy)
    hierarchy['prefix'] = ''
    gen_full_labels(hierarchy)
    # print_hierarchy(hierarchy)
    replaced_labels = {}
    exit_code = replace_labels(
        args.input_texfile, hierarchy_generator(hierarchy), replaced_labels)
    replace_refs(args.input_texfile, replaced_labels)
    sys.exit(exit_code)


if __name__ == "__main__":
    main(make_args())
