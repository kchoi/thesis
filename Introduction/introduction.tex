\chapter*{Introduction}
\label{chap:introduction}
\addcontentsline{toc}{chapter}{Introduction}
\chaptermark{Introduction}

%Introduce what is a music score.

%\begin{itemize}
  %\item Written form of music
  %\item To archive music and save a record through time
  %\item To share music with other persons
  %\item A composer write music scores
    %\begin{itemize}
      %\item Manually or through music typesetting software
    %\end{itemize}
  %\item Publisher reformat, replicates and mass-produces music scores
  %\item Musician reads music scores
  %\item Musicologist, historian studies music scores
%\end{itemize}

A document can be defined as a human artifact which purpose is to preserve and share knowledge.
Music score documents is a specialized type of documents transcribing the act of playing music and aims to capture pitch, rhythm, intensity and many other aspects of music.
The specific case of music scores as a document is very interesting by its unending goal of trying to capture in writing something as ephemeral and emotional as music.
Music notation, like any documents, has both the purpose of preserving a music piece as good as possible, capturing as many details as possible, while being easy to read in real time by musicians during a performance.
Moreover, different musical cultures and traditions expressed different needs for music notations, as well as changing needs through time because of the evolution of music.
As a result, we can observe widely different music notations, going from early music notation to the modern music notation in Europe, while Korea, China and India, to only name a few, developed different music notations to transcribe traditional music.
In this work, we focus on the modern music notation, which is now the most commonly used music notation throughout the world.
However, this music notation has a long history in Europe, starting from the beginning of the 18th century.
The printing of the music scores using the modern notations could not be done anymore using movable music types because of the structural complexity of the notation, where the relative placement of symbols could be much less linear than the early music notations.
Thus, a new kind of imprinting technique was developed where the music score would first be manually engraved on copper plates, and then be used in printing press to replicate the music score page on sheets of papers.
While composers would write music manually, a large amount of music scores were printed and are now precious historical documents that are studied by many musicians and musicologists.
Unfortunately, it is often hard to access these documents because the hard copy is often restricted and the digitalization of the music scores only gives images for musicians and musicologists to study.

The modern music notation consists of a collection of staves which are five vertically stacked horizontal lines.
These five lines serves as a 2 dimensional axis to describe both the flow of time, by reading from left to right and the pitch of musical notes, by the vertical placement of note heads.
Musical notes are laid out on the staves from left to right at different vertical position and transcribes a music piece or melody.
%Introduce what is Optical Music Recognition (OMR) and what are its uses.

%\begin{itemize}
  %\item OMR is the inverse process of producing a music score
  %\item OMR is the process of reading a score, automating the process with the help of a computer
  %\item Practically, it is the process of recovering the music notation in an image of a music score
  %\item It can be used by
    %\begin{itemize}
      %\item Composers or publishers to automatically recognize handwritten compositions
      %\item Musicians to recover the music notation from a scan or pdf of a music score for the purpose of modifying the music score
      %\item Musicians to improve the usability of music scores through new technology like smart phones or tablets with features like automatic page turning
      %\item Musicians with disabilities to transform a music score into a more suitable form like a braille music score for blind musicians.
      %\item Musicologists or historians to study large collections of music scores by doing automated queries at the music notation level
    %\end{itemize}
%\end{itemize}

Optical Music Recognition (OMR) is the automatic process of turning images of music scores into a machine-readable format, allowing easier use and study of those historical printed scores.
This subfield of document recognition research has been studied since the 1960s, starting from the work of \textcite{pruslin_automatic_1966} and is still actively researched.
Well-known OMR literature review like \textcite{fornes_analysis_2014, rebelo_optical_2012} segments the OMR workflow as a succession tasks.
This workflow often starts with various low-level graphical processing that are often common to a lot of document recognition workflow like binarization and noise removal.
Then staff lines are recognized and optionally removed, symbols are detected 
However, the task of recognizing a historical printed score is very challenging because of the combination of a very complex music notation with high symbol density and degradations due to the printing method and age of the document.
While classical OMR systems always struggled to recognize historical printed music score documents, the OMR research has seen a renewed interest recently due to novel computer vision architecture based on Deep Learning models.
The early stages of OMR such as music symbol detections being inherently a computer vision task, we demonstrate in this work how Deep Learning models can produce highly accurate and precise music symbol detections.
One of the downside of classical fully supervised Deep Learning models and methods is the needs for large amount of annotated data to train the models.
This need for large amount of annotated data is problematic, especially in OMR, where no such datasets exists for historical printed music scores outside of datasets produced during the course of this work.
Therefore, the heart of this work is to propose a new unsupervised method called \Method{} able to produce highly accurate symbol detection without using any manual annotations for the training of Deep Learning models and only use a simple set of isolated music symbols.

To summarize, in \cref{chap:state}, we expose the difficulties of correctly recognizing a historical printed music scores because of the printing techniques and degradations of old documents.
Then we present an overview of OMR and its different processing steps, with a focus on the step of music symbol detection.
Finally, we present a review of relevant Deep Learning methods that we will use throughout this work, starting from fully/weakly supervised detectors to generative models like GANs.

Then, we proceed in \cref{chap:supervised} onto using fully supervised Deep Learning music symbol detector to accurately detect music symbol for an OMR pipeline.
We present results on two different datasets and tasks: first, a small and constrained accidental detection task on a newly constituted dataset using 4 different detectors to give a sense of speed versus accuracy of the task of detecting music symbols.
Secondly, we apply fully supervised state-of-the-art detectors on the handwritten music score dataset called MUSCIMA++ and present results on a large set of music symbol classes.

In \cref{chap:learning}, we discuss the various possibility of using Deep Learning models without manual annotations.
We first discuss the possibilities of using synthetic data as commonly done in the document recognition field.
In our case, we chose to use a combination of synthetic data based only on isolated symbols and generative methods such as GANs so that we can train a music symbol detector in an unsupervised fashion while not relying on a complex synthetic generation method such as music score typesetting software.
The generative capability of our method also implies that our method will be able to adapt seamlessly to new corpus of documents.

This leads to \cref{chap:unsupervised} where we expose the design of our new \Method{} method for unsupervised music symbol detection.
We propose a three steps method with the general idea of gradually reducing the complexity of the detection task.
In the first step, we reduce the spatial search space using the DMOS syntactical method.
In the second step, we simplify the graphical representation using an image-to-image translation generative model.
This simplification consists of isolating symbols to detect in a white background and this target representation is synthesized using only a preexisting dataset of isolated music symbols and injected using a hybrid training objective.
This simplification consists of isolating all symbols to detect while erasing other symbols and noises to produce a white background.
This target representation is synthesized using only a preexisting dataset of isolated music symbols and injected using a hybrid training objective.
The third and final step is to detect the previously isolated symbols by using a detector pretrained using a synthetic isolated symbol detection dataset.

Finally, we proceed to show an extensive set of experiments in \cref{chap:experiments}.
Because we have the very challenging task of training an unstable generative model such as a GAN with no manually annotated ground truth, we started out our experiments with a simplified task and dataset to tune the common hyperparameters of our method.
Then, we gradually raise the difficulty of the task by detecting different types of symbols, as well as rarefy the amount of symbols to detect.
Then we validate our evaluation methodology when no ground truth data is present, which is the real use case of our method.
Finally, we evaluate our method using a much larger, more heterogeneous dataset, where symbols to detect are even less frequent.
We demonstrate the effectiveness of our generative method by comparing the results with an ablated non-generative version of our method and shows a massive improvement in precision.

In the end of this manuscript, we discuss in \cref{chap:method} the future work and possible improvements of our method.
We discuss possible strategies to stabilize and improve the training of our method.
We also believe that we can improve our method by taking larger images as input, a more diverse set of symbols to detect and could even be applied on handwritten music scores or other structured documents with significant segmentation problems such as electrical circuit design documents.
Finally, we believe that the work presented in this manuscript could be the first step towards entirely autonomous structured document recognition systems, where documents could be recognized in stages, with each stage processed entirely in an unsupervised manner using only isolated symbols and gradually building a syntactical structure of the document with each stage reusing and building on the symbols recognized on previous stages.
