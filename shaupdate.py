import os
import argparse
import hashlib


def make_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("input_file")
    parser.add_argument("shasum_file")
    args = parser.parse_args()
    return args


def sha256sum(filename):
    h = hashlib.sha256()
    b = bytearray(128*1024)
    mv = memoryview(b)
    with open(filename, 'rb', buffering=0) as f:
        for n in iter(lambda: f.readinto(mv), 0):
            h.update(mv[:n])
    return h.hexdigest()


def main(args):
    new_shasum = sha256sum(args.input_file)
    if os.path.exists(args.shasum_file):
        with open(args.shasum_file) as f:
            old_shasum = f.read()
        if new_shasum != old_shasum:
            print(f"updating {args.shasum_file}")
            with open(args.shasum_file, "w") as f:
                f.write(new_shasum)
    else:
        print(f"updating {args.shasum_file}")
        with open(args.shasum_file, "w") as f:
            f.write(new_shasum)
    return


if __name__ == "__main__":
    main(make_args())
