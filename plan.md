\AddToShipoutPicture{\BackgroundIm}
\lesoustitre{Sous-titre de la th\`{e}se}
\lieu{ Lieu  }
\uniterecherche{Unit\'{e} de recherche: }
\numthese{Th\`{e}se N°: 0}
\rapporteur{
  {
    \setlength{\baselineskip}{0.5\baselineskip} 
    \begin{description}
      \item Pr\'{e}nom Nom Fonction et \'{e}tablissement d'exercice
      \item Pr\'{e}nom Nom Fonction et \'{e}tablissement d'exercice
      \item Pr\'{e}nom Nom Fonction et \'{e}tablissement d'exercice
    \end{description}
  }
}
\jury{
  {
    \setlength{\baselineskip}{0.5\baselineskip} 
    %\hspace{0.8cm}
    \textcolor{mathSTIC-Color}{\textit{Attention, en cas d’absence d’un des membres du Jury le jour de la soutenance,  la composition ne comprend que les membres pr\'{e}sents }}
    \begin{description}
      \item Pr\'{e}sident: \hspace{0.75cm} Pr\'{e}nom Nom Fonction et \'{e}tablissement d'exercice
      \item Examinateurs: \hspace{0.2cm}Pr\'{e}nom Nom Fonction et \'{e}tablissement d'exercice
        %\item \textcolor{mathSTIC-Color}{Pr\'{e}sident \textit{ (\`{a} pr\'{e}ciser apr\`{e}s la soutenance)}}
      \item \hspace{2.53cm} Pr\'{e}nom Nom Fonction et \'{e}tablissement d'exercice
      \item \hspace{2.53cm}  Pr\'{e}nom Nom Fonction et \'{e}tablissement d'exercice
      \item \hspace{2.53cm} Pr\'{e}nom Nom Fonction et \'{e}tablissement d'exercice
      \item \hspace{2.53cm} Pr\'{e}nom Nom Fonction et \'{e}tablissement d'exercice
      \item \hspace{2.53cm} Pr\'{e}nom Nom Fonction et \'{e}tablissement d'exercice
    \end{description}
  }
}
\directeur{
  {
    \setlength{\baselineskip}{0.5\baselineskip} \hspace{0.2cm}Pr\'{e}nom Nom Fonction et \'{e}tablissement d'exercice
    % \vspace{0.2cm}
    %\begin{description}
    %\item \hspace{2.73cm}  Pr\'{e}nom Nom Fonction et \'{e}tablissement d'exercice
    %\end{description}
  }
}
\codirecteur{
  {
    \setlength{\baselineskip}{0.1\baselineskip}  \hspace{0.2cm}Pr\'{e}nom Nom Fonction et \'{e}tablissement d'exercice
  }
}
\invit{
  {
    \begin{description}
      \item Pr\'{e}nom Nom Fonction et \'{e}tablissement d'exercice
    \end{description}
  }
}
\maketitle
## Acknowledgement {#chap:acknowledgement .unnumbered .unnumbered}

Je tiens à remercier\
I would like to thank my parents..\
J'adresse également toute ma reconnaissance à ...\
...

\ClearShipoutPicture{}
\thispagestyle{empty}
\clearemptydoublepage{}
\selectlanguage{english}
\frontmatter 
## Introduction {#chap:introduction .unnumbered .unnumbered}

\addcontentsline{toc}{chapter}{Introduction}
\chaptermark{Introduction}
\clearemptydoublepage{}
\clearemptydoublepage{}
\clearemptydoublepage{}
## State-Of-The-Art {#chap:state}

### Introduction {#sec:state:introduction}

### Optical Music Recognition {#sec:state:optical}

Optical Music Recognition is the task of converting a music score
document into a machine readable format. This task is a well-known
problem in the Document Image Analysis and Recognition domain and has
been researched since the 1960s with [@pruslin_automatic_1966]. In this
section we review the field of OMR with a focus on symbol detection and
classification. We first present how the modern music notation is
formalized
in [\[ssec:state:optical:music\]](#ssec:state:optical:music){reference-type="ref+page"
reference="ssec:state:optical:music"} and present some music score
typesetting
softwares [\[ssec:state:optical:software\]](#ssec:state:optical:software){reference-type="ref+page"
reference="ssec:state:optical:software"} that can be used to produce
such document. We also present an overview of the existing datasets
in [\[ssec:state:optical:datasets\]](#ssec:state:optical:datasets){reference-type="ref+page"
reference="ssec:state:optical:datasets"} that can be used for the tasks
of classification and detection of music symbols. Finally, we present
in [\[ssec:state:optical:optical\]](#ssec:state:optical:optical){reference-type="ref+page"
reference="ssec:state:optical:optical"} an overview of the different
components of an OMR workflow.

#### Music Notation {#ssec:state:optical:music}

Presents in details all the music notation notions needed to understand
this thesis.

-   kinds of music notations: ancient, modern, ...

-   overall structures of modern music notation: systems, staves, bars

-   beginning of a staff: key, armors, time signature

-   elements that can be present in a bar: notes with all their related
    symbols

#### Software {#ssec:state:optical:software}

Explain the process of editing historical scores of the 19th, 20th
century: editors, typesetters, ... Explain modern process of editing
scores with Music Typesetting software like MuseScore and Sibelius.

List OMR software.

#### Datasets {#ssec:state:optical:datasets}

-   MUSCIMA++ [@hajic_muscima++_2017]

-   Synthetic datasets,

#### Optical Music Recognition Stages {#ssec:state:optical:optical}

OMR studies by [@fornes_analysis_2014] or [@rebelo_optical_2012]
typically present the OMR workflow as multiple consecutive stages: image
pre-processing, staff detection with possible removal, music symbol
segmentation/classification and finally music notation reconstruction.
However, many works reorganize, merge or remove some of these stages.

##### Preprocessing {#sssec:state:optical:optical:preprocessing}

Preprocessing of music score document images:

-   binarization

-   ...

Existing work in OMR tends to use common document pre-processing
operations. Binarization is used to isolate connected components from
the background, and often score pages are skew-corrected and have noise
removal applied.

##### Staff Lines Detection and Removal {#sssec:state:optical:optical:staff}

Next, staff line detection has been performed using combinations of
filters, pixel projection profiles, run-length analysis, contour-line
tracking and graph path search. The height between two staff lines is an
important feature in OMR, and this *interline distance* is estimated for
later use. Recent work like [@calvo-zaragoza_staff-line_2017] has used
Convolutional Neural Networks (CNNs) to do pixel-wise classification to
locate staff lines.

In this work, we use a graph-based Kalman filtering method
[@dandecy_kalman_1994] to detect and remove staves from the original
image and is able to process broken or curved lines accurately. The
symbol detection methods that we present are generally robust to
remaining staff removal artifacts.

##### Music Symbol Detection {#sssec:state:optical:optical:music}

Music scores are constructed using a lot of relatively simple shapes
like lines and blobs in a complex bi-dimensional structure. This fact
has pushed OMR systems to use simple extraction algorithm like graphical
primitive detection or connected components, and then use complex adhoc
rules to merge or over-segment primitives [@fornes_analysis_2014]. The
classification of music symbols can be done using a variety of
techniques like simple filters, template matching or classifiers like
HMM, neural network, K-NN and SVM as presented
in [@rebelo_optical_2009].

More recently, convolutional-based neural network
detectors [@pacha_handwritten_2018] that merge the segmentation and
classification steps have been applied to a variety of dataset like the
newly annotated handwritten dataset of modern music, the MUSCIMA++
dataset [@hajic_muscima++_2017] or on mensural music scores
by [@pacha_optical_2018]. Fully convolutional neural networks have also
been used by [@hajic_towards_2018] and [@tuggener_deep_2018] which
allows for pixel wise segmentation of music symbols.

##### Music Notation Reconstruction {#sssec:state:optical:optical:music_notation}

Finally, the last step of the recognition process is to reconstruct the
music notation and validate the structure produced.
[@rebelo_optical_2012] shows that because of the strong structure and
graphical rules of music notation, it makes sense to model this
organization using a grammar. Most of these methods are used at the end
of the OMR pipeline, to check the validity of the recognized music
structure. One exception is the DMOS method described
in [\[ssec:state:optical:the\]](#ssec:state:optical:the){reference-type="ref+page"
reference="ssec:state:optical:the"}, where the grammar drives both the
recognition and validation of the structure produced.

We believe that low-level symbol segmentation problems caused by the
density, noise or pre-processing of a music score
(see [\[fig:hard\_seg\]](#fig:hard_seg){reference-type="ref+page"
reference="fig:hard_seg"}) should not be part of the grammar for an OMR
system, as it is too complex to be modeled explicitly. We wish to devise
a method that delegates the segmentation task to a statistical model, in
our case a Convolutional Neural Network (CNN) designed to do both
segmentation and classification. We developed our method with the goal
that it could be applied to any kind of structured document.

\centering
\subfloat[Touching Symbols]{
    \fbox{\includegraphics[width=0.4\linewidth]{ressources/touching_symbols.jpg}}}
\hfill
\subfloat[Broken Symbols]{
    \fbox{\includegraphics[width=0.4\linewidth]{ressources/broken_symbols.jpg}}}
##### Full-Pipeline OMR {#sssec:state:optical:optical:full}

#### The DMOS Syntactical Method {#ssec:state:optical:the}

The DMOS syntactical method was introduced by [@couasnon_dmos_2001], and
is a general off-line method for recognizing structured documents. The
first version of the system included a grammar for musical scores. DMOS
uses attributed two-dimensional grammars to define the symbolic and
graphical representation of documents, producing constituent parse
trees. The contextual information produced by the grammar can also be
used to restrict the search space of our detector, as explained
in [\[sssec:supervised:hybrid:grammatical:music\]](#sssec:supervised:hybrid:grammatical:music){reference-type="ref+page"
reference="sssec:supervised:hybrid:grammatical:music"}.

The hierarchical graphical structure produced, for example a simple
music note as illustrated
in [\[fig:grammar\]](#fig:grammar){reference-type="ref+page"
reference="fig:grammar"}, is described by a set of rules that can search
through the use of backtracking and check the coherence of different
note elements. This ability to pinpoint inconsistencies can be used to
efficiently produce semi-annotated data by reducing the amount of manual
verification. Although the grammar is tailored to deal with complex
polyphonic orchestral scores, segmentation had to be addressed using
dedicated rules, which are difficult to produce and maintain. This
detection of music symbol is the task we are proposing to resolve using
Convolutional Neural Network-based detectors.

\centering
\subfloat[Stem]{
    \fbox{\includegraphics[width=0.20\linewidth]{ressources/grammar_stem}}}
\hfill
\subfloat[Note~head]{
    \fbox{\includegraphics[width=0.20\linewidth]{ressources/grammar_notehead}}}
\hfill
\subfloat[Accidental]{
    \fbox{\includegraphics[width=0.20\linewidth]{ressources/grammar_accidental}}}
\hfill
\subfloat[Alignement]{
    \fbox{\includegraphics[width=0.20\linewidth]{ressources/grammar_accidental_notehead}}}
### Supervised Object Detection {#sec:state:supervised}

The first focus of our work is to propose a better way to detect music
symbol using state-of-the-art object detectors. The second is to provide
an unsupervised detection method using GAN.

In this work, we use two different approaches to detect a single
accidental using a small labeled training sample:

a novel Spatial Transformer-based network, and

state-of-the-art general object detectors (Faster R-CNN, R-FCN, and SSD)
using transfer learning.

#### Spatial Transformer Network {#ssec:state:supervised:spatial}

Quickly present the fact that the STN is not technically a detection
mechanism but an attention mechanism where the original goal was to
improve the classification performance of the model. However, its
side-effect of producing an explicit localization region can be used as
a detection mechanism.

First, we use a simple convolutional neural network (CNN) architecture
based on Spatial Transformer (ST) networks proposed by
[@jaderberg_spatial_2015]. The ST network is composed of two stacked
CNNs: a localization network and a classification network. The
localization network has the task to output a 2D affine transformation
for a given input image. The Spatial Transformer Layer applies this
transformation to the input image, that will be then fed to the
classification network. In the original work
of [@jaderberg_spatial_2015], the ST was intended as an attention model
and not as a localization model. However, in our context of single
symbol detection, we view this affine transformation as an opportunity
to build a very simple music symbol detector.

\todo[inline]{Recurrent STN}
#### Faster R-CNN {#ssec:state:supervised:faster}

The Faster R-CNN [@ren_faster_2015] is one of the pioneer object
detection architectures in Deep Learning and is now widely used in very
diverse tasks. The detection process happens in two steps. First, in a
Region Proposal Network (RPN) stage, a feature extractor (VGG-16 or
resnet 101) is used to process input images. Then, at some intermediate
layer of the feature extractor, anchor boxes are used in a sliding
window manner to predict class agnostic box proposals. This RPN is
trained using a multi-loss function taking into account both
localization and objectness score produced by the RPN. Secondly, some of
these proposals (usually 300) are cropped from the feature layer used to
predict them, and the rest of the feature extractor is processed. Unlike
the RPN stage, the second stage outputs class-specific bounding boxes
refinement for each of the proposals. Finally, a similar multi-task loss
is used to optimize the second stage detection.

#### R-FCN {#ssec:state:supervised:r}

The R-FCN detector proposed by [@dai_r-fcn_2016] is an adaptation of the
Faster R-CNN architecture designed for even faster detection. While the
Faster R-CNN avoids a lot of computation by sharing a single network for
both RPN and full detection stages, it still needs to process each
region proposal until the end of the feature extractor. That is why the
R-FCN architecture proposes to extract region proposals only at the last
layer of the feature extractor and therefore reduces the amount of
computation for each proposal. They also propose a position-sensitive
cropping mechanism using position-sensitive score maps in order to
retain the localization information for each proposed region. R-FCN is
much faster than the Faster R-CNN, while maintaining comparable
accuracy.

#### Single Shot Detector {#ssec:state:supervised:single}

The third object detector we propose to use is the Single Shot Detector
(SSD) [@liu_ssd_2016]. Unlike the Faster R-CNN and R-FCN that use two
stage predictions, the SSD architecture predicts directly class and
bounding boxes of objects from a single pass of the feature extractor.
This model is typically significantly faster than two stage detectors
like Faster R-CNN and R-FCN.

#### Weakly-Supervised Detection {#ssec:state:supervised:weakly}

\todo[inline]{Change paper to present WSD}
Although large scale detection dataset already exists for popular
application domain like object detection in photographic images, the
high cost of creating such large datasets for less popular domain is
driving the research for weakly-supervised detection models. Formally,
weakly-supervised object detection is a task where only images and
image-level class annotations are provided for training.

The work of [@inoue_cross-domain_2018] proposes an improved
weakly-supervised detection model by using cross-domain adaption of a
pretrained detection model. In this task, we consider both a source
domain and a target domain, where the source domain is fully annotated
with classes and bounding boxes annotation at the instance-level.
However, the target domain only contains image-level annotations and its
class set is a subset of the source domain class set. The proposed model
is a fully supervised detector model, pretrained on the source domain,
which is then successively fine-tuned using two synthetically generated
samples. The first synthetic samples are images of the source domain
which are transfered to the target domain using a Cycle-GAN model. The
detector is therefore fine-tuned using synthetically generated images
and annotations from the source domain. In a second time, the detector
is fine-tuned by using a pseudo-labeling of the target domain images
where synthetic bounding boxes are generated by using a combination of
the detector output and image-level annotations.

Weakly supervised detection tasks implies that image-level class
annotations of the target domain are used to train a weakly supervised
model. However, in our case, we do not have any image-level annotations
of the target domains which are historical printed music scores. This
means that we can not use directly weakly supervised detector for our
task. In another hand, the use of a GAN model to cross the boundaries
between images of different domains does not requires any annotations
and this is why we also use such GAN model in our method.

[@bai_finding_2018] [@souly_semi_2017]

### Semantic Segmentation

#### Fully Convolutional Network

#### U-Net {#sssec:state:generative:u:u}

The U-Net model proposed by [@ronneberger_u-net:_2015] is designed for
task of segmenting biomedical images by labeling each pixels of an
image. Starting from a fully convolutional network (FCN), the U-Net adds
a symmetric expanding path which is able to up-sample each of the
feature maps produced by the corresponding level of the FCN. The
contracting path (FCN) is therefore able to capture contextual
information and build the relevant feature extractor for the task while
the symmetric expanding path is able to produce a precise localization
by directly reusing the FCN extracted features. This work also emphasize
the use of data augmentation in order to efficiently train a U-Net model
with very few annotated examples.

##### dhSegment {#sssec:state:generative:u:dhsegment}

The U-Net architecture has been successfully applied to document
processing tasks [@ares_oliveira_dhsegment_2018] like page extraction,
baseline extraction, layout analysis, illustration and photograph
extraction. This work emphasize the use of a generic CNN-based
architecture for different document processing tasks followed by tasks
specific post-processing like thresholding, morphological operations,
connected components analysis and shape vectorization. Although pixel
level annotations are hard to produce, the authors notes that the
generic U-Net model requires a small amount of manual annotations to be
successfully trained thanks to the pretraining of the U-Net architecture
on the ImageNet dataset [@deng_imagenet_2009]. Building on this
successful application of the U-Net architecture on document processing
tasks, we also propose to reuse this architecture in our unsupervised
detection method presented
in [\[ssec:unsupervised:isolated:architecture\]](#ssec:unsupervised:isolated:architecture){reference-type="ref+page"
reference="ssec:unsupervised:isolated:architecture"}.

### Generative Adversarial Network for Reducing Manual Annotations {#sec:state:generative}

GAN is essential to our unsupervised detection mechanism. Explain the
basic mechanism of how the GAN work.

#### Generative Adversarial Network {#ssec:state:generative:generative}

Generative Adversarial Network (GAN) introduced
by [@goodfellow_generative_2014] is a type of generative model which can
be trained using an adversarial training objective. This adversarial
training puts in competition a generator network and a discriminator
network where the generator needs to fool the discriminator and the
discriminator needs to identify data coming from the generator. This
mechanism can then be used to generate images that are
indistinguishable, in principle, from real images. Although the original
work managed to replicate small images of digits, faces and animals,
this type of adversarial training is known to be very unstable and
further modification of the loss objectives and architecture has been
proposed to use GANs for more complex tasks.

#### Data Augmentation using GAN

#### Cross-Domain Transfer using GAN

##### Weakly Supervised Training {#ssec:state:generative:weakly}

A very practical use of the GAN architecture is to use them jointly with
another fully supervised method to reduce the need for manually produced
annotations. Such approaches, also called weakly supervised approaches,
has been successfully applied to task like object instance segmentation
with the work of [@remez_learning_2018]. This work uses as input
informations produced by a pre-trained Faster R-CNN detection model in
order to train a GAN generator network to learn to segment a single
object previously detected. An adversarial training strategy is proposed
based on a cut-and-paste approach. The generator produces a mask of a
previously detected object by using features extracted by the Faster
R-CNN. This mask is then used to extract the object from its original
image and pasted into a background image. Then, the discriminator has to
differentiate between the original images and images produced using the
generator. In this instance, the use of a GAN architecture train with an
adversarial loss allowed to train a segmentation model without using any
manually annotated segmentation data. Instead, the model uses an
indirect source of information which is the output of a previously
trained detection model.

##### U-Net Generative Adversarial Network {#ssec:state:generative:u}

\todo[inline]{Explain paper on U-Net GAN}
##### Cycle-GAN {#ssec:state:generative:cycle}

The work by [@zhu_unpaired_2017] proposes to apply a modified version of
the original GAN model for the task of Image-to-Image translation.
Traditionally, the task of Image-to-Image translation needed datasets of
aligned pair of images where each images of the source domain had a
known corresponding image in the target domain. The use of a GAN model
allowed here to realize this task without having aligned pair of images.
However, instead of having only one generator and discriminator network,
the Cycle-GAN is using two generators and discriminators network where
each generator can produces images of the source and target domain and
each discriminator can identify images of the source and target domain.
Additionally, two cycle consistency losses are used to minimize the
difference between an image and its translation to and from the target
domain. This training mechanism allowed to train a generative model able
to transform an image from a source domain to an image resembling images
of the target domain while retaining properties that are common to both
the source and target domain.

#### Generative Adversarial Network Instability {#ssec:state:generative:generative_adversarial}

The main difficulty of using a GAN model identified by its original
author [@goodfellow_generative_2014] stems from having no explicit
representation of generator output and having to synchronize the
training between the discriminator and generator. When badly
synchronized, the training can lead to a collapse of the generator to a
single output. This instability has been since studied in works
like [@arjovsky_towards_2017] which identifies that one of the probable
source of instability of GAN models stems from the characteristics of
the loss function used to train a GAN model. Indeed, the use of a binary
cross-entropy loss can leads to a gradient vanishing issue, for example
in situation where the discriminator accept or reject all images coming
from the generator. The authors further expand its idea
in [@arjovsky_wasserstein_2017] where they propose to improve the
stability of the GAN training framework by mainly changing the loss
function by using the Wasserstein distance (also called Earch Mover
distance). This alternative loss function have the property to continue
to provide a useful gradient (as opposed to a null gradient for the
original binary cross-entropy loss) even when the discriminator is
trained to optimality. This reduces the impact of the training balance
between the discriminator and the generator.

Unfortunately, we did not have the time to integrate this work into our
own GAN model architecture but we believe that using a better loss
function such as the Wasserstein distance will improve the stability of
our approach.

### Conclusion {#sec:state:conclusion}

-   Show our first approach on supervised music symbol detection

    -   does not fulfill our goal of using no manual annotations

\clearemptydoublepage{}
## Supervised Music Symbol Detection {#chap:supervised}

This chapter treats about using supervised deep learning detectors for
music symbol detection. First part will be about using a combination of
supervised deep learning detectors and syntactical methods for small
scale accidental detection task. Second part is about a music symbol
detection approach only using big deep learning detectors.

### Optical Music Recognition on mid-18th to mid-20th Century Music Scores {#sec:supervised:optical}

Present dataset.

### Hybrid Approach to Accidental Detection {#sec:supervised:hybrid}

Task: Accidental Detection in Printed Historical Music Scores.

Use of combination of syntactical methods and Deep Learning Detectors.

#### Grammatical Description of Modern Music Notation {#ssec:supervised:hybrid:grammatical}

Present why using a bi-dimensional grammar to describe music notation is
a good idea.

-   Music scores are structured documents

-   Use of context

-   Semi-automatic production of ground-truth

Argue for the fact that we do not want to deal with segmentation
problems in the grammar. This should be done before the using the parser
generated by the grammar.

##### Music Notation Grammar {#sssec:supervised:hybrid:grammatical:music}

Describe the DMOS Music Grammar.

-   Primitive used: staff lines, vertical lines, horizontal lines,
    classified connected components as symbols like: note heads,
    accidentals, ...

-   Describe each music structure recognized:

    -   system of

        -   stave which are

            -   list of bars

    -   bar heading: keys, time signature, key signature

    -   bar contains:

        -   notes, chords, groups

            -   with accidentals

##### Detection Task Definition {#sssec:supervised:hybrid:grammatical:detection}

Dataset constitution using DMOS music grammar. Use context deduced from
the grammar to reduce the search space and ease manual annotations.
Context means: note head already detected. Specific task: detection of
accidentals. Look at the left of note heads, if there are no
accidentals, do rejection. To do rejection, detect the note head
instead.

##### Semi-Automatic Dataset Production {#sssec:supervised:hybrid:grammatical:semi}

Connected component classifier using a CNN symbol classifier trained on
isolated music symbols.

Manually correct data.

\centering
  **Label**                **Quantity**
  ------------------------ --------------
  No accidental (Reject)   968
  Natural                  968
  Sharp                    777
  Flat                     242
  **Total**                **2955**

  : Dataset produced by the DMOS pipeline driving a simple connected
  component based segmentation, a simple music symbol classifier and a
  manual check.[\[tab:dataset\]]{#tab:dataset label="tab:dataset"}

#### Bootstrapping Strategies {#ssec:supervised:hybrid:bootstrapping}

Small detection dataset.

Use bootstrapping to:

-   Scale up the dataset size

-   randomize uniformly the distribution location of symbols

-   balance the distribution of symbol classes

#### Spatial Transformer-Based Detector {#ssec:supervised:hybrid:spatial}

Explain architecture.

Localization network + ST layer + classification Network.

Use of contextual information with note head centroid information as
input.

#### Multiple ROI Object Detector Approach {#ssec:supervised:hybrid:multiple}

Present how the Faster R-CNN, R-FCN, SSD are specifically used for this
task.

##### Faster R-CNN {#sssec:supervised:hybrid:multiple:faster}

##### R-FCN {#sssec:supervised:hybrid:multiple:r}

##### SSD {#sssec:supervised:hybrid:multiple:ssd}

#### Results {#ssec:supervised:hybrid:results}

Present metrics with special case for rejection.

##### Detector Comparison {#sssec:supervised:hybrid:results:detector}

Faster R-CNN produced best results, while ST-based detector is the
fastest for reasonable precision.

##### Impact of Bootstrapping Techniques and Contextual Information {#sssec:supervised:hybrid:results:impact}

Show that bootstrapping and use of note head centroid as contextual
information has helped ST-based detector reach good performance.

#### Conclusion {#ssec:supervised:hybrid:conclusion}

First experiment with supervised DL detectors with a reduced search
space and reduced class set shows good results. How about large scale
with large class set dataset?

### Large Scale Supervised Music Symbol Detection {#sec:supervised:large}

Task is to demonstrate the use of state-of-the-art object detectors like
Faster R-CNN for the task of detecting music symbols in a less
constrained situation.

#### MUSCIMA++ Dataset for Handwritten Symbol Detection {#ssec:supervised:large:muscima++}

Present the design of the detection task using the MUSCIMA++ dataset.

Cropping of music scores along the staves. Filtering of data using area
intersection. Classes filtering by removing the most less frequent
symbols.

Present the dataset:

-   number of images

-   number of symbols

-   ...

#### Experimental Design {#ssec:supervised:large:experimental}

Experimental design for the use of Faster R-CNN, R-FCN, SSD on MUSCIMA++
dataset.

#### Results {#ssec:supervised:large:results}

Present results.

### Conclusion {#sec:supervised:conclusion}

Summarize insights found on supervised music symbol detection:

-   Fully-supervised detection of music symbol in printed/handwritten
    historical music scores is mostly a resolved problem

    -   in a constrained search space =\> small input image

        -   constraining the search space: can be done with the grammar

    -   given sufficient annotated data

        -   annotation can be done semi-automatically using a grammar

        -   bootstrapping can artificially augment dataset size

Main drawback is the use of manually annotated data. Introduce next
chapter for unsupervised dataset annotation.

Supervised detection of music notation produce very good results.

-   if have lot of annotated data

-   although bootstrapping help

Supervised Detection Methods

-   Add data and ground truth

-   Improve model architecture

Cost of Manual Annotation

-   Intensive Human Interaction

-   very expensive

-   Slow

-   goal: remove the need of manual annotation

\clearemptydoublepage{}
## Learning Symbol Detection Without Manual Annotations {#chap:learning}

Compare two approaches to reduce manual annotation for music symbol
detection.

-   Using music typesetting software to generate synthetic data
    (traditional)

-   Using U-Net-based GAN to transfer images of real music scores to a
    synthetic space (new)

### Train With Synthetic Data {#sec:learning:train}

Classical approach to DL-based detection without manual annotations.

-   Use domain specific complex synthetic data generation strategy

    -   For music scores: software generated scores

-   Train DL detectors using synthetic data

-   Apply trained detector on real non-annotated images

Detectors: Faster R-CNN, R-FCN, SSD pretrained on ImageNet

\centering
![Training Deep Learning Detector with Synthetic
Data[\[fig:unsup\_detect\_synth\_method\]]{#fig:unsup_detect_synth_method
label="fig:unsup_detect_synth_method"}](ressources/dl-detector-synthetic-data.pdf){width="\linewidth"}

**Process Real Data With Pretrained Model**

#### Synthetic Music Score Generation {#ssec:learning:train:synthetic}

-   Need isolated symbol shapes as base elements for generation

-   Need domain-specific knowledge (music notation) to structure the
    document

-   Need knowledge about document degradation and transformation (aging,
    scanning, ...)

##### Existing Software {#sssec:learning:train:synthetic:existing}

MuseScore, Lilypond

DocCreator

#### Problematic {#ssec:learning:train:problematic}

Synthetic detection is about model generalization to a new corpus.

We choose to use a new method based on transferring data to a simpler
representation.

Problem: Hard to adapt to a new corpus of document

-   Hard to adapt to historical music notation

-   Hard to synthesize historical document degradation

-   hard to adapt to a new corpus

-   No training on real data

Goal:

Automatically annotate music symbol found in these music scores with box
and class information.

Small validation set:

A small validation set should be manually annotated to drive
hyper-parameter tuning and early stopping of the training of the GAN.
Small validation set should contain $\sim$20 symbols per class. This set
can be artificially augmented using bootstrapping (random window
position). Conclusion: Can we train a music symbol detector using only
isolated music symbols?

### Transfer Historical Scores to Synthetic Representation {#sec:learning:transfer}

\todo[inline]{\Method{} is not synthetic generation.}
#### Introduction {#ssec:learning:transfer:introduction}

\todo[inline]{explain globally the why of our method with the logical chain of thought}
-   Can do simple symbol detection with isolated music symbols

-   Adapt real data to be used for simple detection task

#### Simple Graphical Representation for Symbol Detection {#ssec:learning:transfer:simple}

Remove the need for domain-specific knowledge and document degradation
knowledge

Need isolated symbol shapes as base elements for generation: simple to
get

Easy to build/train detector

Can't be directly applied to real data

\todo[inline]{show a figure to summarize the whole process}
#### Using GAN for Isolated Symbols Domain Transfer {#ssec:learning:transfer:using}

### Conclusion {#sec:learning:conclusion}

-   Automatic adaptation to new notation, new degradation, new corpus

-   Unified method for different document type

-   Use GAN to transfer images to new domain

-   Do detection on new transferred images

\clearemptydoublepage{}
## Unsupervised Symbol Detection through Isolated Symbols Domain Transfer {#chap:unsupervised}

### Objectives {#sec:unsupervised:objectives}

We present now our novel approach to unsupervised music symbol detection
of historical music scores by only using an isolated music symbol
dataset as presented
in [\[sec:learning:transfer\]](#sec:learning:transfer){reference-type="ref+page"
reference="sec:learning:transfer"}.

As we previously shown
in [\[sec:learning:train\]](#sec:learning:train){reference-type="ref+page"
reference="sec:learning:train"}, the task of detecting music symbols on
an entirely new corpus of music scores document images is classically
resolved by generating synthetic music scores. These scores should match
the real documents structures, music notation, document degradation as
closely as possible so that detection models trained using synthetic
data could be used on the real corpus of documents. However, these
synthetic generation procedures can be really complex, has no guaranties
of working and are difficult to generalize to a new dataset.

Our core proposition is the use of isolated music symbols to construct a
trivial detection task which will then be used as an intermediary step
for the detection of music symbols in real images of music scores. The
transfer of real images of historical music scores to this intermediate
representation is done using a Isolating GAN model which act as a
graphical filter, removing unwanted noise or symbols and enhancing
target symbols to detect. However, the GAN model is prone to a lot of
instability when trained. This is why we propose in this chapter
multiple solutions and improvements both in the way we use and process
our data and the way we use the Isolating GAN model.

#### Real Data to Annotate {#ssec:unsupervised:objectives:real}

\todo[inline]{Organize~\vref{ssec:unsupervised:objectives:real} with~\vref{sec:supervised:optical} and organize what goes with what.}
As explained before
in [\[sec:supervised:optical\]](#sec:supervised:optical){reference-type="ref+page"
reference="sec:supervised:optical"}, we concentrate on the detection of
music symbols in historical printed music scores of the mid-18th to
mid-20th century. These documents present unique segmentation problems
because of their music notation density typically present in orchestral
or piano scores. The imprinting techniques of the era and time
degradation also creates hard segmentation problems of touching and
broken music symbols as shown
in [\[fig:hard\_seg\]](#fig:hard_seg){reference-type="ref+page"
reference="fig:hard_seg"}. The lack of manually annotated datasets for
symbol detection existing prior to this work makes this kind of document
an ideal test bed to focus on unsupervised music symbol detection.

Another characteristics of these documents are the voluminous size of
the scanned images. The causes are mainly the combination of fairly
large original documents (A4 or larger) and the need for high resolution
images that can capture enough details for an accurate detection. A good
heuristic is that the minimum resolution required for a scanned image of
a music score is to have a minimum height of 20 pixels between two lines
of a staff. Combined with a document containing 10 or more staves, this
will produce scanned images of music scores with height and width in the
magnitude of multiple thousands of pixels.

Finally, the music notation allows the use of a very large set of music
symbols in order to better express the complexity of describing musical
sounds. For example, the Standard Music Font Layout
(SMuFL) [@smufl_standard_2013] specify the use of at least 2600
different glyphs. However, the use of the different music symbols are
not equally distributed in a music score, where symbols like note heads
are overwhelmingly frequent while others like the double sharp will be
very rarely used. To illustrate this unbalanced distribution, the
MUSCIMA++ dataset [@hajic_muscima++_2017] annotated 91255 music symbols
where 23352 are actually note heads, i.e. $\sim$25% of annotated symbols
are note heads.

All these characteristics make the detection of music symbols in
historical printed scores a very difficult task, especially when no
annotated dataset exists to directly train a supervised detection model
such as a Faster R-CNN. In contrast, we now present an iterative method
where each step is able to reduce the complexity of the detection task
step by step in order to accurately produce detection annotations
without any ground truth data.

#### Overview of Unsupervised Detection of Symbols {#ssec:unsupervised:objectives:overview}

Our novel approach to unsupervised music symbol detection can be broken
down into three specific steps as shown
in [\[fig:workflow-overview\]](#fig:workflow-overview){reference-type="ref+page"
reference="fig:workflow-overview"}.

###### Simplifying Music Notation for Music Symbol Detection {#par:unsupervised:objectives:overview:simplifying}

As shown
in [\[ssec:unsupervised:objectives:real\]](#ssec:unsupervised:objectives:real){reference-type="ref+page"
reference="ssec:unsupervised:objectives:real"}, images of music score
document can be very large when full pages are scanned at the
appropriate resolutions.

Intuitively, a detection task complexity is directly related to the size
of the area we search into. The larger the search area is, the more
difficult it will be to fit the correct bounding box to the
corresponding symbol. This problem is all the more relevant for
generative models where generating high resolution images has been a
notably difficult task. The size of the search area will also directly
impact the virtual memory usage of the computing device, which will
often be limited on GPU devices. Deep Learning models will typically
accommodate for larger input images by having deeper networks, which
also increases training complexity, training times and the model memory
usage.

Therefore, we propose
in [\[sec:unsupervised:simplifying\]](#sec:unsupervised:simplifying){reference-type="ref+page"
reference="sec:unsupervised:simplifying"} the reuse of the DMOS-PI
syntactical method and its music grammar that we previously presented
in [\[ssec:supervised:hybrid:grammatical\]](#ssec:supervised:hybrid:grammatical){reference-type="ref+page"
reference="ssec:supervised:hybrid:grammatical"}, to identify Region of
Interests (ROIs) and reduce the amount of search areas for music
symbols. Using this method, we can also progressively build our
detection ground truth for the corpus of music scores document by
iteratively focusing on different set of music symbols and reusing
previously detected symbols to improve the detection of a new set of
symbols. For example, if we have already detected note head symbols, it
is easy to know that regions at the left of note head symbols are likely
to present accidental symbols.

###### Isolating Music Symbols using Image-to-Image Translation {#par:unsupervised:objectives:overview:simplifying:isolating}

Once we identified region likely to contain symbols to detect, we
propose to use a Isolating GAN model as an image-to-image transfer
mechanism as explained
in [\[sec:unsupervised:u\]](#sec:unsupervised:u){reference-type="ref+page"
reference="sec:unsupervised:u"}. We train an U-Net generator of the GAN
model to isolate existing music symbols in small input images and
removing unwanted background noise or symbols. The training strategy is
designed by using an existing isolated music symbol dataset that will be
used to generate image patches with isolated symbols and a white
background. These generated images serves two purposes: one is to be the
target representation that the GAN is trying to imitate. The other is to
allow for a trivial detection task to be learned by a small and fast
detector model like the SSD model which we now present.

###### Isolated Music Symbol Detection {#par:unsupervised:objectives:overview:isolated}

After using the U-Net generator of the GAN model to transform and
isolate existing symbols in real image patches of music scores, the
final step of our method is to detect such isolated symbols using a
small and fast detector such as the SSD model. Our choice for this small
and fast detector stems from the fact that we the detection task is
trivial and done on small image patches. We present how we pre-train the
SSD model
in [\[sec:unsupervised:isolated\]](#sec:unsupervised:isolated){reference-type="ref+page"
reference="sec:unsupervised:isolated"} together with the generation
methodology using the isolated music symbol dataset.

The core of our proposition is the use of the Isolating GAN model as a
transfer function for symbols in real images of music scores into a
graphical representation only containing isolated music symbols and a
white background. In order to correctly evaluate this task in relation
to our end-goal task of detecting music symbols in real music scores
documents, we first present our detection method using the SSD model
in [\[sec:unsupervised:isolated\]](#sec:unsupervised:isolated){reference-type="ref+page"
reference="sec:unsupervised:isolated"} which we will later reuse to
evaluate the GAN model
in [\[sssec:unsupervised:isolated:evaluation:evaluation\]](#sssec:unsupervised:isolated:evaluation:evaluation){reference-type="ref+page"
reference="sssec:unsupervised:isolated:evaluation:evaluation"}.

\centering
![[\[fig:workflow-overview\]]{#fig:workflow-overview
label="fig:workflow-overview"}Overview of the unsupervised method for
music symbol detection presented with three cascading steps:
1. Simplifying the detection task, 2. Transferring small image patches
of real music scores into a simpler representation, 3. Detection of
isolated music symbol using the SSD
model.](ressources/workflow-overview.pdf){width="\linewidth"}

### Simplifying Music Notation for Music Symbol Detection {#sec:unsupervised:simplifying}

Due to the need to reduce the size of images that the GAN will take as
input, we propose to process the music scores document images with the
DMOS syntactical method [@couasnon_dmos_2001]. This method, previously
introduced
in [\[ssec:state:optical:the\]](#ssec:state:optical:the){reference-type="ref+page"
reference="ssec:state:optical:the"}, can be used to specify a music
grammar, presented
in [\[sssec:supervised:hybrid:grammatical:music\]](#sssec:supervised:hybrid:grammatical:music){reference-type="ref+page"
reference="sssec:supervised:hybrid:grammatical:music"}, able to parse
music scores images.

This grammar takes as input graphical terminals such as lines and
detected symbols and is able to reconstruct the music notation structure
of a score. One of the strength of this method is the ability of the
grammar to be used despite missing terminals with minimal alteration to
the grammar itself. When insufficient information are provided to be
able to construct the correct music notation structure, the grammar can
be made to ignore such error and continue the processing of the rest of
the document. Such failures can also be recorded, together with their
position and contextual information in the document. This process, as
shown in [\[fig:dmos\]](#fig:dmos){reference-type="ref+page"
reference="fig:dmos"}, can be used to reduce both the spatial search
space of the music symbol detection task and the class set of symbols to
detect.

\centering
![[\[fig:dmos\]]{#fig:dmos label="fig:dmos"}Grammatical processing of
whole pages of historical music scores to find Regions of Interests with
high probability of finding target
symbols.](ressources/dmos.pdf){width="0.4\linewidth"}

We use this mechanism to our advantage to bootstrap our detection task
for note head and accidental symbols. We first reuse the capability of
the grammar to build music notation structures such as systems, staves
and bars using only a priori information about staff lines and vertical
lines. Although the grammar will not be able to reconstruct a complete
note due to the missing note heads terminals, we can make a slight
modification to the grammar rule constructing music notes to record
every examples of potential valid stems together with their position and
current context. This will allow us to extract and isolate all the
possible regions containing note heads, for which we will then be able
to apply a detection method such as our GAN-based unsupervised detection
method. Furthermore, we can incrementally improve the recognition of the
grammar by adding progressively any new terminals recognized as input to
the grammar. Once note heads have been detected, the grammar rule
reconstructing music notes can now successfully reconstruct a music note
from a vertical bar, the stem, and one or multiple note heads. We can
then move on to a second detection task such as accidental detection by
recording every attempt by the grammar to find an accidental symbol at
the left of a note head. Moreover, we can artificially augment this
dataset by reusing our bootstrapping method previously explained
in [\[ssec:supervised:hybrid:bootstrapping\]](#ssec:supervised:hybrid:bootstrapping){reference-type="ref+page"
reference="ssec:supervised:hybrid:bootstrapping"}.

#### Conclusion {#ssec:unsupervised:simplifying:conclusion}

Using the DMOS grammar, we are therefore able to reduce the spatial
search space for the music symbol detection task. With this method, we
propose to experiment using a conservative approach on the size of the
input image presented to the GAN due to the well-known instable nature
of the GAN architecture. We start with small image patches that
guaranties us that the GAN training will manage to converge to
competitive results and leave the task of increasing the input image
size to future work.

The primary goal of this method is to design a method for resolving the
music symbol detection task without using any kind of manual annotations
for symbol detection and only using an isolated symbol dataset. While
this is true for the training of both the GAN model and detection model,
we found that it is necessary to constitute a very small validation
detection dataset containing manual annotations of both labels and boxes
of a symbol. The need for this small validation set is due to the
instable nature of the GAN, producing wildly different results when
using the same data and model architecture. This small validation set is
presented
in [\[sssec:unsupervised:isolated:evaluation:evaluation\]](#sssec:unsupervised:isolated:evaluation:evaluation){reference-type="ref+page"
reference="sssec:unsupervised:isolated:evaluation:evaluation"} where we
also explain our strategy to select the best trained model using an
early stopping mechanism.

Following this first step in our data processing pipeline, we now
present the use of an isolated music symbol dataset in order to design
an intermediate graphical representation allowing for trivial symbol
detection. Following this first step in our data processing pipeline, we
now present the use of an isolated music symbol dataset in order to
design an intermediate graphical representation. We will also present
the final step of our pipeline which is isolated symbol detection. We
first present this detection task because of its simplicity and the fact
that we will use the resulting trained detector in our GAN training
experiment to evaluate the generation quality of the GAN in regards to
the end goal of unsupervised symbol detection.

### Isolated Music Symbol Detection {#sec:unsupervised:isolated}

The task of supervised music symbol detection or more generally
supervised object detection requires data demonstrating the use of the
symbol in context, e.g. the use of music symbols in the context of a
music score and manually added annotations such as the bounding boxes
and class label of symbols. To remove the need of such slow and costly
manual annotations, such dataset can also be synthesized automatically
as explained
in [\[sec:learning:train\]](#sec:learning:train){reference-type="ref+page"
reference="sec:learning:train"} by using a lot of expert knowledge of
the specific application domain. The basic requirement of any synthetic
generation methods of document images is the use of isolated symbol
image dataset and it is the process of spatially arranging such symbols
in a blank page of a document that we name "Synthetic Generation". As we
explained before
in [\[sec:learning:transfer\]](#sec:learning:transfer){reference-type="ref+page"
reference="sec:learning:transfer"}, our approach aims to transform this
approach to unsupervised symbol detection by automatically translating
real images of documents into a simpler representation and therefore
reducing the need for expert knowledge. We now explain how we design
this simpler graphical representation by first presenting the isolated
music symbol dataset.

#### Isolated Music Symbol Dataset {#ssec:unsupervised:isolated:isolated}

An isolated symbol dataset is often used for classification tasks and is
constituted of images of symbols together with their corresponding class
label. This kind of datasets already gather multiple types of
information, such as the shape of a class of symbol and the
correspondence between a shape and a label. Multiple examples of the
same symbol class are included in order to characterize the variability
of a shape corresponding to a single class.

One of the premise of our work is that we believe that isolated music
symbol dataset are much more common and easily available than detection
dataset. For example, the OMR-Datasets website [@pacha_optical_nodate]
count at the time of this writing 11 different symbol classification
datasets but only three different symbol detection datasets. Although
this difference will be reduce as the field mature and more researcher
share their manually constituted datasets, the lack of annotated dataset
is a recurring problem for any new application domains trying to use
supervised detection model.

For this work, we needed an isolated symbol dataset that would strongly
resemble symbols from historical music scores of the mid-18th to the
mid-20th century. The isolated symbol dataset we use for accidental
detection, as shown in
fig. [\[fig:isolated-symbol-dataset\]](#fig:isolated-symbol-dataset){reference-type="ref"
reference="fig:isolated-symbol-dataset"}, is an isolated symbol dataset
manually constituted by the Intuidoc Team at the IRISA laboratory, where
symbols were extracted from real historical printed music scores and
classified automatically and verified manually. This dataset contains
541 symbol images with 3 different accidental classes and the
following [\[tab:isolated-symbol-dataset\]](#tab:isolated-symbol-dataset){reference-type="ref+page"
reference="tab:isolated-symbol-dataset"} show in details the detailed
distribution of symbols.

\centering
![[\[fig:isolated-symbol-dataset\]]{#fig:isolated-symbol-dataset
label="fig:isolated-symbol-dataset"}Isolated music symbol dataset,
classically used for music symbol classification
task.](ressources/isolated-symbol-dataset.pdf){width="0.6\linewidth"}

\centering
\todo[inline]{improve this table}
  Class       Amount
  --------- --------
  Natural        206
  Flat           144
  Sharp          191
  Total          541

  : [\[tab:isolated-symbol-dataset\]]{#tab:isolated-symbol-dataset
  label="tab:isolated-symbol-dataset"}Isolated Symbol Dataset class
  distribution.

As explain in our next section, this isolated symbol dataset is used to
design a target representation that the GAN model will be trained to
generate. Therefore, contrary to the task of classification, the
isolated symbol dataset priority is not about the number or diversity of
the samples, but samples of high quality and intra-class shape
variability that accurately model the intra-class shape variability of
the real dataset. That is why we did not consider using the Rebelo
dataset [@rebelo_optical_2009] because of the low resolution of symbols.
Using this isolated music symbol dataset, we are now able to design an
intermediate graphical representation to be use a target representation
for the GAN model.

#### Isolated Music Symbol Detection Dataset Generation {#ssec:unsupervised:isolated:isolated_music}

Using this isolated music symbol dataset, we can now construct a
detection task in the simplest manner possible. In a blank canvas, we
paste isolated music symbols at a random position after randomly
resizing the size of the symbols. We illustrate this generation
in [\[fig:unsup\_detect\_isolated\_symbol\_synth\_gen\]](#fig:unsup_detect_isolated_symbol_synth_gen){reference-type="ref+page"
reference="fig:unsup_detect_isolated_symbol_synth_gen"}. With this
method, we can avoid almost all the need for expert knowledge about
music notation which makes this step easier to do and easier to port to
a new application field. Although this step seems like a trivial image
generation step, it is the pivotal phase of the method that bridges the
transformation step done by the Isolating GAN presented
in [\[sec:unsupervised:u\]](#sec:unsupervised:u){reference-type="ref+page"
reference="sec:unsupervised:u"} and the detection step using a SSD that
we show next
in [\[ssec:unsupervised:isolated:isolated\_symbols\]](#ssec:unsupervised:isolated:isolated_symbols){reference-type="ref+page"
reference="ssec:unsupervised:isolated:isolated_symbols"}. By generating
this artificial detection dataset, we design a simpler graphical
representation that the Isolating GAN model can learn to generate while
allowing for an accurate detection of the real symbols in generated
images.

\centering
![[\[fig:unsup\_detect\_isolated\_symbol\_synth\_gen\]]{#fig:unsup_detect_isolated_symbol_synth_gen
label="fig:unsup_detect_isolated_symbol_synth_gen"}Synthetic detection
dataset generation using only isolated music symbols. Generated images
synthesizes a graphical representation that allows trivial detection
while keeping interesting characteristics from real images of music
scores like the position and shape of symbol to
detect.](ressources/synthetic-detection-dataset.pdf){width="0.4\linewidth"}

A number of hyperparameters needs to be defined in order to accurately
this generation method. A set of classes should be chosen as target
classes and defines which symbols we want to detect in the original
images. The remaining hyperparameters, such as the number of symbols,
their size and shape, can only be chosen using expert knowledge of the
specific application domain. In OMR, we can parameterize the size of
symbols using the vertical distance between two staff lines, i.e. the
interline, as a base distance unit. For example, we can specify that the
width of a sharp symbol should approximately be one times the interline
and the height 3 times the interline. However, this specific knowledge
for a sharp and other symbols can vary in printed scores depending on
the typeset used for imprinting.

In [\[ssec:unsupervised:isolated:learning\]](#ssec:unsupervised:isolated:learning){reference-type="ref+page"
reference="ssec:unsupervised:isolated:learning"}, we explain in more
details how this generation step relates to our use of the Isolating GAN
and how we are able to isolate music symbols in real images using this
simpler graphical representation. Next, we define how we perform the
final detection step using the small SSD model which we will then use to
evaluate the Isolating GAN training.

#### Isolated Symbols Detection using Single-Shot MultiBox Detector {#ssec:unsupervised:isolated:isolated_symbols}

Since the design of the intermediate representation make the detection
trivial because images present isolated music symbols in a white
background, we chose to use a Single-Shot MultiBox Detector model
introduced by [@liu_ssd_2016]. This small and lite detector model is
ideal for our use-case where we need a model with a very fast inference
time for intensive evaluation as used
in [\[sssec:unsupervised:isolated:evaluation:evaluation\]](#sssec:unsupervised:isolated:evaluation:evaluation){reference-type="ref+page"
reference="sssec:unsupervised:isolated:evaluation:evaluation"}. Since
our task is so simple, we further reduce the size of the model by only
using a 7 layer convolutional network instead of the original VGG-16
network.

\centering
![[\[fig:ressources/ssd-training\]]{#fig:ressources/ssd-training
label="fig:ressources/ssd-training"}Training of a Single-Shot Detection
(SSD) model with a synthetic detection dataset generated as shown
previously.](ressources/ssd-training.pdf){width="0.6\linewidth"}

\todo[inline]{prove this with experimental data}
As expected this task is easily solved even by such a small detector
model. However, we can not directly apply this trained detector on the
real image patches of historical music scores document as shown in
(whatever table or figure showing results), which is expected given that
our data does not model background noises and symbols.

This detection step happens as the third and final step of our method,
as shown
in [\[fig:workflow-overview\]](#fig:workflow-overview){reference-type="ref+page"
reference="fig:workflow-overview"}. We can now present the Isolating GAN
method as a transfer function from real data to synthetic data.

### Isolating Music Symbols using Image-to-Image Translation {#sec:unsupervised:u}

We have defined
in [\[sec:unsupervised:simplifying\]](#sec:unsupervised:simplifying){reference-type="ref+page"
reference="sec:unsupervised:simplifying"} the input images in which we
have to isolated music symbols and
in [\[ssec:unsupervised:isolated:isolated\_music\]](#ssec:unsupervised:isolated:isolated_music){reference-type="ref+page"
reference="ssec:unsupervised:isolated:isolated_music"}, we specify the
graphical representation on which we want to run the detection task. We
now need to design a generative model able to transform our input to our
output graphical representation which correspond to the second steps of
our method illustrated
in [\[fig:workflow-overview\]](#fig:workflow-overview){reference-type="ref+page"
reference="fig:workflow-overview"}. As previously explained
in [\[ssec:learning:transfer:using\]](#ssec:learning:transfer:using){reference-type="ref+page"
reference="ssec:learning:transfer:using"}, the literature has shown that
using Generative Adversarial Network has the required characteristics
for this difficult problem. We now present in details how we take
advantages of the peculiar GAN generative properties in regards to our
music symbol isolation tasks.

#### Learning the Intermediate Representation as Isolated Music Symbol {#ssec:unsupervised:isolated:learning}

The introduction of the GAN architecture starting
from [@goodfellow_generative_2014] made possible the generation of
completely artificial data like handwritten digits using an adversarial
methodology. One interesting characteristics of this method is that the
generation of artificial data is conditioned mainly by a another target
dataset, the MNIST [@lecun_mnist_nodate] in this case. A following
work [@chu_cyclegan_2017] has further shown that a modified GAN
architecture called Cycle-GAN can transform a set of images belonging to
a specific domain, for example: images of horses, into another
representation domain, like images of zebras, and vice-versa. The main
novelty of this work is to be able to do **unpaired** image-to-image
translation without any paired image information that explicitly link
the representation of an object in multiple domains of representation.
Specifically, the author of [@chu_cyclegan_2017] manage to train a model
able to transform a horse into a zebra and vice-versa sharing the same
pose and position in the image, without explicitly presenting a paired
instance of a horse and a zebra.

The main proposition of this work is to take advantage of this peculiar
capacity of GAN models to implicitly learn how to translate an instance
of an object from one domain into another. In the case of music symbol
detection, our aim is to transform an input image coming from a real
historical music scores, containing noise, degradation and complex
background symbols and signals into a simple clean output image free of
any background signals. However, this output image should keep the shape
and exact position and size of symbols to detect so that a detection
performed on the output image can be automatically transfered onto the
input image. In other words, the task of the GAN is threefold:

1.  Keep invariant the position and size of real symbols to detect

2.  Modify the shape of real symbols to detect to resemble isolated
    symbols of the corresponding class

    -   Also enhance and repair damaged symbols to resemble isolated
        symbols

3.  Remove background symbols and noise

In order for the GAN model to learn this task, we show
in [\[ssec:unsupervised:isolated:isolated\_symbols\]](#ssec:unsupervised:isolated:isolated_symbols){reference-type="ref+page"
reference="ssec:unsupervised:isolated:isolated_symbols"} how we design
an intermediate graphical representation using only isolated music
symbols that retain the three main characteristics of the GAN task:
music symbol shape information, music symbol position and size
information, an empty white background. Using isolated music symbols
corresponding to the target symbol class that we want to detect, our
objective is for the GAN to learn how to translate real symbols to
detect in real music scores into the corresponding isolated music symbol
shape. By pasting isolated music symbols into a blank canvas, we
introduce the notion of symbol position and size relative to the image
patch size.

As we mentioned before
in [\[ssec:unsupervised:isolated:isolated\_symbols\]](#ssec:unsupervised:isolated:isolated_symbols){reference-type="ref+page"
reference="ssec:unsupervised:isolated:isolated_symbols"}, symbols
present in real images of music scores present an intra-class
variability in their size and shapes. This variability can have a huge
impact on the performance of the GAN training because the sizes of
isolated symbols should be representative of the sizes of symbols in the
real images of music scores so that the GAN can learn to transfer
symbols to detect with an identical size. This is why we introduce
randomized variability into the generated sizes of isolated symbols by
using a minimal and maximal height and width parameters. Width and
height for each symbols are then sampled using a uniform distribution
between these two extrema. We explore the impact of these parameters in
experiments shown
in [\[sssec:experiments:large:ablation:different\]](#sssec:experiments:large:ablation:different){reference-type="ref+page"
reference="sssec:experiments:large:ablation:different"}.

In the case of the classical adversarial training, the GAN is not
constrained into respecting the original shape and position of the
symbol to detect. This lacks of constraint led us to introduce an
additional reconstruction loss using only isolated symbol data described
in [\[sssec:unsupervised:isolated:architecture:generator\]](#sssec:unsupervised:isolated:architecture:generator){reference-type="ref+page"
reference="sssec:unsupervised:isolated:architecture:generator"}. By
using a white empty background, we also avoid the difficult generation
process of trying to synthesize accurately historical music score
background images and we force the generator of the GAN to learn to act
as a filter for background symbols and noise of the input real image. We
further help the GAN training to model this rejection task by using
positive and negative examples of isolated music symbols. While positive
examples are symbols we want the GAN to isolate and keep into the
generated image, we add negative examples of isolated symbols belonging
to symbol classes we do not want to detect. The annotation of symbols
being positive or negative examples is actually done in the GAN loss
functions as described
in [\[sssec:unsupervised:isolated:architecture:discriminator\]](#sssec:unsupervised:isolated:architecture:discriminator){reference-type="ref+page"
reference="sssec:unsupervised:isolated:architecture:discriminator"}.

We now present the architecture of our Isolating GAN together with the
various enhancements to the training methodology in order to further
stabilize the process and improve the detection results.

#### Architecture {#ssec:unsupervised:isolated:architecture}

\todo[inline]{justify GAN instability using the literature}
The base architecture for our GAN model is the Deep Convolutional
Generative Adversarial Network (DCGAN) proposed
by [@radford_unsupervised_2016]. This architecture is an extension to
the original GAN architecture of [@goodfellow_generative_2014] using
convolutional and pooling layer instead of the original dense layers.
The use of convolutional layers greatly improve the quality of the
features learned during the adversarial training and directly impact the
generation and discrimination quality of the model. We replace the
generator part of the DCGAN model with the U-Net semantic segmentation
model proposed by [@ronneberger_u-net:_2015] to be able to use images as
input data to the generator model. The U-Net model architecture is
similar to an encoder-decoder architecture, where the encoder is a
traditional convolutional/pooling feature extractor. The decoder part is
a mirrored version of the encoder where the upscaling is done using
transposed 2D convolutions. Features extracted by the encoder are also
concatenated to the corresponding level of generated features in order
to improve the generation quality. We choose to use the VGG16 model
proposed by [@simonyan_very_2015] as a backbone network for the U-Net
generator which allows us to reuse pretrained VGG16 weights on the
ImageNet dataset, see [@deng_imagenet_2009], for the encoder part of the
U-Net model. A detailed description of the network generator is shown
in [\[tab:generator\]](#tab:generator){reference-type="ref+page"
reference="tab:generator"} and the network discriminator
in [\[tab:discriminator\]](#tab:discriminator){reference-type="ref+page"
reference="tab:discriminator"}.

\centering
\csvautobooktabular{ressources/generator.csv}
\centering
\csvautobooktabular{ressources/discriminator.csv}
##### Isolating GAN Training Architecture {#sssec:unsupervised:isolated:architecture:method}

In order to train the Isolating GAN architecture, we build on the
original training algorithm presented by [@goodfellow_generative_2014]
using both the discriminator loss and adversarial loss to train
respectively the discriminator and the generator. In all of our
experimentation, we only trained the Isolating GAN to isolate and detect
a single symbol class at a time in order to simplify to a maximum the
detection task and therefore improve the training results and stability.

To drive the detection of a specific symbol, we reuse the isolated
symbol dataset for detection presented
in [\[ssec:unsupervised:isolated:isolated\_music\]](#ssec:unsupervised:isolated:isolated_music){reference-type="ref+page"
reference="ssec:unsupervised:isolated:isolated_music"} where we generate
images containing isolated music symbols of a single class. In the
particular case of the discriminator loss illustrated
in [\[fig:gan-disc-training-negative\]](#fig:gan-disc-training-negative){reference-type="ref+page"
reference="fig:gan-disc-training-negative"}, we also need to specify for
each image generated a positive or a negative label. In this case, the
labels marks the origin of the image where a positive labels marks
automatically generated images using isolated symbols and negative
labels marks images generated by the Isolating GAN. However, this
labeling mechanism can also be used to improve the rejection capability
of the Isolating GAN as explained
in [\[sssec:unsupervised:isolated:architecture:discriminator\]](#sssec:unsupervised:isolated:architecture:discriminator){reference-type="ref+page"
reference="sssec:unsupervised:isolated:architecture:discriminator"}.

The adversarial loss shown
in [\[fig:gan-gen-training\]](#fig:gan-gen-training){reference-type="ref+page"
reference="fig:gan-gen-training"} is used to train the U-Net generator
of the GAN. As usual of the adversarial training algorithm, we only
specify positive labels for the adversarial loss in order to push the
generator to only keep symbols of the relevant class while removing
everything else.

\centering
![[\[fig:gan-disc-training-negative\]]{#fig:gan-disc-training-negative
label="fig:gan-disc-training-negative"}Isolating GAN discriminator
training using isolated symbols negative
examples](ressources/gan-disc-training-negative.pdf){width="0.8\linewidth"}

\centering
![[\[fig:gan-gen-training\]]{#fig:gan-gen-training
label="fig:gan-gen-training"}GAN generator
training.](ressources/gan-gen-training.pdf){width="0.6\linewidth"}

The adversarial training of the Isolating GAN is done in two distinct
steps in order to optimize in turns the generator and the discriminator.
That is why, we present the different improvements successively for the
discriminator, generator and how to balance the training of the two
models.

##### Discriminator Training {#sssec:unsupervised:isolated:architecture:discriminator}

The discriminator training is done by using real images of music scores
transformed by the U-Net generator and isolated symbols detection
images. Both group of images are processed by the discriminator for
binary classification which classifies the origin the images, either
coming from the U-Net Generator or coming from the isolated symbol
detection dataset. The gradients are computed by using a binary cross
entropy loss between the output of the discriminator and artificially
generated labels that marks the true origins of each images. The
gradients are then backpropagated and applied in the discriminator only.

While the composition of the real input images to the generator is
randomized over the whole dataset, we have complete control over of the
distribution of the isolated symbol detection dataset. We found in our
experiments that the constraints in term of possible minimum and maximum
sizes that a specific symbol class can take is crucial to the
performance and stability of the Isolating GAN training. Unfortunately,
these constraints parameters can not be deduced automatically from the
real dataset since no bounding boxes ground truth exist, but we can
derive approximate parameters using domain specific knowledge as
explained previously
in [\[ssec:unsupervised:isolated:isolated\_music\]](#ssec:unsupervised:isolated:isolated_music){reference-type="ref+page"
reference="ssec:unsupervised:isolated:isolated_music"}. These parameters
can then be fined tune by grid-search exploration using our minimal
validation set.

Another important parameters of the isolated symbol detection dataset
generation process is the choice of symbol classes to use. Originally,
we only used classes of symbols we wanted to our U-Net generator to
detect, i.e. positive examples. However, isolated symbol dataset usually
contains a large set of symbol classes that covers the entire
application domains. For example, our own isolated symbol dataset used
in this works contains 26 different classes of music symbols. Instead of
trying to use every classes of symbol possible, we used a more iterative
approach where we first observed the most common mistake the Isolating
GAN was doing. For example, preliminary results for our accidental
detection task showed a clear behavior of the U-Net generator to confuse
the different accidental classes. From this observation, we made an
experiment using other accidental classes except the accidental class to
detect as negative examples. We illustrate this
in [\[fig:gan-disc-training-negative\]](#fig:gan-disc-training-negative){reference-type="ref+page"
reference="fig:gan-disc-training-negative"} where isolated sharps are
marked as positive examples and isolated flat and natural symbols are
marked as negative examples. This mechanism can be used to explicitly
forbid the generator to produce certain shapes and help the generator to
avoid confusing symbols with different classes but similar shapes.

Formally, we name our source domain $X$ with image samples
$\{x^i\}^M_{i=1}$ the domain of real music scores image patches. For the
discriminator loss, images from the source domain are always labeled as
negative examples, i.e. $X_{label} = 0$. In the other hand, we name our
target domain $Y = Y_{pos} \cup Y_{neg}$ with image samples
$\{y^j_{pos}\}^N_{j=1} \in Y_{pos}$ containing isolated music symbols of
classes we want to detect and image samples
$\{y^k_{neg}\}^O_{k=1} \in Y_{neg}$ containing isolated music symbols of
classes we do not want to detect. We label our target domain images as
follow: $$Y_{label}(\{y^l_{label}\}^P_{l=1}) = \begin{cases}
    1, &\text{ if } y^l_{label} \in Y_{pos}\\
    0, &\text{ if } y^l_{label} \in Y_{neg}
  \end{cases}$$ Our U-Net generator $G$ goal is to map images from the
source domain to the target domain while our discriminator $D$ goal is
to discriminate images based on their origin: $Y_{pos}$ or
$Y_{neg} \cup G(X)$. Using the binary cross-entropy loss, we define the
usual discriminator loss: $$\begin{aligned}
 \label{eq:discriminator-loss}
  \mathcal{L}_D(G, D, X, Y, Y_{label}) = 
    & \mathbb{E}_{(y, y_{label}) \sim (Y, Y_{label})}[\overbrace{y_{label} * \log(D(y)) + (1 - y_{label}) * \log(1 - D(y))}^\text{binary cross entropy loss}] \nonumber \\
    & + \mathbb{E}_{x \sim X}[\underbrace{\log(1 - D(G(x)))}_{\mathclap{\text{simplified binary cross entropy since }x_{label}=0}}]\end{aligned}$$

##### Generator Training {#sssec:unsupervised:isolated:architecture:generator}

The training of the generator is done using the original adversarial
loss, by doing a forward pass using image patches of real music scores
through the U-Net generator and the discriminator. The gradients are
computed at the end of the discriminator using a binary cross entropy
loss between the output of the discriminator and synthetically generated
positive labels. The gradients are then backpropagated through the
discriminator and the generator but only applied to the generator. By
using only positive labels, we train the generator to fool the
discriminator by generating what the discriminator consider as positive
labels, i.e. isolated music symbol of the class we want to detect.

One of the problem with the original GAN training losses, in respect to
our detection task, is the lack of constraints to keep the original
symbol shape and position in the generated image. As described in the
original GAN work, the generator can easily collapse into systematically
generating the same symbol at the same position while perfectly fooling
the discriminator. However, it is not possible to add additional
informations about symbol sizes and positions using the generator input
image patches of real music scores since this dataset is not annotated.
In an other hand, we actually have total control and complete
information for our isolated music symbol dataset. We propose to reuse
this knowledge to our advantage by adding a supplementary loss called
**reconstruction loss** and apply it to the isolated music symbol
dataset. The main goal of this new training objective is to explicitly
train the generator to correctly process the specific case of isolated
music symbols pasted at random position and size in a white canvas. This
loss is applied only to the generator in the style of classic
auto-encoder network, as shown
in [\[fig:gan-recons-training\]](#fig:gan-recons-training){reference-type="ref+page"
reference="fig:gan-recons-training"}, where input data are images to
process and ground truth are respectively images that the generator
should produce.

In our case, we use symbols from our isolated symbol dataset and
generate both input images and output images using the same method as
described
in [\[ssec:unsupervised:isolated:isolated\_music\]](#ssec:unsupervised:isolated:isolated_music){reference-type="ref+page"
reference="ssec:unsupervised:isolated:isolated_music"} with a slight
variation. In the case where the input image contains an isolated symbol
of the class we want to detect, we use the exact same image as ground
truth. In the case where the input image contains an isolated symbol of
the class we do not want to detect, we replace the isolated symbol with
white pixels. This method allows to both provide an explicit training
objective for the ideal input case of the generator (isolated symbol
with empty background) for which the generator should learn the identity
transform and a few rejection cases using negative examples of symbols
that we do not want to detect for which the generator should learn to
suppress. This additional information during the training process allows
us to stabilize even further the training of the Isolating GAN while
also improving the detection performance.

\centering
![[\[fig:gan-recons-training\]]{#fig:gan-recons-training
label="fig:gan-recons-training"}GAN reconstruction loss using synthetic
data.](ressources/gan-recons-training.pdf){width="0.6\linewidth"}

Formally, we now recall the usual adversarial loss which uses our real
music score image patches $X$ as a source domain:
$$\label{eq:adversarial-loss}
  \mathcal{L}_{Adv}(G, D, X) = \mathbb{E}_{x \sim X}[\log(1 - D(G(x)))]$$
And our additional reconstruction loss which is a binary cross-entropy
loss applied between our target domain images $Y$ and target domain
ground truth images $Y^*$: $$\label{eq:recons-loss}
  \mathcal{L}_{Recons}(G, Y, Y^*) = \mathbb{E}_{(y, y^*) \sim (Y, Y^*)}[y^* * \log(G(y)) + (1 - y^*) * \log(1 - G(y))]$$
Where $Y^*$ is defined as we explained previously, either $y$ the input
image for positive examples and $y_{blank}$ an empty white image for
negative examples: $$Y^*(\{{y^*}^l\}^P_{l=1}) = \begin{cases}
    y, &\text{ if } y^l_{label} \in Y_{pos}\\
    y_{blank}, &\text{ if } y^l_{label} \in Y_{neg}
  \end{cases}$$

##### Tuning Balance Between Different Losses {#sssec:unsupervised:isolated:architecture:tuning}

We can now define our global training objective as a two-player minimax
game where we aim to solve: $$\begin{aligned}
 \label{eq:minimax}
  \operatorname*{\min}_G \operatorname*{\max}_D V(D, G) = 
  & \quad \mathbb{E}_{(y, y_{label}) \sim (Y, Y_{label})}[y_{label} * \log(D(y)) \nonumber \\
  & + (1 - y_{label}) * \log(1 - D(y))] \nonumber \\
  & + \mathbb{E}_{x \sim X}[\log(1 - D(G(x)))] \nonumber \\
  & + \mathbb{E}_{(y, y^*) \sim (Y, Y^*)}[y^* * \log(G(y)) + (1 - y^*) * \log(1 - G(y))]\end{aligned}$$

The original GAN paper [@goodfellow_generative_2014] mention that in
order to avoid a collapse of the generator, the discriminator should be
synchronized to the generator training, i.e. the generator should not be
trained two much. In the original paper, a single parameter *K* is used
to balance the adversarial and discriminator loss. This parameter
control the number of batch-wise training iterations the discriminator
should do before retraining the generator using the adversarial loss.

In this work, we apply this concept to our training algorithm where we
parameterize the number of consecutive batch-wise training step for our
three discriminator, adversarial and reconstruction losses, which we
name respectively *disc\_num\_train*, *adv\_num\_train* and
*recons\_num\_train*. We also parameterize the number of time a loss can
be trained before retraining another loss, such as
*gen\_max\_num\_train* which is the maximum number of training step
allowed for the generator using the adversarial or reconstruction loss
before retraining the discriminator. Finally, we limit the use of the
reconstruction loss by using an additional parameter
*recons\_train\_freq* which specify the frequency at which the
reconstruction loss is used. We present our whole training
algorithm [\[alg:unet-gan-training\]](#alg:unet-gan-training){reference-type="ref"
reference="alg:unet-gan-training"} which show how we use our data and
parameters for training the Isolating GAN model.

\DontPrintSemicolon{}
\SetKwData{advnumtrain}{AdvNumTrain}
\SetKwData{gennumtraincpt}{GenNumTrainCpt}
\SetKwData{genmaxnumtrain}{GenMaxNumTrain}
\SetKwData{discnumtrain}{DiscNumTrain}
\SetKwData{reconstrainfreq}{ReconsTrainFreq}
\SetKwData{reconstraincpt}{ReconsTrainCpt}
\SetKwData{reconsnumtrain}{ReconsNumTrain}
$\gennumtraincpt \leftarrow 0$ $\reconstraincpt \leftarrow 0$

A simpler implementation of this loss would have been to merge the
adversarial and reconstruction loss as a single loss using a weight
parameter. The reason for our implementation is the organic growth of
the model where we gradually added parameters and improvements in order
to improve our results. Simplifying this algorithm is one of the first
planned improvement for our future works.

#### Evaluation Protocol {#ssec:unsupervised:isolated:evaluation}

We now present our evaluation methodology of the Isolating GAN model in
relation to our music symbol detection task.

##### Evaluation architecture: U-Net Generator + SSD {#sssec:unsupervised:isolated:evaluation:evaluation}

Contrary to more classical model in Deep Learning literature, the
evaluation task of a GAN model is not a straightforward task because it
is difficult to evaluate directly the generation quality of the GAN
generator. Instead of using human manual validation, GAN models are
commonly evaluated using another model, like using an image classifier
to evaluate how much a generated image is realistic. Moreover, the
images produced by the Isolating GAN generator are not our end goal but
just an intermediary representation for the following detection task.

That is why we propose to evaluate our Isolating GAN model using a proxy
detector model. For that, we reuse our isolated symbol detector model we
shown
in [\[ssec:unsupervised:isolated:isolated\_symbols\]](#ssec:unsupervised:isolated:isolated_symbols){reference-type="ref+page"
reference="ssec:unsupervised:isolated:isolated_symbols"}. Our evaluation
architecture is made by connecting the U-Net generator of the GAN model
with the SSD model in order to create an end-to-end detection task going
from real image patches of music scores all the way to producing
detection predictions with boxes and labels. We illustrate this
architecture
in [\[fig:gan-eval\]](#fig:gan-eval){reference-type="ref+page"
reference="fig:gan-eval"}.

\centering
![[\[fig:gan-eval\]]{#fig:gan-eval label="fig:gan-eval"}GAN evaluation
Architecture.](ressources/gan-eval.pdf){width="0.7\linewidth"}

##### Training Results Analysis {#sssec:unsupervised:isolated:evaluation:training}

Next, we need to have annotated detection ground truth in order to
evaluate our detection task. However, this need contradict our approach
which is to avoid at much as possible to use manually annotated data
since it is costly and slow to produce. Ideally, we could train our
Isolating GAN model without using any ground truth information and
directly use it to produce reliable detection prediction. However, we
found that it is critical for the Isolating GAN model to be tuned
correctly using the right hyperparameters in order to stabilize the
training process and improve the detection accuracy. Instead of trying
to optimize the hyperparameters blindly, we propose to use a very small
validation dataset which we will manually annotate. This manual
annotations are produced by splitting images produced by our DMOS parser
into a training and validation set. The validation set is manually
annotated while the training set is directly used for training the
Isolating GAN model without any manual annotations. We also use
bootstrapping as presented
in [\[ssec:supervised:hybrid:bootstrapping\]](#ssec:supervised:hybrid:bootstrapping){reference-type="ref+page"
reference="ssec:supervised:hybrid:bootstrapping"} in order to
artificially augment our validation set and improve our estimator.
Finally, we use the mean Average Precision (mAP) detection metric as
presented by [@everingham_pascal_2010] with an Intersection over Union
(IoU) threshold of 0.75.

\todo[inline]{present in detail the validation set with number of symbol annotated and experimental data about choosing this number}
The goal of the validation set is to be able to give relatively accurate
measure of the detection performance of the Isolating GAN + SSD
architecture while being as small as possible. Although it won't give a
exact estimation of the detection performance of the method, we can at
least use this estimation to fine tune our hyperparameters and choose
the best performing Isolating GAN out of multiple trainings.

GAN models are well known for their training instability and the mode
collapsing problem has been identified since the first introduction of
the model and is explained by a saturated discriminator which is able to
reject the generator examples so strongly that no gradients are produced
for the generator training. In our work, we want to evaluate how well
our Isolating GAN is able to transform images from our source domain,
patch images of real music scores, into our target domain, isolated
music symbol of classes we want to detect, in relation to the detection
that will be performed by our SSD model. But this evaluation can be
difficult when we only have access to a small evaluation dataset and the
training is blocked in a mode collapsed state. To improve our evaluation
protocol, we decide to retrain the same Isolating GAN model with the
same hyperparameters set multiple times using a different random seed
each time. This common practice in machine learning gives us some
insight about the basic stability of the training method. Because of the
mode collapsing problem, retraining our model multiple time is essential
to have a smoother estimation of the performance of the model where we
can count the number of mode collapse. Next, we explain how we use our
validation set to do an early stopping of the training.

##### Early Stopping {#sssec:unsupervised:isolated:evaluation:early}

Due to the instable nature of the GAN training, multiple training of the
same model with the same hyperparameters but with different random seeds
can actually produce significantly different results. While the results
can vary quite a bit, the best model out of a set of trained model can
performed satisfactorily. However, there is a need to identify this
particular run and particular training epoch that gave the best result.
This is why we use an early stopping mechanism together with a multi run
approach using a small validation set that we presented previously. The
early stopping mechanism enables us to stop the Isolating GAN training
before overfitting happens. At every epoch, we compute the mAP metric on
our small validation set and if this metric does not improve after a
number of successive epoch, also called the patience parameter, we stop
the training. The last epoch that gave the best results will then be
used to produce detection prediction on the whole dataset.

### Conclusion {#sec:unsupervised:conclusion}

\todo[inline]{Finish conclusion}
We have presented our main contribution with a three steps method to do
unsupervised music symbol detection:

-   Reducing the spatial search detection problem complexity using a
    syntactical method

-   Image domain transfer using a Isolating GAN model and isolated music
    symbols for further simplifying the detection task

-   Symbol detection using a pretrained detection model using isolated
    music symbol

\clearemptydoublepage{}
## Experiments {#chap:experiments}

Experiments around GAN architecture, only on accidental dataset
end-to-end experiments on multiple datasets for multiple symbol classes

\todo[inline]{
* describe in details the experimentation protocol with dataset used, validation protocol used, etc
* for each section in chapter 4, identify which experiment to use and make a forward and backward reference.
}
\todo[inline, author=YR]{todo}
### Architectural Experiments {#sec:experiments:architectural}

We now present our experimental procedure to develop and validate our
new strategy for symbol detection. We used an iterative strategy and
started with a simple model, reduced dataset and a simple detection
task. We then gradually improve the model by modifying our loss function
or by modifying the data used during training.

The goal this section is to validate the capacity of the Isolating GAN
architecture to correctly transform input image patches of real image
scores to isolated music symbol to detect. We then investigate how to
balance the different learning rates used by the Isolating GAN
in [\[ssec:experiments:architectural:stabilize\]](#ssec:experiments:architectural:stabilize){reference-type="ref+page"
reference="ssec:experiments:architectural:stabilize"}. Next, we present
the iterative experiments that validate each of our proposed
improvements to the vanilla architecture such as adding a reconstruction
loss
in [\[sssec:experiments:architectural:stabilize:improved\]](#sssec:experiments:architectural:stabilize:improved){reference-type="ref+page"
reference="sssec:experiments:architectural:stabilize:improved"} and
using negative samples
in [\[sssec:experiments:architectural:stabilize:improved\_discriminator\]](#sssec:experiments:architectural:stabilize:improved_discriminator){reference-type="ref+page"
reference="sssec:experiments:architectural:stabilize:improved_discriminator"}.
Finally, we investigate
in [\[sssec:experiments:architectural:method:early\]](#sssec:experiments:architectural:method:early){reference-type="ref+page"
reference="sssec:experiments:architectural:method:early"} how we can
partially evaluate our method using a very small validation dataset
together with a pretrained SSD to be able to correctly tune the
Isolating GAN training without using a fully annotated dataset.

\centering
![[\[fig:ressources/expe\]]{#fig:ressources/expe
label="fig:ressources/expe"}Overview of architectural experiments of the
Isolating GAN method. Stars indicate what we consider our contributions.
Double arrows indicate a notion of balance and circled arrows
repetition.](ressources/expe.pdf){width="0.8\linewidth"}

#### Stabilize and Improve Isolating GAN Training {#ssec:experiments:architectural:stabilize}

Goal: In this section, we aim at stabilizing the training of the U-Net
and improving the generation quality of the generator.

Therefore, we tolerate the use of less standard metrics like pixel
accuracies and pixel iou as long as it shows a relative improvement from
a baseline.

\todo[inline]{add a conclusion for the section}
##### Balancing Discriminator and Adversarial Loss {#sssec:experiments:architectural:stabilize:balancing}

###### Objectives {#par:experiments:architectural:stabilize:balancing:objectives}

We now present the exploratory search of tuning the learning rates and
training balance between the generator and discriminator of the
Isolating GAN method driven by isolated music symbols. We propose to
first adjust the learning rates for the adversarial and discriminator
losses using an independent search on a range of learning rate values.
We then fine-tune this balance between the discriminator and generator
by modifying the AdvNumTrain and DiscNumTrain parameters which regulate
the number of consecutive time the generator and discriminator are
trained using the same batch of data. The goal of these experiments is
to find a set of parameters which produces a stable GAN training and
good generation performance.

###### Datasets {#par:experiments:architectural:stabilize:balancing:datasets}

We start our experiments using the vanilla Isolating GAN model and use
two different datasets to do the adversarial training. The first dataset
is the Isolated Printed Music Symbol Dataset presented
in [\[tab:isolated-symbol-dataset\]](#tab:isolated-symbol-dataset){reference-type="ref+page"
reference="tab:isolated-symbol-dataset"}. This dataset is used to
automatically produce larger images of size $128 \times 128$ pixels with
a white background. A single isolated music symbol is distorted using a
random size coefficient and then pasted on the white background.

The second dataset used is constituted of image patches of real music
scores as explained
in [\[sec:unsupervised:simplifying\]](#sec:unsupervised:simplifying){reference-type="ref+page"
reference="sec:unsupervised:simplifying"}. For these preliminary
experiments, we artificially control the classes and distribution of
classes used during training. For this section, we restrict this dataset
only to images that contains the accidental symbol which we want to
detect. For the experiments done in this section, we restrain the task
to single class detection task and take the accidental sharp class as
the class of symbol we want to detect. We also reuse the bootstrapping
techniques as mentioned
in [\[sec:unsupervised:simplifying\]](#sec:unsupervised:simplifying){reference-type="ref+page"
reference="sec:unsupervised:simplifying"} to artificially augment this
dataset.

###### Training Algorithm {#par:experiments:architectural:stabilize:balancing:training}

We train the GAN using the same algorithm as the original GAN
work [@goodfellow_generative_2014] where we alternatively train the
discriminator and generator. We start by using both datasets:
automatically produced images containing isolated symbols and real
images of music scores. The real images are then fed to the generator
which transform them and ideally remove background noise and symbols and
only keep the symbol to detect. Both sets of images are then fed to the
discriminator and the discriminator loss is applied to the discriminator
only by annotating the images containing isolated symbols as the
**True** set while images produced by the generator are annotated as the
**False** set. This operation is then repeated DiscNumTrain times. In
the second step, only images of real music scores are used. Images are
fed from the generator to the discriminator and the adversarial loss is
applied to the generator only by annotating this set of images as the
**True** set. This operation is then repeated AdvNumTrain times. A more
complete description of this algorithm is done
in [\[sssec:unsupervised:isolated:architecture:method\]](#sssec:unsupervised:isolated:architecture:method){reference-type="ref+page"
reference="sssec:unsupervised:isolated:architecture:method"}.

###### Evaluation {#par:experiments:architectural:stabilize:balancing:evaluation}

To directly evaluate the generation quality of the Isolating GAN
generator, we compute the pixel accuracy between the generated image and
an artificially generated image which represent the ideal output of the
generator. For example, if the input image patch extracted from a real
music score contains both a symbol to detect and background noise and
symbols, the ideal output of the generator would be an image where only
the symbol to detect remains and everything else was removed and
replaced by a white background. This artificial task allows us to
evaluate the background filtering capability of the U-Net as well as the
ability to keep and maintain the shape, size and position of the symbol
we want to detect. In this case, the original pixel accuracy metric is
heavily affected by the balance between black and white pixels in the
ground-truth data. That is why, in all our experiments, the pixel
accuracy results shown are measures where the white and black pixels
contribution were balanced.

Although both our dataset constitution and task definition uses manually
annotated ground-truth information, the training of the Isolating GAN is
entirely done without any ground-truth information. The ability to
measure directly the generation capability of the GAN greatly helped us
during the development of this method and we believe that these results
are showing very interesting properties of the GAN training behavior.

###### Isolating GAN-AdvLr/DiscLr Experimental Results {#par:experiments:architectural:stabilize:balancing:method}

These preliminary experiments focus on the tuning of the learning rates
parameters for the GAN adversarial training. From the original
adversarial training, we need to tune both the adversarial learning rate
and discriminator learning rate. The adversarial learning rate sets the
speed at which the generator network will learn while the discriminator
learning rate will set the speed of the discriminator training.
Moreover, we fine tune this training balance between the generator and
the discriminator by using two additional parameters: the adversarial
number of training step (AdvNumTrain) and the discriminator number of
training step (DiscNumTrain). These two parameters specify the number of
successive training each network receive for a single batch of data.
Although these parameters are not necessary and we could only try to
tune the learning rates, we found that it was easier to use these
additional parameters to fine tune the GAN performance.

First of all, the Isolating GAN-AdvLr experiment explore the adversarial
learning rate parameter with values between $[\num{1e-6}, \num{1e-2}]$.
Other parameters for this experiment are specified
in [\[tab:expe-params\]](#tab:expe-params){reference-type="ref+page"
reference="tab:expe-params"}. The exploration is done on a log scale and
as shown in [\[fig:advlr\]](#fig:advlr){reference-type="ref+page"
reference="fig:advlr"}, we found a typical bell curve showing that a
learning rate of gives the best results. Although we can not generalize
the use of this value for other datasets or task, we show that a shallow
grid-search is sufficient to stabilize the GAN training and produce
reasonably good results. The same can be said for the discriminator
learning rate with an optimal value of . However, the training of the
GAN still show a lot of instability as seen by the minimum pixel
accuracy standard deviation
in [\[fig:disclr\]](#fig:disclr){reference-type="ref+page"
reference="fig:disclr"}.

\centering
\scriptsize
              Expe                        AdvLr                        DiscLr             AdvNumTrain   DiscNumTrain
  ---------------------------- ---------------------------- ---------------------------- ------------- --------------
      Isolating GAN-AdvLr       $[\num{1e-6}, \num{1e-2}]$                                     1             1
      Isolating GAN-DiscLr                                   $[\num{2e-5}, \num{2e-3}]$        1             1
   Isolating GAN-AdvNumTrain                                                               $[1, 16]$         1
   Isolating GAN-DiscNumTrain                                                                  4         $[1, 16]$

  : [\[tab:expe-params\]]{#tab:expe-params
  label="tab:expe-params"}Parameter values for all Isolating GAN
  experiments. Values between square brackets are the minimal and
  maximal bounds explored for the parameter.

\subfloat[\label{fig:advlr}\Method-AdvLr]{
  \input{ressources/adv_lr.tex}
}
\hfill
\subfloat[\label{fig:disclr}\Method-DiscLr]{
  \input{ressources/disc_lr.tex}
}
\todo[inline]{say that the experiment was done 5 time.}
One interesting observation of these experiments is that the optimal
adversarial learning rate is much smaller than the optimal discriminator
learning rate. This behavior is coherent with the original GAN
work [@goodfellow_generative_2014] which observe that the discriminator
of the GAN should normally be trained to optimality at every batch step
before retraining the generator. In practice, we shorten the training of
the discriminator for the training to be computationally tractable in a
reasonable time. However, the use of a much larger discriminator
learning rate guaranties us that the discriminator learns and adapt much
faster than the generator.

###### Isolating GAN-AdvNumTrain/DiscNumTrain Experimental Results {#par:experiments:architectural:stabilize:balancing:method_advnumtrain/discnumtrain}

In the following experiments Isolating GAN-AdvNumTrain/DiscNumTrain, we
try to fine-tune even further the balance of training between the
generator and the discriminator by exploring respectively the
AdvNumTrain and DiscNumTrain parameters. As explained before, these
parameters regulate the number of successive training steps the
generator or the discriminator has. We explore values between $[1, 16]$
and do a complete grid-search for these two parameters as shown
in [\[tab:expe-loss\]](#tab:expe-loss){reference-type="ref+page"
reference="tab:expe-loss"}.

\centering
  ------------- -------------- ------------------- ---------------------------- -------------------
  AdvNumTrain   DiscNumTrain         Maximum                 Average                  Minimum
  1             1               $0.978 \pm 0.000$       $0.953 \pm 0.004$        $0.831 \pm 0.033$
  1             2               $0.972 \pm 0.002$       $0.920 \pm 0.011$        $0.647 \pm 0.107$
  1             4               $0.968 \pm 0.004$       $0.845 \pm 0.018$        $0.499 \pm 0.000$
  1             8               $0.785 \pm 0.068$       $0.545 \pm 0.020$        $0.411 \pm 0.036$
  1             16              $0.844 \pm 0.011$       $0.555 \pm 0.018$        $0.379 \pm 0.036$
  2             1               $0.978 \pm 0.001$       $0.958 \pm 0.004$        $0.762 \pm 0.115$
  2             2               $0.979 \pm 0.001$       $0.948 \pm 0.005$        $0.684 \pm 0.124$
  2             4               $0.973 \pm 0.003$       $0.875 \pm 0.011$        $0.499 \pm 0.000$
  2             8               $0.760 \pm 0.176$       $0.616 \pm 0.115$        $0.467 \pm 0.037$
  4             1               $0.979 \pm 0.001$   $\textbf{0.967} \pm 0.005$   $0.910 \pm 0.028$
  4             2               $0.976 \pm 0.001$       $0.959 \pm 0.004$        $0.774 \pm 0.165$
  4             4               $0.909 \pm 0.141$       $0.881 \pm 0.127$        $0.592 \pm 0.074$
  4             8               $0.881 \pm 0.190$       $0.768 \pm 0.148$        $0.500 \pm 0.000$
  4             16              $0.663 \pm 0.152$       $0.548 \pm 0.047$        $0.450 \pm 0.050$
  8             1               $0.907 \pm 0.148$       $0.900 \pm 0.145$        $0.868 \pm 0.133$
  8             2               $0.979 \pm 0.000$       $0.963 \pm 0.005$        $0.804 \pm 0.107$
  8             4               $0.979 \pm 0.001$       $0.963 \pm 0.004$        $0.810 \pm 0.123$
  8             8               $0.977 \pm 0.001$       $0.937 \pm 0.017$        $0.633 \pm 0.153$
  8             16              $0.912 \pm 0.133$       $0.842 \pm 0.104$        $0.540 \pm 0.058$
  16            1               $0.981 \pm 0.004$       $0.905 \pm 0.143$        $0.858 \pm 0.180$
  16            2               $0.922 \pm 0.119$       $0.913 \pm 0.115$        $0.876 \pm 0.101$
  16            4               $0.979 \pm 0.001$       $0.965 \pm 0.010$        $0.813 \pm 0.169$
  16            8               $0.981 \pm 0.001$       $0.958 \pm 0.010$        $0.674 \pm 0.192$
  16            16              $0.979 \pm 0.003$       $0.878 \pm 0.059$        $0.533 \pm 0.119$
  ------------- -------------- ------------------- ---------------------------- -------------------

  : [\[tab:expe-loss\]]{#tab:expe-loss label="tab:expe-loss"}Grid-search
  parameters values for AdvNumTrain and DiscNumTrain parameters
  measuring maximum, average and minimum pixel accuracy during Isolating
  GAN training. The GAN was trained 5 times using the same set of
  parameters and we show the average and standard deviation pixel
  accuracy over these 5 training. The AdvNumTrain and DiscNumTrain
  parameters are used to tune the number of consecutive time the
  generator and discriminator are trained during batch training step.

shows one of the optimal combination of using either 4 steps for
AdvNumTrain or 1 step for DiscNumTrain. However, these results do not
shows that a maximum was reached for the DiscNumTrain parameter. Indeed,
we can not explore values below 1 for the DiscNumTrain parameters.

\subfloat[\label{fig:DiscNumTrain}\Method-DiscNumTrain]{
 \input{ressources/disc_num_train.tex}
}
\hfill
\subfloat[\label{fig:AdvNumTrain}\Method-AdvNumTrain]{
  \input{ressources/adv_num_train.tex}
}
###### Conclusion {#par:experiments:architectural:stabilize:balancing:conclusion}

In this section, we have shown how we explored values for tuning the
adversarial and discriminator learning rates. The fine-tuning of these
parameters are essential to be able to train a GAN model with stability
and produce good performance on the generation task. Our best pixel
accuracy results at around 96% shows that the GAN learns to do the task
of keeping the sharp symbol to detect while removing background noise
and symbols. However, this task was learned by the GAN model without
using a direct formulation of this task in the form of a training
objectives, which in turns allows to learn this task without manually
annotated ground-truth. This fact is the main interest of our
unsupervised symbol detection method.

In the next section, we show how we can further improve the generation
quality of the Isolating GAN by using an additional reconstruction loss
together with an improved training algorithm.

##### Improved Generator Training Using Isolated Symbols Reconstruction Loss {#sssec:experiments:architectural:stabilize:improved}

\todo[inline]{
* add sssec to present the different experimentations
* inject knowledge using positive examples
* avoid confusion using negative examples
* in the generator or generation quality
* use schema of chapter 4 to illustrate what we are doing
}
###### Objectives {#par:experiments:architectural:stabilize:improved:objectives}

We now propose to investigate the use of an additional reconstruction
loss in order to further improve the generation quality of the Isolating
GAN. We model a reconstruction task for the generator using the isolated
music symbol dataset. As for the training of the discriminator, we
automatically generate blank images which contains a deformed isolated
symbol. However, in this case, the class set of isolated symbols is the
three accidental classes: sharp, flat and natural. Since the GAN model
is trained to detect a single symbol class, this reconstruction loss
will model two task at the same time. First, if the class of the
isolated symbol present in the image is the symbol class to detect, the
generator will have to produce the exact same image and will have to
learn an identity transformation. On the other hand, if the class of the
isolated symbol present in the image is not a symbol class to detect,
the generator will have to remove this symbol from the image and produce
a white empty image.

With the addition of the reconstruction, we also propose to improve the
training algorithm to better balance the training of the discriminator
and the generator. In this experiment, we explore different values for
the reconstruction learning rate to better adjust the contribution of
this new loss to the training of the generator.

In contrast to previous preliminary experiments presented
in [5.1.1.1](#sssec:experiments:architectural:stabilize:balancing){reference-type="ref"
reference="sssec:experiments:architectural:stabilize:balancing"}, we now
conduct our following set of experiments with a more realistic modeling
of the real image dataset. We also show the results using the mean
Average Precision detection metric which allows a better evaluation of
our detection task.

###### Datasets {#par:experiments:architectural:stabilize:improved:datasets}

In addition to the two datasets used in the previous
experiments [\[par:experiments:architectural:stabilize:balancing:datasets\]](#par:experiments:architectural:stabilize:balancing:datasets){reference-type="ref+page"
reference="par:experiments:architectural:stabilize:balancing:datasets"},
we build another dataset based on the isolated music symbol dataset.
Although the generation of the isolated symbol detection dataset does
not change from our previous experiment previously explained
in [\[par:experiments:architectural:stabilize:balancing:datasets\]](#par:experiments:architectural:stabilize:balancing:datasets){reference-type="ref+page"
reference="par:experiments:architectural:stabilize:balancing:datasets"},
we now better model the distribution of classes in the dataset
constituted of real images produced by our first preprocessing
step [\[sec:unsupervised:simplifying\]](#sec:unsupervised:simplifying){reference-type="ref+page"
reference="sec:unsupervised:simplifying"}. For all our following
experiments, we propose to use a realistic 1 to 9 ratio between images
containing an accidental symbol and rejection images that does not
contain any accidental symbols. In the 10% of images containing an
accidental symbol, the ratio between different accidental classes
follows the real distribution of accidental classes produced by our
syntactical method. We present more extensive evaluations of the impact
of the frequency of reject images
in [\[ssec:experiments:method:impact\]](#ssec:experiments:method:impact){reference-type="ref+page"
reference="ssec:experiments:method:impact"}

For our new reconstruction loss to train the generator, we automatically
generate images in the same manner as for the discriminator training
using isolated music symbols. However, we also add isolated accidental
symbols to reject and automatically produce ground-truth images. The
ground-truth images are either identical to the input image if there is
a simple to detect or are blank images for input images with symbols to
reject. In our case, we use the three accidental classes: sharp, natural
and flat, but we only train the GAN to detect one of the symbol class at
a time.

###### Training Algorithm {#par:experiments:architectural:stabilize:improved:training}

We modify the original GAN training algorithm to include this
reconstruction loss. The goal of this new training algorithm is to be
able to interleave the different training objective and correctly
balance the training of the generator and the discriminator. This
algorithm is described in detail
in [\[sssec:unsupervised:isolated:architecture:tuning\]](#sssec:unsupervised:isolated:architecture:tuning){reference-type="ref+page"
reference="sssec:unsupervised:isolated:architecture:tuning"}.

###### Evaluation {#par:experiments:architectural:stabilize:improved:evaluation}

To better evaluate the detection task of our method, we propose to use
the evaluation method presented
in [\[sssec:experiments:architectural:method:using\]](#sssec:experiments:architectural:method:using){reference-type="ref+page"
reference="sssec:experiments:architectural:method:using"}. Since the
end-goal of our method is to detect music symbol with a bounding box and
a class label, we propose to use a Single-Shot Detector model pretrained
on our automatically generated images containing Isolated Music Symbol
Dataset. Using this setup, we can measure a mean Average Precision
metric with an Intersection over Union over 0.75 (mAP \[IoU \> 0.75\]).
Using this metric, we can now compare our approach to a classic
fully-supervised detector model. The evaluation architecture is
constructed by forwarding produced images by the generator the SSD model
and then computing the mAP using detection ground-truth. As usual, we
use this manually annotated ground-truth only for evaluation in order to
develop our method. Moreover, we show
in [\[sssec:experiments:architectural:method:early\]](#sssec:experiments:architectural:method:early){reference-type="ref+page"
reference="sssec:experiments:architectural:method:early"} how we can
minimize the use of this ground-truth data while still being able to
correctly tune and improve the Isolating GAN method. Our evaluation
dataset consists of our image dataset extracted from real music scores
but without restraining images to contain a symbol of the class we want
to detect.

We measure the mAP metric at every epoch of the GAN training and report
the maximum mAP attained during a training. For every different
reconstruction learning rate values, we redo the training 12 times and
report the average and standard deviation of the maximum mAP with IoU \>
0.75. By reporting the maximum mAP value over a training, we
overestimate our detection performance of our method because in
practice, it will not be possible to identify the epoch which gave the
best mAP. Indeed, the use of a fully manually annotated detection
ground-truth will not be possible during actual use of our method. We
will show in the
next [\[sssec:experiments:architectural:method:early\]](#sssec:experiments:architectural:method:early){reference-type="ref+page"
reference="sssec:experiments:architectural:method:early"} how we can
mitigate this issue and evaluate the real performance we can obtain with
this method.

###### Isolating GAN-ReconsLr Experimental Results {#par:experiments:architectural:stabilize:improved:method}

We now show the results using our additional reconstruction loss for our
Isolating GAN method. We propose to explore the behavior of the GAN
training with different reconstruction loss learning rate as shown
in [\[fig:sg\_lr\]](#fig:sg_lr){reference-type="ref+page"
reference="fig:sg_lr"}. Moreover, we show the results for three
different class of accidental symbols: Flat, Natural and Sharp. We
compare these results with a baseline experiment shown
in [\[fig:sg\_lr\_witness\]](#fig:sg_lr_witness){reference-type="ref+page"
reference="fig:sg_lr_witness"} where the experimental settings are
identical to the Isolating GAN-ReconsLr experiment but without the use
of the reconstruction loss and without modification of the training
algorithm. Our baseline results shows that our changes for a more
realistic real dataset as input to the generator will effectively
prevent the GAN to achieve any meaningful detection results except for
one or two training outliers. This shows the necessity of improving the
performance and stability of the training of the GAN model.

In contrast, by using our additional reconstruction objective, we can
see that both the natural and sharp class obtain their best results
using reconstruction learning rate of with respectively $\sim$80% and
$\sim$88% of mAP with IoU \> 0.75. The flat class also shows a big
improvement by achieving 62% of mAP average using a reconstruction
learning rate of .

\centering
\centering
###### Conclusion {#par:experiments:architectural:stabilize:improved:conclusion}

In this experiment, we have shown the behavior of the Isolating GAN when
using an additional reconstruction loss together with a modified
training algorithm. We show that we start to obtain usable results for
the task of music symbol detection. Moreover, we show that using a
reconstruction task for the generator actually enables our method to
work. The idea of using negative examples with isolated symbol to be
removed by the generator can also be extended to the training of the
discriminator. We show in the next section how we implement this
improvement.

##### Improved Discriminator Training Using Negative Examples of Isolated Symbols {#sssec:experiments:architectural:stabilize:improved_discriminator}

###### Objectives {#par:experiments:architectural:stabilize:improved_discriminator:objectives}

The purpose of this experiment is to present the last improvement of our
Isolating GAN detection performance by using negative examples of
isolated music symbols for the discriminator training. In this case, we
focus on the discriminator loss training objective which is used to
train the discriminator network. In the original GAN training algorithm,
the discriminator is made to recognize the origin of given images, which
are either produced by the generator or from an existing dataset. In our
case, the discriminator has to differentiate between real image patches
of music scores transformed by the generator or images containing
isolated music symbol of the class we want to detect. With this training
objective, we give to the GAN the explicit information of which kind of
symbol **should be present** in images produced by the generator.
However, since we also have examples of isolated symbols which we do not
want to detect, we can also give this information to the GAN, which in
turn should improve the stability of the training and detection
performance of the method. This way, we also give the GAN the explicit
information of which kind of symbol **should not be present** in images
produced by the generator.

###### Datasets {#par:experiments:architectural:stabilize:improved_discriminator:datasets}

For this experiment, we build on the previous Isolating GAN-ReconsLr
experimentation datasets described
in [\[par:experiments:architectural:stabilize:improved:datasets\]](#par:experiments:architectural:stabilize:improved:datasets){reference-type="ref+page"
reference="par:experiments:architectural:stabilize:improved:datasets"}.

We modify the automatically produced images with isolated symbols used
by the discriminator by added two accidental classes. Therefore, images
can contain any of the three accidental classes: flat, natural and sharp
while the GAN is trained to detect only one of the accidental class at a
time. When annotating these images for the discriminator loss, we
annotate the images containing isolated symbols we want to detect as 1
while images containing other isolated symbols and images produced by
the generator are annotated as 0. The dataset produced from isolated
symbols is balanced by the isolated symbol classes, which means that
there will always be equal amount of symbols of each classes.

###### Training Algorithm {#par:experiments:architectural:stabilize:improved_discriminator:training}

We reuse the training algorithm describe in our previous experiment
Isolating GAN-ReconsLr
in [\[par:experiments:architectural:stabilize:improved:training\]](#par:experiments:architectural:stabilize:improved:training){reference-type="ref+page"
reference="par:experiments:architectural:stabilize:improved:training"}.

###### Evaluation {#par:experiments:architectural:stabilize:improved_discriminator:evaluation}

To evaluate our experimentation, we use the same mAP with an IoU \> 0.75
metric as presented
in [\[par:experiments:architectural:stabilize:improved:evaluation\]](#par:experiments:architectural:stabilize:improved:evaluation){reference-type="ref+page"
reference="par:experiments:architectural:stabilize:improved:evaluation"}.
As said earlier, reporting the maximum mAP obtained during the training
of the GAN is likely to overestimate our GAN performance because we will
not be able in practice to identify the epoch that gave this results due
to the lack of any ground-truth data. However, we explain how we
overcome this limitation by using a small validation set
in [5.1.2.2](#sssec:experiments:architectural:method:early){reference-type="ref"
reference="sssec:experiments:architectural:method:early"}.

\todo[inline]{change \Method-Neg to \Method-ReconsLr+Neg to specify that we build on previous experiment}
###### Isolating GAN-Neg Experimental Results {#par:experiments:architectural:stabilize:improved_discriminator:method}

In this experiment, we compare the results of using additional negative
examples of isolated symbol in the discriminator loss shown
in [\[fig:neg\_disc\]](#fig:neg_disc){reference-type="ref"
reference="fig:neg_disc"} with a witness experiment using the same
hyper-parameters except for the use of negative examples. First of all,
we can see that we obtain competitive results for the natural and sharp
class at around 90% and 95% of mAP. On the other hand, the flat class
produce more instable results with runs going from 50% of mAP to 87% of
mAP.

The use of negative examples shows a improved mAP of XX% for the sharp
class, XX% for the natural class and XX% for the flat class. This
clearly shows that the use of negative examples helps the GAN to better
filter and extract symbols to be detected by our pre-trained SSD.

\centering
###### Conclusion {#par:experiments:architectural:stabilize:improved_discriminator:conclusion}

We have shown that the use of the negative examples of isolated symbols
in the discriminator training helps the Isolating GAN to better filter
and extract music symbols we want to detect. By using a combination of
improvement such as an additional training loss and modified training
algorithm, careful tuning and balance of the training of the generator
and discriminator and use of additional information through an isolated
symbol dataset, we obtain competitive detection results using a totally
unsupervised training method for the task of music symbol detection.
However, we acknowledge that during these experiments, we did use a
manually annotated ground-truth dataset to **evaluate** and tune the
hyperparameters of our method. This means that it will be difficult to
adapt our method to a different dataset and therefore limit the
usefulness of our method. In our next section, we mitigate this issue by
using a small manually annotated validation dataset which can be used to
tune the hyperparameters of our method and select the best trained GAN
out of a pool of trained GANs.

#### Isolating GAN Evaluation {#ssec:experiments:architectural:method}

Since we have now presented the architecture and training methodology of
our Isolating GAN method, we focus in this section on our evaluation
methodology. Our end-goal task is the accurate detection of music symbol
in real historical printed music scores, we therefore propose
in [\[sssec:experiments:architectural:method:using\]](#sssec:experiments:architectural:method:using){reference-type="ref+page"
reference="sssec:experiments:architectural:method:using"} to use a
pretrained SSD model for evaluating our Isolating GAN method.
Afterwards, we will focus on the problem of trying to use our method in
a real use-case scenario where no manually annotated ground-truth is
available for the training data used.
In [\[sssec:experiments:architectural:method:early\]](#sssec:experiments:architectural:method:early){reference-type="ref+page"
reference="sssec:experiments:architectural:method:early"}, we will see
how we can use a very small evaluation dataset together with an early
stopping mechanism to select the best performing model and therefore
create the ability to correctly tune the hyperparameters of our model.

##### Using a SSD for Detection Evaluation {#sssec:experiments:architectural:method:using}

expe 36: evaluation using pre-trained detector

##### Early Stopping Using Small Validation Set {#sssec:experiments:architectural:method:early}

Using a SSD model to produce detection results from images generated by
the GAN allows us to evaluate our Isolating GAN method with the common
mean Average Precision metric for detection. However, we still have the
problem of having no ground-truth information to compare our prediction
with.

In the case of a real utilization of our method, the primary evaluation
goal is to have the ability to correct and fine-tune the hyperparameters
of our Isolating GAN, while the precise evaluation of the performance of
the method is secondary. Therefore, we propose to use a very small
validation dataset which should be sufficient to discriminate the best
trained GAN for a set of different hyperparameters. Because of the
instability of our method, we go even further and use this validation
dataset to discriminate the best trained GAN out of a pool of trained
GAN with identical hyperparameters. In the same manner, because of the
instability of the GAN model during the training itself, we use this
validation set to choose the best epoch to stop the training using an
early stopping mechanism.

We illustrate this mechanism
in [\[fig:training\_curves\]](#fig:training_curves){reference-type="ref+page"
reference="fig:training_curves"} by showing an example of using both a
fully annotated training dataset and a small validation dataset of 10
examples per class to evaluate the mAP metric during a GAN training.
First of all, we can see that the training is very instable which makes
it critical to stop the GAN training at an epoch that maximize the
performance of the GAN. In order to study our evaluation method, we
compute the optimal GAN performance by using a fully annotated training
dataset with detection ground-truth. However, in a real use-case
situation, no such ground-truth will be available. Therefore, we compare
this optimal performance with a mAP estimated using a small validation
dataset. This estimation will then be used to decide when the GAN
training should be stopped using a simple early stopping mechanism with
a patience parameter. We also report the actual performance of the GAN
computed on the fully annotated training dataset at the epoch which
maximized the mAP on the small validation dataset. This shows us the
loss in precision between using a fully annotated training dataset and a
small validation dataset.

\centering
\todo[inline]{explain that we voluntarily took an example where the results are bad in order to see the difference between the two curves.}
Unfortunately, we do not have a definitive answer on the size of this
small validation set. We believe this size will be dependent of the
training dataset used and the manual annotation capacity of the user.
However, we believe that a single user with a few hours of manual
annotations should be largely sufficient to produce this validation
dataset.

We demonstrate this by the following experiments where we evaluate our
Isolating GAN using different sizes for the validation set. We use as an
example a real situation which happened during the develop of our
method. One notable observation was that at this stage of the
development, the natural class for isolated symbols had a bad
configuration for the minimum and maximum size bounds for the random
resizing data augmentation technique. This led to bad detection results
for the natural class. On the other hand, the sharp and flat class
obtains reasonably good detection results.

We show the evolution of the real and estimation of the detection
performance using a fully manually annotated training dataset and a
small validation dataset of variable size. We start with a validation
size of 10 symbol examples per class, which gives us 40 ground-truth
examples equally distributed between three accidental classes and a
reject class. We show the results
in [\[fig:early\_stopping10\]](#fig:early_stopping10){reference-type="ref+page"
reference="fig:early_stopping10"} where we also report the real mAP
computed on the full training dataset at the epoch that maximize the mAP
on the small validation dataset. We show 10 trainings using the same
experimental parameters but with a different random seed for each
different target accidental class to detect. We can see in this case
that such a small validation dataset does not give a enough accurate
estimation of the performance of the GAN training. We can also see a
high discrepancy between the maximum mAP achieved by the fully annotated
training dataset and the mAP given by the epoch which maximized the mAP
on the small validation dataset. Luckily, it is easy to detect here that
the small validation dataset is too small by comparing the uniform
perfect results on the small validation dataset and a few produced
images from the generator.

\todo[inline]{
* complete caption for each size of validation set to note what to think about it
* add paragraph titles
}
\shorthandoff{:}
\shorthandoff{!}
\centering
\todo[inline]{improve figure titles and caption to explain what we want to say}
\shorthandon{:}
\shorthandon{!}
The same experiment was done with a slightly larger validation dataset
with 20 examples per class shown
in [\[fig:early\_stopping20\]](#fig:early_stopping20){reference-type="ref+page"
reference="fig:early_stopping20"}. Although we start to see some
variation on the results of the small validation dataset, the estimation
of the mAP does not follow the trend of the real mAP, which makes it
impossible to select the GAN which gave the best mAP.

\shorthandoff{:}
\shorthandoff{!}
\centering
\shorthandon{:}
\shorthandon{!}
Next, we propose to reuse our bootstrapping method as explained
in [\[ssec:supervised:hybrid:bootstrapping\]](#ssec:supervised:hybrid:bootstrapping){reference-type="ref+page"
reference="ssec:supervised:hybrid:bootstrapping"} to artificially
augment the small validation dataset. We show the results using a base
of 10 examples per
class [\[fig:early\_stopping\_bootstrap10\]](#fig:early_stopping_bootstrap10){reference-type="ref+page"
reference="fig:early_stopping_bootstrap10"} and 20 examples per
class [\[fig:early\_stopping\_bootstrap20\]](#fig:early_stopping_bootstrap20){reference-type="ref+page"
reference="fig:early_stopping_bootstrap20"}. As can be seen in the
results, using bootstrapping with 20 examples per class significantly
improve the reliability of the mAP estimation given by the small
validation dataset. It especially improve the early stopping mechanism
of the GAN training by identifying an epoch that maximize the real
precision of the GAN on the fully annotated training dataset.

\shorthandoff{:}
\shorthandoff{!}
\centering
\shorthandon{:}
\shorthandon{!}
\shorthandoff{:}
\shorthandoff{!}
\centering
\shorthandon{:}
\shorthandon{!}
In this section, we show how we can use a small validation dataset of 20
examples together with an augmentation technique such as bootstrapping
to accurately evaluate our Isolating GAN method without a fully
annotated dataset. Using this method, we are able to do an early
stopping of the GAN training that maximize the real mAP of the GAN. In
the next section, we show the robustness and fragility of our method in
respect to some key aspects of our method such as the real dataset class
distribution and the generation sizes parameter for the isolated symbol
dataset.

### Isolating GAN Robustness Evaluation {#sec:experiments:method}

We now measure the impact of some of the most important hyper-parameters
of our method. First, we measure the impact on the amount of images in
the real dataset that does not contain any symbol we want to detect.
Then, we also show the impact of the isolated symbols sizes generated
using the minimum and maximum size bounds hyper-parameters.

#### Impact of Rejection Task in Real Dataset Distribution {#ssec:experiments:method:impact}

\todo[inline]{
Experiments to redo:

* add 3\% ratio experiment
}
The stability of our GAN model depends not only on the architecture but
also on the data used for training the model. In this section, we study
the impact of the amount of rejection images in the real dataset used as
input to the generator on the stability of the GAN model.

###### Objectives {#par:experiments:method:impact:objectives}

What we call rejection images are empty images produced by our first
preprocessing step of simplifying the music notation of real music
scores [\[ssec:unsupervised:objectives:real\]](#ssec:unsupervised:objectives:real){reference-type="ref+page"
reference="ssec:unsupervised:objectives:real"} by identifying ROIs that
has a high probability of containing the type of symbol we want to
detect. This step will obviously always have false-positive examples
with images that do not contain any symbol to detect. Since there are no
way to know in advance of the ratio between the amount of rejection
images and images that do contains a symbol to detect, we investigates
different ratios where we artificially modified the amount images that
do contain symbols to detect and rejection images. We base this
experiments on the experimental settings of the Isolating GAN-Neg
experiment
in [\[sssec:experiments:architectural:stabilize:improved\_discriminator\]](#sssec:experiments:architectural:stabilize:improved_discriminator){reference-type="ref+page"
reference="sssec:experiments:architectural:stabilize:improved_discriminator"}
and only modify the composition of the real dataset used as input to the
generator.

###### Datasets {#par:experiments:method:impact:objectives:datasets}

We propose to artificially modify to ratio between images that contains
a symbol to detect and rejection images. We explore three different
ratio values of 10%, 50% and 90% of images with symbols to detect. We
also remove images with accidental symbols that we do not want to
detect, since we noticed that the GAN model can sometimes confuse
symbols with different accidental classes as the same class. This
simplification is done in order to isolate the sole effect of rejection
images on the training of the GAN model.

###### Evaluation {#par:experiments:method:impact:evaluation}

In order to evaluate our method with different rejection ratios, we use
our early stopping mechanism as presented
in [\[sssec:experiments:architectural:method:early\]](#sssec:experiments:architectural:method:early){reference-type="ref+page"
reference="sssec:experiments:architectural:method:early"} where we use
both a small validation dataset of 20 examples per symbol classes with
bootstrapping and the fully annotated training dataset for evaluation.
We report results using a mAP metric with IoU \> 0.75 computed on the
fully annotated training dataset but at epoch that maximized the results
on the small validation dataset. We repeat the training using the same
rejection ratio 10 times with a different random seed and present the
median, first and third quantiles mAP results. We also show the mAP
computed on the full training dataset of the model that had the best
results on the small validation dataset.

###### Isolating GAN-RejectRatio Experimental Results {#par:experiments:method:impact:evaluation:method}

shows the results of using different rejection ratio and we observe
different behavior for the three different accidental classes. The model
produces stable results for the sharp and natural classes for any ratios
rejection. However, the model becomes very instable when training with
only 10% of images containing a flat symbol and 90% of rejection images.
In order to counteract this instability, we use our early stopping
mechanism to select the best performing GAN model out of 10 trained
models and shows that we can still obtain very good results of 91% of
mAP by training with only 10% of images containing a flat symbol.

\centering
#### Sensibility to Generation Sizes of Isolated Symbols {#ssec:experiments:method:sensibility}

###### Objectives {#par:experiments:method:sensibility:objectives}

One of the standing stone of our method is the use of isolated symbols
to drive both the image translation done by the GAN model and symbol
detection done by the SSD detector. However, isolated symbols can not be
used as provided since they have to resemble to the symbols we want to
detect in real historical music scores. We also use these isolated
symbols in conjunction with simple data augmentation techniques which
are commonly used in deep learning experiments to introduce more
variation to the data and normally improve the performance of the
trained model. One basic modification to be made is the size that a
symbol will have since isolated symbols and real symbols won't probably
be of the same resolution. Since no ground-truth exists for the real
music symbols we want to detect, we can only guess the ranges of sizes
that each class of symbols have. Fortunately, music symbols follows
strict typesetting rules and we can often guess the possible range of
sizes that a symbol class will have. On the other hand, a lot of
variation in sizes can be present by using different music typefaces
which will have different sizes and width/height ratio for the same
symbol class. During the various experiments made while investigating
the use of a GAN model for symbol detection, we noticed that the GAN
model can be very sensible to the variation in sizes that isolated
symbols can take. Therefore, we propose to evaluate in this section the
robustness of our Isolating GAN in regards to isolated symbol sizes.

As usual, we base our experiment on the Isolating GAN-Neg shown
in [\[sssec:experiments:architectural:stabilize:improved\_discriminator\]](#sssec:experiments:architectural:stabilize:improved_discriminator){reference-type="ref+page"
reference="sssec:experiments:architectural:stabilize:improved_discriminator"}
and reuse the same model and datasets except for one modification for
the isolated symbol dataset.

###### Datasets {#par:experiments:method:sensibility:objectives:datasets}

We use the same dataset setup as our baseline experiment Isolating
GAN-Neg except for one modification to the minimum and maximum possible
width and height of isolated symbols. We show in
table [\[tab:iso\_sizes\]](#tab:iso_sizes){reference-type="ref+page"
reference="tab:iso_sizes"} the detailed minimum and maximum
width/heights values per class for the baseline and this new experiment.
For each classes, we reduce the minimum width/height and augment the
maximum width/height that accidental symbols can take.

\centering
\scriptsize
  --------------------- ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- -----
                                                                                          
                                                                                          
                        min   max   min   max   min   max   min   max   min   max   min   max
  Isolating GAN-Neg     79    112   25    37    98    128   16    36    72    112   27    45
  Isolating GAN-Sizes   64    128   16    56    64    128   8     48    64    128   16    64
  --------------------- ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- -----

  : [\[tab:iso\_sizes\]]{#tab:iso_sizes label="tab:iso_sizes"}Minimum
  and maximum width/height settings of isolated symbols for the
  Isolating GAN-Neg and Isolating GAN-Sizes experiments. The values are
  in pixels and symbols are pasted in canvas of 128$\times$128 pixels.

###### Evaluation {#par:experiments:method:sensibility:evaluation}

As usual, we evaluate our method with our early stopping mechanism
presented
in [\[sssec:experiments:architectural:method:early\]](#sssec:experiments:architectural:method:early){reference-type="ref+page"
reference="sssec:experiments:architectural:method:early"} by computing
the mAP metric with IoU \> 0.75 computed on the fully annotated training
dataset at the epoch that maximized the results on the small validation
dataset. We repeat the training 10 times with different random seeds and
present box plots of the 10 runs.

###### Isolating GAN-Sizes Experimental Results {#par:experiments:method:sensibility:evaluation:method}

\centering
\todo[inline]{Show baseline with smaller range of possible sizes for isolated symbols}
### Large Scale Experiments on IMSLP Dataset {#sec:experiments:large}

#### Large Scale Accidental detection {#ssec:experiments:large:large}

#### Large Scale Notehead detection {#ssec:experiments:large:large_scale_notehead}

#### Ablation Study {#ssec:experiments:large:ablation}

##### No reconstruction loss {#sssec:experiments:large:ablation:no}

##### Different bounds for isolated symbols {#sssec:experiments:large:ablation:different}

##### No negative examples in synthetic data {#sssec:experiments:large:ablation:no_negative}

### Conclusion {#sec:experiments:conclusion}

\clearemptydoublepage{}
## Isolating GAN Usage and Future Works {#chap:method}

### Improve Isolating GAN Prediction Quality {#sec:method:improve}

Architecture: U-Net Generator + SSD

Input data: whole real dataset

Merge predictions for multiple classes

Keep high confidence detection prediction: optimize precision instead of
recall

If needed: train a supervised detector (Faster R-CNN) using high
confidence detection prediction

\clearemptydoublepage{}
\backmatter{}
## Conclusion {#chap:conclusion .unnumbered .unnumbered}

\addcontentsline{toc}{chapter}{Conclusion}
\chaptermark{Conclusion}
## Bibliography {#chap:bibliography .unnumbered .unnumbered}

\addcontentsline{toc}{chapter}{Bibliography}
\printbibliography[heading=primary,keyword=primary]

\newpage
\printbibliography[heading=secondary,notkeyword=primary]
\newpage
\clearemptydoublepage{}
\markboth{}{}
\AddToShipoutPicture{\put(0,0){\includegraphics[width=\paperwidth,height=\paperheight]{./Couverture-these/MathSTIC/image-fond-MATHSTIC-dos.png}}}
\pagestyle{empty}
\vspace{-2cm}
\hspace{0.05cm}
\nobreak{}
\selectlanguage{french}
\hspace{- 1.75cm}
::: {style="color: mathSTIC-Color"}

------------------------------------------------------------------------
:::

### [Titre]{style="color: mathSTIC-Color"}: titre (en français)... {#titre-titre-en-français .unnumbered .unnumbered}

2 **Resumé:** Eius populus ab incunabulis primis ad usque pueritiae
tempus extremum, quod annis circumcluditur fere trecentis, circummurana
pertulit bella, deinde aetatem ingressus adultam post multiplices
bellorum aerumnas Alpes transcendit et fretum, in iuvenem erectus et
virum ex omni plaga quam orbis ambit inmensus, reportavit laureas et
triumphos, iamque vergens in senium et nomine solo aliquotiens vincens
ad tranquilliora vitae discessit. Hoc inmaturo interitu ipse quoque sui
pertaesus excessit e vita aetatis nono anno atque vicensimo cum
quadriennio imperasset. Natus apud Tuscos in Massa Veternensi, patre
Constantio Constantini fratre imperatoris, matreque Galla. Thalassius
vero ea tempestate praefectus praetorio praesens ipse quoque adrogantis
ingenii.

\selectlanguage{english}
\hspace{- 1.75cm}
::: {style="color: mathSTIC-Color"}

------------------------------------------------------------------------
:::

### [Title]{style="color: mathSTIC-Color"}: title (en anglais)... {#title-title-en-anglais .unnumbered .unnumbered}

2 **Abstract:** Eius populus ab incunabulis primis ad usque pueritiae
tempus extremum, quod annis circumcluditur fere trecentis, circummurana
pertulit bella, deinde aetatem ingressus adultam post multiplices
bellorum aerumnas Alpes transcendit et fretum, in iuvenem erectus et
virum ex omni plaga quam orbis ambit inmensus, reportavit laureas et
triumphos, iamque vergens in senium et nomine solo aliquotiens vincens
ad tranquilliora vitae discessit. Hoc inmaturo interitu ipse quoque sui
pertaesus excessit e vita aetatis nono anno atque vicensimo cum
quadriennio imperasset. Natus apud Tuscos in Massa Veternensi, patre
Constantio Constantini fratre imperatoris, matreque Galla. Thalassius
vero ea tempestate praefectus praetorio praesens ipse quoque adrogantis
ingenii.
