import argparse
import pandas as pd
import matplotlib.pyplot as plt
import utils


def make_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("results")
    parser.add_argument("--type", choices=["Small", "Large"], default='Small')
    parser.add_argument("--output")
    args = parser.parse_args()
    return args


def main(args):
    df = pd.read_csv(args.results)
    df = df[df.dataset == args.type]
    df = df.drop(columns='dataset')
    subplots = len(df.metric.unique())
    all_df = df.groupby(by=['metric']).mean().reset_index()
    all_df['label'] = 'All'
    df = pd.concat([df, all_df])
    df = df.melt(id_vars=['label', 'metric'], var_name='expe',
                 value_name='Value')
    df = df.pivot(index=['expe', 'label'], columns=['metric'], values='Value')

    if args.type == "Small":
        # extracted from multiclass_earlystopping.py
        chosen_run = df.loc['9']
    elif args.type == "Large":
        # extracted from fullscale_accidental_multiclass_notes_clippaste.py
        chosen_run = df.loc['1']
    print("chosen_run")
    print(chosen_run)

    axes = df.boxplot(by='label', layout=(1, subplots))
    for axe in axes:
        axe.set_xticks([1, 2, 3, 4])
        axe.set_xticklabels(['All', 'Flat', 'Natural', 'Sharp'])
        axe.set_xlabel('')
        if axe.get_title() != '':
            if axe == axes[0]:
                axe.plot([1, 2, 3, 4], chosen_run[axe.get_title()], marker='x',
                         markersize=10, markeredgecolor='red',
                         linestyle='None', label='chosen run')
            else:
                axe.plot([1, 2, 3, 4], chosen_run[axe.get_title()], marker='x',
                         markersize=10, markeredgecolor='red',
                         linestyle='None')
            axe.legend(loc='lower right')
    plt.ylim(0, 1)
    plt.suptitle(
        f'{args.type} Dataset Results'
    )

    def replace_callback(line):
        if line.startswith(r'\begin{groupplot}'):
            return line.replace(
                ']', r', width=\textwidth * 0.35, height=\textwidth * 0.5]')
        elif line == '  scale=0.6,':
            return '  scale=1.0,'
        elif line.startswith(r'\draw'):
            return line.replace('0.98', '1.12')
        else:
            return line

    def post_line_callback(line):
        if line.startswith('xticklabels={'):
            return r'xticklabel style={font=\scriptsize},'
        else:
            return None

    if args.output:
        utils.save_tex(
            args.output, 0.55, 0.5, replace_callback=replace_callback,
            post_line_callback=post_line_callback)
    else:
        plt.show()
    return


if __name__ == "__main__":
    main(make_args())
