import argparse
import pandas as pd
import matplotlib.pyplot as plt
import utils


def make_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("results")
    parser.add_argument("--output")
    args = parser.parse_args()
    return args


def main(args):
    df = pd.read_csv(args.results)
    df.plot.bar(stacked=True, x='Dataset', rot=0)
    plt.title(r"Ablation Study True Positive/False Positive Results\\on Small/Large Accidental Detection Dataset")
    plt.ylabel("Detections")
    utils.save_tex(args.output, width=0.80, height=0.3)
    return


if __name__ == "__main__":
    main(make_args())
