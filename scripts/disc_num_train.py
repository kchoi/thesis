from num_train import num_train


def disc_num_train(args):
    num_train(args, 'disc_num_train', 'adv_num_train', 4,
              '\\texttt{DiscNumTrain}', 'Exploration of \\texttt{DiscNumTrain}')
