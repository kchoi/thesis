import itertools
import pandas as pd
from plot_pixel_acc import plot_pixel_acc


def num_train(args, xkey, other, other_value, xlabel, title,
              ylabel='Pixel Accuracy'):
    df = pd.read_csv(args.results, low_memory=False)
    expes = [f"expe-{i}" for i in itertools.chain(
        range(13, 18))]
    df = df.query(f"run in {expes}")
    df = df[df[other] == other_value]
    mean_rename_dict = {
        'mean max_weighted_pixel_acc': 'maximum',
        'mean mean_weighted_pixel_acc': 'average',
        'mean min_weighted_pixel_acc': 'minimum',
    }
    std_rename_dict = {
        'std max_weighted_pixel_acc': 'maximum',
        'std mean_weighted_pixel_acc': 'average',
        'std min_weighted_pixel_acc': 'minimum',
    }
    df = df[[
        xkey,
        *list(mean_rename_dict),
        *list(std_rename_dict),
    ]]
    mean = df[[
        xkey,
        *list(mean_rename_dict),
    ]]
    mean = mean.rename(columns=mean_rename_dict)
    std = df[[
        xkey,
        *list(std_rename_dict),
    ]]
    std = std.rename(columns=std_rename_dict)
    columns = list(mean_rename_dict.values())
    error_low = mean[columns] - std[columns]
    error_high = mean[columns] + std[columns]

    plot_pixel_acc(
        args, mean, error_low, error_high, columns, xkey,
        xlabel, title, ylabel=ylabel, xscale='linear')
