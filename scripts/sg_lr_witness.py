import matplotlib.pyplot as plt
import utils


def sg_lr_witness(args):
    datasets = ['AccidentalDataset', 'ChoiDataset_EarlyStopping20']
    value_column = 'PascalBoxes_Precision/mAP@0.75IOU'
    df = utils.preprocess(
        args.results, [[590, 600]], datasets,
        ['name', 'dataset', 'expe', 'train', 'epoch'], value_column)

    df = utils.pivot_on_dataset(df, value_column,
                                ['name', 'expe', 'train', 'epoch'])

    # choose best epoch based on results on whole training dataset
    _, df = utils.early_stopping(
        df, ['name', 'expe', 'train'], 'AccidentalDataset')
    print(len(df))

    df = utils.pivot_on_name(df, 'AccidentalDataset',
                             ['expe', 'train'])

    axes = df.boxplot()
    axes.get_figure().suptitle('')
    plt.grid(True, which="both")
    plt.ylim(0, 1)
    plt.ylabel('mAP [IoU > 0.75]')
    plt.xlabel('Labels')
    plt.title('Baseline Experiment without Reconstruction Loss')

    # plt.savefig("ressources/sg_lr_witness.pdf")

    utils.save_tex(args.output, 0.5)
