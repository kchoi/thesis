import argparse
import pandas as pd
import matplotlib.pyplot as plt
import utils


def make_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("final_results")
    parser.add_argument("ablation_results")
    parser.add_argument("--type", choices=["Small", "Large"], default='Small')
    parser.add_argument("--output")
    args = parser.parse_args()
    return args


def main(args):
    ablation = pd.read_csv(args.ablation_results)
    ablation['name'] = 'Ablation'
    final = pd.read_csv(args.final_results)
    final['name'] = 'Isolating-GAN'
    df = pd.concat([final, ablation])
    df = df[df.Dataset == args.type]
    df = df.drop(columns='Dataset')
    df = df.sort_values(by='name')

    df.plot.bar(stacked=True, x='name', rot=0)
    plt.title(r"TP/FP Comparison\\"
              r"Ablation vs \Method{}\\"
              f"{args.type} Detection Dataset")
    # plt.ylabel("Detections")
    if args.type == "Small":
        plt.legend(loc='lower right')
    elif args.type == "Large":
        plt.legend(loc='center right')

    def post_line_callback(line):
        if line.startswith("xticklabels"):
            return r"xticklabel style={font=\scriptsize},"
        else:
            return None

    utils.save_tex(args.output, width=0.25, height=0.4,
                   post_line_callback=post_line_callback)
    return


if __name__ == "__main__":
    main(make_args())
