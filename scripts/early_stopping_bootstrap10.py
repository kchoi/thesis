from early_stopping import early_stopping_boxplot


def early_stopping_bootstrap10(args):
    early_stopping_boxplot(
        args, [[630, 640]], 'ChoiDataset_EarlyStopping10_Boot',
        r'Evaluation using Full Dataset and\\'
        'Small Validation Dataset of 10 Examples With Bootstrapping'
    )
