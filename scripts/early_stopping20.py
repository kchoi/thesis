from early_stopping import early_stopping_boxplot


def early_stopping20(args):
    early_stopping_boxplot(
        args, [[640, 650]], 'ChoiDataset_EarlyStopping20_NoBoot',
        r'Evaluation using Full Dataset and\\'
        'Small Validation Dataset of 20 Examples'
    )
