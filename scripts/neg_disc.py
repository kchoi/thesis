import pandas as pd
import matplotlib.pyplot as plt
import utils


def neg_disc(args):
    dataset = 'AccidentalDataset'
    value_column = 'PascalBoxes_Precision/mAP@0.75IOU'
    df = utils.preprocess(args.results, [[620, 630], [560, 570]], [dataset],
                          ['sg_lr', 'name', 'expe', 'run', 'train', 'epoch'],
                          value_column)
    df = df.query(
        "(name == 'seg_gan_singleclass' & sg_lr == 5e-05) | "
        "(name == 'seg_gan_flat' & sg_lr == 7e-05) | "
        "(name == 'seg_gan_natural' & sg_lr == 5e-05)")
    df = df.drop(columns='sg_lr')
    df = df.copy()
    df1 = utils.preprocess(args.results, [[590, 600]], [dataset],
                           ['name', 'expe', 'run', 'train', 'epoch'],
                           value_column)
    df1 = df1.copy()
    df = pd.concat([df, df1])
    df = df.reset_index(drop=True)

    def get_expe_name(expe):
        if '56' in expe:
            return 'ReconsLr'
        elif '62' in expe:
            return 'ReconsLr+Neg'
        elif '59' in expe:
            return 'Baseline'
    df['Experiment'] = [get_expe_name(expe) for expe in df.expe]
    _, df = utils.early_stopping(
        df, ['Experiment', 'name', 'expe', 'run', 'train'], value_column)
    print(len(df))
    df = utils.pivot_on_name(
        df, value_column, ['Experiment', 'expe', 'train'])
    df = df.drop(columns=['expe', 'train'])

    df1 = df.groupby(by=['Experiment'])
    df1_max = df1.max()
    df1_min = df1.min()
    df1_median = df1.median()
    df1_1st_quantile = df1.quantile(q=0.25)
    df1_3rd_quantile = df1.quantile(0.75)
    ax = plt.gca()
    df1_max.plot(ax=ax)
    ax.set_prop_cycle(None)
    df1_median.plot(linestyle='--', ax=ax, legend=False)
    ax.set_prop_cycle(None)
    df1_min.plot(linestyle='dotted', linewidth=2, ax=ax, legend=False)
    for i, line in enumerate(ax.lines):
        if i >= 3:
            line.set_label('')
    for key in df1_median.columns:
        plt.fill_between(
            df1_median.index, df1_1st_quantile[key], df1_3rd_quantile[key],
            alpha=0.3)
    ax.legend(loc='best')
    plt.grid(True, which="both")
    plt.ylim(0, 1)
    plt.ylabel('Maximum mAP [IoU > 0.75]')
    plt.xlabel('Experiments')
    plt.title('Comparison of Baseline, ReconsLr and ReconsLr+Neg Experiments')

    def post_line_callback(line):
        if line.startswith('legend style='):
            return 'legend columns=2,'
        elif line == ']':
            return ['\\addlegendimage{empty legend}',
                    '\\addlegendentry{Labels}',
                    '\\addlegendimage{empty legend}',
                    '\\addlegendentry{Stats}']
        elif line == '\\addlegendentry{Flat}':
            return ['\\addlegendimage{no markers, color0}',
                    '\\addlegendentry{Max}']
        elif line == '\\addlegendentry{Natural}':
            return ['\\addlegendimage{no markers, color0, dashed}',
                    '\\addlegendentry{Median}']
        elif line == '\\addlegendentry{Sharp}':
            return ['\\addlegendimage{no markers, color0, area legend, fill=color0}',
                    '\\addlegendentry{\\nth{1}/\\nth{3} Quartiles}',
                    '\\addlegendimage{empty legend}',
                    '\\addlegendentry{}',
                    '\\addlegendimage{no markers, color0, dotted, thick}',
                    '\\addlegendentry{Min}',
                    ]
        else:
            return None
    utils.save_tex(args.output, 0.7, 0.5,
                   post_line_callback=post_line_callback)
