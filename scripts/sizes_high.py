import matplotlib.pyplot as plt
import utils


def sizes_high(args):
    datasets = ['AccidentalDataset', 'ChoiDataset_EarlyStopping20']
    value_column = 'PascalBoxes_Precision/mAP@0.75IOU'
    df = utils.preprocess(args.results, [[610, 620], [620, 630]], datasets,
                          ['name', 'dataset', 'expe', 'epoch'], value_column)
    df = df.copy()

    def make_sizes(expe):
        if '62' in expe:
            return 'low'
        elif '61' in expe:
            return 'high'
        else:
            raise RuntimeError()
    df['sizes'] = df['expe'].apply(make_sizes)

    df = utils.pivot_on_dataset(
        df, value_column, ['sizes', 'name', 'expe', 'epoch'])

    _, df = utils.early_stopping(df, ['sizes', 'name', 'expe'], datasets[1])

    df = utils.pivot_on_name(df, datasets[0], ['sizes', 'expe'])
    df = df.drop(columns="expe")

    axes = df.boxplot(by=['sizes'], layout=(1, 3))

    axes[0].set_ylabel('Maximum mAP [IoU > 0.75]')
    axes[0].set_ylim(0, 1)
    for axe in axes:
        axe.set_xticks([1, 2])
        axe.set_xlabel('')
        if axe.get_title() != '':
            axe.legend(loc='lower right')

    plt.ylim(0, 1)
    plt.suptitle(r"Impact of Isolated Symbol Sizes using \Method{}"
                 r"\\Using Small Validation Dataset for Early Stopping")

    def replace_callback(line):
        if line.startswith(r'\begin{groupplot}'):
            return line.replace(
                ']', r', width=\textwidth * 0.35, height=\textwidth * 0.5]')
        elif line == '  scale=0.6,':
            return '  scale=1.0,'
        elif line == '  rotate=0.0':
            return '  rotate=0.0,'
        elif line.startswith(r'\draw'):
            return line.replace('0.98', '1.12')
        else:
            return line

    def post_line_callback(line):
        if line == 'title={Natural},':
            return r'xlabel={Isolated Accidentals Sizes (see \cref{tab:iso_sizes})},'
        elif line.startswith('  rotate=0.0'):
            return '  align=center'
        else:
            return None
    utils.save_tex(
        args.output, 0.55, 0.5, replace_callback=replace_callback,
        post_line_callback=post_line_callback)
