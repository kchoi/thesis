import argparse
import pandas as pd
import matplotlib.pyplot as plt
import utils


def make_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("results")
    parser.add_argument("--output")
    args = parser.parse_args()
    return args


def main(args):
    df = pd.read_csv(args.results)
    df = df.groupby(by=['dataset', 'metric']).mean()
    df.plot.bar()
    plt.legend(loc='upper left', bbox_to_anchor=(0.16, 0.97))
    plt.title("Ablation Study Results on Small/Large Accidental Detection Dataset")
    utils.save_tex(args.output, width=0.90, height=0.3)
    return


if __name__ == "__main__":
    main(make_args())
