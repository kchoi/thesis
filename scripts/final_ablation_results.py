import argparse
import pandas as pd
import matplotlib.pyplot as plt
import utils


def make_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("final_results")
    parser.add_argument("ablation_results")
    parser.add_argument("--type", choices=["Small", "Large"], default='Small')
    parser.add_argument("--output")
    args = parser.parse_args()
    return args


def main(args):
    df = pd.read_csv(args.final_results)
    df = df[df.dataset == args.type]
    df = df.drop(columns='dataset')
    all_df = df.groupby(by=['metric']).mean().reset_index()
    all_df['label'] = 'All'
    df = pd.concat([df, all_df])
    df = df.melt(id_vars=['label', 'metric'], var_name='expe',
                 value_name='Value')
    df = df.pivot(index=['expe', 'metric'], columns=['label'], values='Value')
    if args.type == "Small":
        # extracted from multiclass_earlystopping.py
        chosen_run = df.loc['9']
    elif args.type == "Large":
        # extracted from fullscale_accidental_multiclass_notes_clippaste.py
        chosen_run = df.loc['1']
    chosen_run['name'] = 'Iso-GAN'

    df1 = pd.read_csv(args.ablation_results)
    df1 = df1[df1.dataset == args.type]
    df1 = df1.drop(columns='dataset')
    df1 = df1.set_index('metric')
    df1['name'] = 'Ablat°'

    df2 = pd.concat([chosen_run, df1])
    df2 = df2.reset_index()
    df2 = df2.set_index(['metric', 'name'])
    df2 = df2.sort_values(by=['metric', 'name'])

    print(df2)
    df2.plot.bar(rot=0)
    plt.legend(loc='lower right')
    plt.ylim(0, 1)
    plt.suptitle(
        f'{args.type} Dataset Results vs Ablation Results'
    )

    def replace_callback(line):
        if line.startswith("xticklabels"):
            line = line.replace('(', '')
            line = line.replace(')', '')
            line = line.replace(', ', r'\\')
            return line
        elif line.startswith(r'\draw'):
            return line.replace('0.98', '1.10')
        elif line == '  scale=0.6,':
            return '  scale=1.0,'
        else:
            return line

    def post_line_callback(line):
        if line.startswith("xticklabels"):
            return r"xticklabel style={font=\scriptsize,align=center},"
        else:
            return None

    if args.output:
        utils.save_tex(
            args.output, 0.53, 0.375, replace_callback=replace_callback,
            post_line_callback=post_line_callback)
    else:
        plt.show()
    return


if __name__ == "__main__":
    main(make_args())
