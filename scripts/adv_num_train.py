from num_train import num_train


def adv_num_train(args):
    num_train(args, 'adv_num_train', 'disc_num_train', 1,
              '\\texttt{AdvNumTrain}', 'Exploration of \\texttt{AdvNumTrain}',
              ylabel=None)

