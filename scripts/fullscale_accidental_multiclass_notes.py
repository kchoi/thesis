import pandas as pd
import matplotlib.pyplot as plt
import utils


def fullscale_accidental_multiclass_notes(args):
    datasets = [
        'ChoiDatasetV3_Valid',
        'ChoiDatasetV3_EarlyStopping20_Boot',
    ]
    value_column = [
        'PascalBoxes_Precision/mAP@0.75IOU',
        "PascalBoxes_PerformanceByCategory/AP@0.75IOU/b'BECARRE'",
        "PascalBoxes_PerformanceByCategory/AP@0.75IOU/b'BEMOL'",
        "PascalBoxes_PerformanceByCategory/AP@0.75IOU/b'DIESE'",
    ]
    df = utils.preprocess(
        args.results, [[690, 700]], datasets,
        ['name', 'dataset', 'expe', 'epoch'], value_column)
    df = df.reset_index(drop=True)

    df = utils.pivot_on_dataset(df, value_column, ['name', 'expe', 'epoch'])

    # choose best epoch based on results on small validation dataset
    _, df1 = utils.early_stopping(
        df, ['name', 'expe'], datasets[1], metric=value_column[0])
    _, df11 = utils.early_stopping(
        df1, ['name'], datasets[1], drop_col=datasets[1],
        metric=value_column[0])
    for cols in df1.columns:
        if datasets[1] in cols:
            df1 = df1.drop(columns=cols)
    df11 = df11.rename(columns={
        key: value for key, value in zip(
            value_column, ['mAP', 'Natural', 'Flat', 'Sharp'])
    })
    print(df11.name, df11.expe, df11.epoch)
    df1.columns = [cols[0] for cols in df1.columns]
    df1['order'] = '1'

    # choose best epoch based on results on valid dataset
    _, df2 = utils.early_stopping(
        df, ['name', 'expe'], datasets[0], drop_col=datasets[1],
        metric=value_column[0])
    df2['order'] = '0'
    df = pd.concat([df2, df1])
    df = df.drop(columns=['expe', 'epoch'])
    df = df.rename(columns={
        key: value for key, value in zip(
            value_column, ['mAP', 'Natural', 'Flat', 'Sharp'])
    })

    axes = df.boxplot(by=['order'], layout=(1, 4))
    axes[0].set_ylabel('Maximum AP [IoU > 0.75]')
    axes[0].set_ylim(0, 1)
    for axe in axes:
        axe.set_xticks([1, 2])
        axe.set_xticklabels(['MaxFull', 'EarlyStop'],
                            fontdict={'fontsize': 'medium'})
        axe.set_xlabel('')
        if axe.get_title() != '':
            if axe == axes[0]:
                axe.plot([2], df11[axe.get_title()][0], marker='x',
                         markersize=10, markeredgecolor='red',
                         linestyle='None', label='chosen run')
            else:
                axe.plot([2], df11[axe.get_title()][0], marker='x',
                         markersize=10, markeredgecolor='red',
                         linestyle='None')
            axe.legend(loc='lower right')

    plt.ylim(0, 1)
    plt.suptitle(
        r'Large Scale Multi-class \Method{} Evaluation Trained With\\'
        'Note Rejection'
    )

    def replace_callback(line):
        if line.startswith(r'\begin{groupplot}'):
            return line.replace(
                ']', r', width=\textwidth * 0.27, height=\textwidth * 0.5]')
        elif line == '  scale=0.6,':
            return '  scale=1.0,'
        elif line == '  rotate=0.0':
            return '  rotate=0.0,'
        elif line.startswith(r'\draw'):
            return line.replace('0.98', '1.12')
        else:
            return line

    def post_line_callback(line):
        if line == 'title={Natural},':
            return r'xlabel={Labels shown in \cref{fig:training_curves}},'
        elif line.startswith('  rotate=0.0'):
            return '  align=center'
        elif line == 'xticklabels={MaxFull,EarlyStop},':
            return r'xticklabel style={font=\scriptsize},'
        else:
            return None
    utils.save_tex(
        args.output, 0.55, 0.5, replace_callback=replace_callback,
        post_line_callback=post_line_callback)
