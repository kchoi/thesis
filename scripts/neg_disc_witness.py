import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from utils import preprocess, save_tex


def neg_disc_witness(args):
    dataset = 'AccidentalDataset'
    value_column = 'PascalBoxes_Precision/mAP@0.75IOU'
    df = preprocess(args.results, [[550, 560]], [dataset],
                    ['name', 'expe', 'run', 'train', 'epoch'], value_column)

    df = df.groupby(by=['name', 'expe', 'run', 'train'])
    df = df['PascalBoxes_Precision/mAP@0.75IOU']
    df = df.agg(maximum=np.max)
    df = df.reset_index()
    df = pd.pivot_table(
        df, values='maximum', index=['expe', 'train'], columns='name')
    df = df.rename(columns={
        'seg_gan_flat': 'Flat',
        'seg_gan_natural': 'Natural',
        'seg_gan_singleclass': 'Sharp',
    })
    df = df.reset_index().drop(columns=['expe', 'train'])
    axes = df.boxplot()
    axes.legend(loc='best')
    plt.ylim(0, 1)
    plt.ylabel('Maximum mAP [IoU > 0.75]')
    plt.xlabel('Symbol Class Label')
    plt.title('Baseline Experiment for \\Method-Neg Experiment')
    # plt.tight_layout()

    save_tex(args.output, 0.5)
