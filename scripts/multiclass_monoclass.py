import utils
import pandas as pd
import matplotlib.pyplot as plt


def multiclass_monoclass(args):
    datasets = ['AccidentalDataset']
    value_column = [
        'PascalBoxes_Precision/mAP@0.75IOU',
        "PascalBoxes_PerformanceByCategory/AP@0.75IOU/b'BECARRE'",
        "PascalBoxes_PerformanceByCategory/AP@0.75IOU/b'BEMOL'",
        "PascalBoxes_PerformanceByCategory/AP@0.75IOU/b'DIESE'",
    ]

    df = utils.preprocess(args.results, [[620, 630]], datasets,
                          ['name', 'expe', 'epoch'], value_column[0])
    df = df.reset_index(drop=True)

    _, df = utils.early_stopping(df, ['name', 'expe'], value_column[0])
    print(df.iloc[df.groupby(by=['name'])[value_column[0]].idxmax(axis=1)])
    print(df.groupby(by=['name'])[value_column[0]].std())
    df = utils.pivot_on_name(df, value_column[0], ['expe'])
    df = df.drop(columns=['expe'])
    df['Experiment'] = 'MonoClass'

    df1 = utils.preprocess(
        args.results, [[660, 670]], datasets, ['name', 'expe', 'epoch'],
        value_column)
    df1 = df1.reset_index(drop=True)
    print(df1.iloc[df1[value_column[0]].idxmax(axis=1)])
    print("standard deviation by class")
    print(df1.iloc[df1.groupby(by='expe')[value_column[0]].idxmax(axis=1)][value_column].std())

    # choose best epoch based on results on small validation dataset
    _, df1 = utils.early_stopping(df1, ['name', 'expe'], value_column[0])

    df1 = df1.drop(columns=['expe', value_column[0]])
    df1 = df1.rename(columns={
        key: value for key, value in zip(
            value_column[1:], ['Natural', 'Flat', 'Sharp'])
    })
    df1['Experiment'] = 'MultiClass'
    df1 = df1.drop(columns=['name', 'epoch'])
    df = pd.concat([df, df1])

    axes = df.boxplot(by=['Experiment'], layout=(1, 3))
    axes[0].set_ylabel('Maximum AP [IoU > 0.75]')
    axes[0].set_ylim(0, 1)
    axes[0].set_xlabel('')
    axes[2].set_xlabel('')
    for axe in axes:
        axe.set_xticks([1, 2])

    plt.ylim(0, 1)
    plt.suptitle(
        r'Mono-class \Method{} vs Multi-class \Method{}'
    )

    def replace_callback(line):
        if line.startswith(r'\begin{groupplot}'):
            return line.replace(
                ']', r', width=\textwidth * 0.35, height=\textwidth * 0.5]')
        elif line == '  scale=0.6,':
            return '  scale=1.0,'
        elif line == '  rotate=0.0':
            return '  rotate=0.0,'
        elif line.startswith(r'\draw'):
            return line.replace('0.98', '1.12')
        else:
            return line

    def post_line_callback(line):
        if line.startswith('xticklabels={'):
            return r'xticklabel style={font=\small},'
        else:
            return None
    utils.save_tex(
        args.output, 0.55, 0.5, replace_callback=replace_callback,
        post_line_callback=post_line_callback)
    return
