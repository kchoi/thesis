import itertools
import pandas as pd
import matplotlib.pyplot as plt
import utils


def early_stopping(args, expe_range, small_dataset, small_dataset_legend,
                   argmax_legend, title):
    df = pd.read_csv(args.results, low_memory=False)
    expes = [f"expe-{i}" for i in itertools.chain(
        range(*expe_range))]
    df = df.query(f"expe in {expes}")
    df = df[[
        'name', 'dataset', 'expe', 'train', 'epoch',
        'PascalBoxes_Precision/mAP@0.75IOU',
    ]]
    df = pd.pivot_table(df, values='PascalBoxes_Precision/mAP@0.75IOU',
                        index=['name', 'expe', 'train', 'epoch'],
                        columns=['dataset'])
    df = df.reset_index()
    df_group = df.groupby(by=['name', 'expe', 'train'])
    df_idxmax = df_group.idxmax()

    df_OtherDataset = df.iloc[df_idxmax[small_dataset]]
    df_OtherDataset = df_OtherDataset.set_index(
        ['name', 'expe', 'train']).drop(
            columns=['epoch'])

    df_AccidentalDataset = df.iloc[df_idxmax['AccidentalDataset']]
    df_AccidentalDataset = df_AccidentalDataset.set_index(
        ['name', 'expe', 'train']).drop(
            columns=['epoch', small_dataset])

    df = df_AccidentalDataset.join(
        df_OtherDataset, on=['name', 'expe', 'train'],
        rsuffix='_argmax')
    df = df.reset_index().drop(columns=['expe', 'train'])

    fig, axes = plt.subplots(1, 3, sharex=True, sharey=True,
                             figsize=(7.7, 4.8))
    # to have a common xlabel
    # add a big axis, hide frame
    fig.add_subplot(111, frameon=False)
    # hide tick and tick label of the big axis
    plt.tick_params(labelcolor='none', top=False, bottom=False, left=False,
                    right=False)
    label_map = {
        'seg_gan_singleclass': 'Sharp',
        'seg_gan_flat': 'Flat',
        'seg_gan_natural': 'Natural',
    }
    df = df.rename(columns={
        'AccidentalDataset': 'Full Training Dataset',
        'AccidentalDataset_argmax': argmax_legend,
        small_dataset: small_dataset_legend,
    })
    for i, (group_name, group) in enumerate(df.groupby(by='name')):
        group = group.reset_index(drop=True)
        group = group.sort_values([small_dataset_legend], ascending=False)
        group = group[['Full Training Dataset', small_dataset_legend,
                       argmax_legend]]
        ax = group.plot.bar(ax=axes[i])
        # if i == 0:
            # ax.legend(loc='lower right')
        # else:
        ax.get_legend().remove()
        ax.set_title(label_map[group_name])
        plt.ylim(0, 1)
    handles, labels = ax.get_legend_handles_labels()
    fig.legend(handles, labels, loc=(0.1, 0.15))
    plt.ylabel('mAP [IoU > 0.75]')
    plt.xlabel('Training Id')
    fig.suptitle(title)

    plt.tight_layout(rect=[-0.04, -0.04, 1, 0.95])
    plt.savefig(args.output)


def early_stopping_boxplot(args, expes, dataset, title):
    value_column = 'PascalBoxes_Precision/mAP@0.75IOU'
    datasets = ['AccidentalDataset', dataset]
    df = utils.preprocess(
        args.results, expes, datasets,
        ['dataset', 'name', 'expe', 'train', 'epoch'], value_column)

    df = utils.pivot_on_dataset(
        df, value_column, ['name', 'expe', 'train', 'epoch'])

    # choose best epoch based on results on small training dataset
    _, df1 = utils.early_stopping(df, ['name', 'expe', 'train'], dataset)
    print(len(df1))
    # choose best among 10 runs
    _, df11 = utils.early_stopping(df1, ['name'], dataset, drop_col=dataset)
    df11 = utils.pivot_on_name(df11, 'AccidentalDataset', [])

    df1 = df1.drop(columns=dataset)
    df1['order'] = '1'

    # choose best epoch based on results on whole training dataset
    _, df2 = utils.early_stopping(
        df, ['name', 'expe', 'train'], 'AccidentalDataset', drop_col=dataset)
    df2['order'] = '0'
    df = pd.concat([df2, df1])
    df = utils.pivot_on_name(df, 'AccidentalDataset',
                             ['expe', 'order'])
    df = df.drop(columns='expe')

    axes = df.boxplot(by=['order'], layout=(1, 3))

    axes[0].set_ylabel('Maximum mAP [IoU > 0.75]')
    axes[0].set_ylim(0, 1)
    for axe in axes:
        axe.set_xticks([1, 2])
        axe.set_xticklabels(['MaxFull', 'EarlyStop'])
        axe.set_xlabel('')
        if axe.get_title() != '':
            axe.plot([2], df11[axe.get_title()][0], marker='x', markersize=10,
                     markeredgecolor='red', linestyle='None',
                     label='chosen run')
            axe.legend(loc='lower right')

    plt.ylim(0, 1)
    plt.suptitle(title)

    def replace_callback(line):
        if line.startswith(r'\begin{groupplot}'):
            return line.replace(
                ']', r', width=\textwidth * 0.35, height=\textwidth * 0.5]')
        elif line == '  scale=0.6,':
            return '  scale=1.0,'
        elif line == '  rotate=0.0':
            return '  rotate=0.0,'
        elif line.startswith(r'\draw'):
            return line.replace('0.98', '1.12')
        else:
            return line

    def post_line_callback(line):
        if line == 'title={Natural},':
            return r'xlabel={Labels shown in \cref{fig:training_curves}},'
        elif line.startswith('  rotate=0.0'):
            return '  align=center'
        else:
            return None
    utils.save_tex(
        args.output, 0.55, 0.5, replace_callback=replace_callback,
        post_line_callback=post_line_callback)
