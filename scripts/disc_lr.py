import pandas as pd
from plot_pixel_acc import plot_pixel_acc


def disc_lr(args):
    df = pd.read_csv(args.results, low_memory=False)
    df = df[df.run == 'expe-12']
    df = df[[
        'disc_lr',
        'mean max_weighted_pixel_acc',
        'mean min_weighted_pixel_acc',
        'mean mean_weighted_pixel_acc',
        'std max_weighted_pixel_acc',
        'std min_weighted_pixel_acc',
        'std mean_weighted_pixel_acc',
    ]]
    mean = df[[
        'disc_lr',
        'mean max_weighted_pixel_acc',
        'mean min_weighted_pixel_acc',
        'mean mean_weighted_pixel_acc',
    ]]
    mean = mean.rename(columns={
        'mean max_weighted_pixel_acc': 'maximum',
        'mean mean_weighted_pixel_acc': 'average',
        'mean min_weighted_pixel_acc': 'minimum',
    })
    std = df[[
        'disc_lr',
        'std max_weighted_pixel_acc',
        'std min_weighted_pixel_acc',
        'std mean_weighted_pixel_acc',
    ]]
    std = std.rename(columns={
        'std max_weighted_pixel_acc': 'maximum',
        'std mean_weighted_pixel_acc': 'average',
        'std min_weighted_pixel_acc': 'minimum',
    })
    columns = ['maximum', 'average', 'minimum']
    error_low = mean[columns] - std[columns]
    error_high = mean[columns] + std[columns]
    plot_pixel_acc(
        args, mean, error_low, error_high, columns, 'disc_lr',
        'Discriminator Learning Rate', 'Exploration of discriminator lr',
        ylabel=None)
