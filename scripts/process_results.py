import argparse
from needed_data import needed_data
from adv_lr import adv_lr
from disc_lr import disc_lr
from adv_num_train import adv_num_train
from disc_num_train import disc_num_train
from sg_lr import sg_lr
from sg_lr_witness import sg_lr_witness
from neg_disc import neg_disc
from neg_disc_witness import neg_disc_witness
from early_stopping10 import early_stopping10
from early_stopping20 import early_stopping20
from early_stopping_bootstrap10 import early_stopping_bootstrap10
from early_stopping_bootstrap20 import early_stopping_bootstrap20
from training_curves import training_curves
from reject_ratio import reject_ratio
from sizes_high import sizes_high
from multiclass_earlystopping import multiclass_earlystopping
from multiclass_monoclass import multiclass_monoclass
from fullscale_accidental_multiclass import fullscale_accidental_multiclass
from IMSLP00022_accidental_multiclass import IMSLP00022_accidental_multiclass
from fullscale_accidental_multiclass_notes import \
    fullscale_accidental_multiclass_notes
from fullscale_accidental_multiclass_notes_clippaste import \
    fullscale_accidental_multiclass_notes_clippaste


def make_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("results", help="input results csv file")
    subparser = parser.add_subparsers(help="figure")
    needed_data_parser = subparser.add_parser("needed_data")
    needed_data_parser.set_defaults(func=needed_data)
    adv_lr_parser = subparser.add_parser("adv_lr")
    adv_lr_parser.set_defaults(func=adv_lr)
    disc_lr_parser = subparser.add_parser("disc_lr")
    disc_lr_parser.set_defaults(func=disc_lr)
    adv_num_train_parser = subparser.add_parser("adv_num_train")
    adv_num_train_parser.set_defaults(func=adv_num_train)
    disc_num_train_parser = subparser.add_parser("disc_num_train")
    disc_num_train_parser.set_defaults(func=disc_num_train)
    sg_lr_parser = subparser.add_parser("sg_lr")
    sg_lr_parser.set_defaults(func=sg_lr)
    sg_lr_witness_parser = subparser.add_parser("sg_lr_witness")
    sg_lr_witness_parser.set_defaults(func=sg_lr_witness)
    neg_disc_parser = subparser.add_parser("neg_disc")
    neg_disc_parser.set_defaults(func=neg_disc)
    neg_disc_witness_parser = subparser.add_parser("neg_disc_witness")
    neg_disc_witness_parser.set_defaults(func=neg_disc_witness)
    early_stopping10_parser = subparser.add_parser("early_stopping10")
    early_stopping10_parser.set_defaults(func=early_stopping10)
    early_stopping20_parser = subparser.add_parser("early_stopping20")
    early_stopping20_parser.set_defaults(func=early_stopping20)
    early_stopping_bootstrap10_parser = subparser.add_parser("early_stopping_bootstrap10")
    early_stopping_bootstrap10_parser.set_defaults(func=early_stopping_bootstrap10)
    early_stopping_bootstrap20_parser = subparser.add_parser("early_stopping_bootstrap20")
    early_stopping_bootstrap20_parser.set_defaults(func=early_stopping_bootstrap20)
    training_curves_parser = subparser.add_parser("training_curves")
    training_curves_parser.set_defaults(func=training_curves)
    reject_ratio_parser = subparser.add_parser("reject_ratio")
    reject_ratio_parser.set_defaults(func=reject_ratio)
    sizes_high_parser = subparser.add_parser("sizes_high")
    sizes_high_parser.set_defaults(func=sizes_high)
    multiclass_earlystopping_parser = subparser.add_parser("multiclass_earlystopping")
    multiclass_earlystopping_parser.set_defaults(func=multiclass_earlystopping)
    multiclass_monoclass_parser = subparser.add_parser("multiclass_monoclass")
    multiclass_monoclass_parser.set_defaults(func=multiclass_monoclass)
    fullscale_accidental_multiclass_parser = subparser.add_parser("fullscale_accidental_multiclass")
    fullscale_accidental_multiclass_parser.set_defaults(func=fullscale_accidental_multiclass)
    IMSLP00022_accidental_multiclass_parser = subparser.add_parser("IMSLP00022_accidental_multiclass")
    IMSLP00022_accidental_multiclass_parser.set_defaults(func=IMSLP00022_accidental_multiclass)
    fullscale_accidental_multiclass_notes_parser = subparser.add_parser("fullscale_accidental_multiclass_notes")
    fullscale_accidental_multiclass_notes_parser.set_defaults(func=fullscale_accidental_multiclass_notes)
    fullscale_accidental_multiclass_notes_clippaste_parser = subparser.add_parser("fullscale_accidental_multiclass_notes_clippaste")
    fullscale_accidental_multiclass_notes_clippaste_parser.set_defaults(func=fullscale_accidental_multiclass_notes_clippaste)
    parser.add_argument("--output")
    args = parser.parse_args()
    return args


def main(args):
    args.func(args)


if __name__ == "__main__":
    main(make_args())
