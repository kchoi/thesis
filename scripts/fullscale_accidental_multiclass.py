import pandas as pd
import matplotlib.pyplot as plt
import utils


def fullscale_accidental_multiclass(args):
    datasets = ['ChoiDataset_EarlyStopping20']
    value_column = [
        'PascalBoxes_Precision/mAP@0.75IOU',
        "PascalBoxes_PerformanceByCategory/AP@0.75IOU/b'BECARRE'",
        "PascalBoxes_PerformanceByCategory/AP@0.75IOU/b'BEMOL'",
        "PascalBoxes_PerformanceByCategory/AP@0.75IOU/b'DIESE'",
    ]
    df = utils.preprocess(
        args.results, [[670, 680]], datasets,
        ['name', 'dataset', 'expe', 'epoch'], value_column)
    df = df.reset_index(drop=True)

    # df = utils.pivot_on_dataset(df, value_column, ['name', 'expe', 'epoch'])

    # choose best epoch based on results on small validation dataset
    _, df1 = utils.early_stopping(df, ['name', 'expe'], value_column[0])
    print(df1[['expe', 'epoch']])
    _, df11 = utils.early_stopping(df1, ['name'], value_column[0])
    print(df11.name, df11.expe, df11.epoch)
    df1 = df1[value_column]
    df1 = df1.rename(columns={
        value_column[0]: "mAP",
        value_column[1]: "Natural",
        value_column[2]: "Flat",
        value_column[3]: "Sharp",
    })
    df1 = pd.melt(df1, var_name="Label", value_name="AP")
    df1.boxplot("AP", by=['Label'])
    plt.ylim(0, 1)
    plt.title(r"Full Scale Experiment using Small Validation Dataset\\20 Examples With Bootstrapping")
    plt.suptitle('')

    def replace_callback(line):
        return line

    def post_line_callback(line):
        return None

    utils.save_tex(
        args.output, 0.55, 0.5, replace_callback=replace_callback,
        post_line_callback=post_line_callback)
    return
