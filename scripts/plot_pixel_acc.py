import matplotlib.pyplot as plt
import tikzplotlib


def plot_pixel_acc(args, mean, error_low, error_high, columns, xkey, xlabel,
                   title, ylabel='Pixel Accuracy', xscale='log'):
    axes = mean.plot(x=xkey, y=columns)
    axes.legend(loc='lower right')
    for key in columns:
        plt.fill_between(mean[xkey], error_low[key], error_high[key],
                         alpha=0.2)
    plt.grid(True)
    plt.xscale(xscale)
    plt.ylim(0, 1)
    plt.ylabel(ylabel)
    plt.xlabel(xlabel)
    plt.title(title)
    plt.tight_layout()

    code = tikzplotlib.get_tikz_code()
    new_code = []
    for line in code.split('\n'):
        new_code.append(line)
        if line == '\\begin{axis}[':
            new_code.append('scale only axis,')
            new_code.append('width=\\textwidth * 0.38,')
    with open(args.output, 'w') as f:
        f.write('\n'.join(new_code))
