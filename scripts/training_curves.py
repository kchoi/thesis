import pandas as pd
import matplotlib.pyplot as plt
import tikzplotlib


def training_curves(args):
    df = pd.read_csv(args.results, low_memory=False)
    df = df.query(f"name == 'seg_gan_natural' and expe == 'expe-381' and train == 'train-0'")
    df = df[[
        'dataset', 'epoch',
        'PascalBoxes_Precision/mAP@0.75IOU',
    ]]
    df = pd.pivot_table(df, values='PascalBoxes_Precision/mAP@0.75IOU',
                        index=['epoch'],
                        columns=['dataset'])
    df = df.rename(columns={
        'AccidentalDataset': 'Full Training Dataset',
        'AccidentalDataset10': 'Small Validation Dataset 10 ex/class',
    })
    df = df.ewm(span=10).mean()
    ax = df.plot(figsize=(6.8, 4.8))
    ax.legend(loc='lower right')
    box_text_size = 15
    for line in ax.get_lines():
        epoch = df[line.get_label()].argmax()
        value = df[line.get_label()].max()
        color = line.get_color()
        ax.plot([0, epoch], [value, value], color=color,
                linestyle='--', linewidth=6)
        ax.plot([epoch, epoch], [0, value], color=color,
                linestyle='--', linewidth=6)
        x = epoch - 80
        y = value + 0.02
        bbox_props = dict(boxstyle="round", ec="black", lw=2, fc='white')
        if line.get_label() == "Full Training Dataset":
            ax.text(x, y, f"MaxFull: max on {line.get_label()}", ha="left",
                    va="bottom", size=box_text_size, color=line.get_color(),
                    bbox=bbox_props)
        else:
            ax.text(x, y, f"MaxSmall: max on {line.get_label()}", ha="left",
                    va="bottom", size=box_text_size, color=line.get_color(),
                    bbox=bbox_props)
    epoch_valid = df['Small Validation Dataset 10 ex/class'].argmax()
    train_value = df['Full Training Dataset'].loc[epoch_valid]
    line = ax.plot([0, epoch_valid], [train_value, train_value],
                   linestyle='--', linewidth=6)[0]
    ax.plot([epoch_valid, epoch_valid], [0, train_value],
            linestyle='--', color=line.get_color(), linewidth=6)
    x = epoch_valid + 10
    y = train_value
    bbox_props = dict(boxstyle="round", ec="black", lw=2, fc='white')
    ax.text(
        x, y, r"EarlyStop: mAP on Full Training Dataset\\using Early-Stopping",
        ha="left", va="center", size=box_text_size, color=line.get_color(),
        bbox=bbox_props)
    plt.ylim(0, 1.05)
    plt.ylabel('mAP [IoU > 0.75]')
    plt.title(r"mAP during GAN training measured for the Fully Annotated Training Dataset\\"
              "and Small Validation Dataset with 10 examples per class")
    # import os
    # plt.savefig(os.path.splitext(args.output)[0] + ".pdf")

    code = tikzplotlib.get_tikz_code()
    new_code = []
    for line in code.split('\n'):
        if line.startswith("title="):
            new_code.append('align=center,')
        new_code.append(line)
        if line == '\\begin{axis}[':
            new_code.append('scale only axis,')
            new_code.append('width=\\textwidth * 0.7,')
    with open(args.output, 'w') as f:
        f.write('\n'.join(new_code))
