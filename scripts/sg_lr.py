import numpy as np
import matplotlib.pyplot as plt
import utils


def sg_lr(args):
    value_column = 'PascalBoxes_Precision/mAP@0.75IOU'
    datasets = ['AccidentalDataset', 'ChoiDataset_EarlyStopping20']
    df = utils.preprocess(
        args.results, [[560, 570]], datasets,
        ['sg_lr', 'dataset', 'name', 'expe', 'train', 'epoch'], value_column)

    # get sg_lr range
    sg_lr_min, sg_lr_max = df.sg_lr.agg([np.min, np.max])

    df = utils.pivot_on_dataset(
        df, value_column, ['sg_lr', 'name', 'expe', 'train', 'epoch'])

    # choose best epoch based on results on whole training dataset
    _, df = utils.early_stopping(df, ['sg_lr', 'name', 'expe', 'train'],
                                 'AccidentalDataset')
    print(df.sg_lr.unique())
    print(len(df))
    # print('CD_ES20_max_epoch')
    # print(CD_ES20_max_epoch)

    # plot median and 1st & 3rd quantile of CD_ES20_max_epoch
    df1 = utils.pivot_on_name(df, 'AccidentalDataset', ['sg_lr', 'expe'])
    df1 = df1.drop(columns='expe')
    # print('CD_ES20_max_epoch_AD')
    # print(CD_ES20_max_epoch_AD)

    results = utils.compute_boxplot(df1, 'sg_lr')
    ax = plt.gca()
    results['max'].plot(ax=ax)
    ax.set_prop_cycle(None)
    results['median'].plot(linestyle='--', ax=ax, legend=False)
    for i, line in enumerate(ax.lines):
        if i >= 3:
            line.set_label('')
    for key in results['median'].columns:
        plt.fill_between(results['median'].index, results['1st_quantile'][key],
                         results['3rd_quantile'][key], alpha=0.3)
    ax.legend(loc='best')
    plt.grid(True, which="both")
    plt.xscale('log')
    plt.ylim(0, 1)
    plt.ylabel('Maximum mAP [IoU > 0.75]')
    plt.xlabel('Reconstruction Learning Rate')
    plt.title('Exploration of reconstruction learning rate')

    # plt.tight_layout()

    # import os
    # plt.savefig(os.path.splitext(args.output)[0] + ".pdf")

    def post_line_callback(line):
        if line.startswith('legend style='):
            return 'legend columns=2,'
        elif line == ']':
            return ['\\addlegendimage{empty legend}',
                    '\\addlegendentry{Labels}',
                    '\\addlegendimage{empty legend}',
                    '\\addlegendentry{Stats}']
        elif line == '\\addlegendentry{Flat}':
            return ['\\addlegendimage{no markers, color0}',
                    '\\addlegendentry{Max}']
        elif line == '\\addlegendentry{Natural}':
            return ['\\addlegendimage{no markers, color0, dashed}',
                    '\\addlegendentry{Median}']
        elif line == '\\addlegendentry{Sharp}':
            return ['\\addlegendimage{no markers, color0, area legend, fill=color0}',
                    '\\addlegendentry{\\nth{1}/\\nth{3} Quartiles}']
        else:
            return None
    utils.save_tex(args.output, 0.7, 0.5,
                   post_line_callback=post_line_callback)
