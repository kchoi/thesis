import itertools
import numpy as np
import pandas as pd
from plot_pixel_acc import plot_pixel_acc


def adv_lr(args):
    xkey = 'adv_lr'
    df = pd.read_csv(args.results, low_memory=False)
    expes = [f"expe-{i}" for i in itertools.chain(
        range(10, 12))]
    df = df.query(f"expe in {expes}")
    df = df[df.name == 'seg_gan_singleclass']
    df = df[[
        'expe', 'run', 'train', 'epoch',
        xkey,
        'white_pixel_acc', 'black_pixel_acc',
    ]]
    df = df[df.epoch > 10]
    df['pixel_acc'] = (df['white_pixel_acc'] + df['black_pixel_acc']) / 2
    agg_dict = {
        'maximum': np.max,
        'average': np.mean,
        'minimum': np.min
    }
    df = df.groupby(by=[xkey, 'expe', 'run', 'train'])['pixel_acc'].agg(
        **agg_dict)
    df = df.reset_index()
    mean = df.groupby(by=[xkey]).mean().reset_index()
    std = df.groupby(by=[xkey]).std().reset_index()
    error_low = mean[list(agg_dict.keys())] - std[list(agg_dict.keys())]
    error_high = mean[list(agg_dict.keys())] + std[list(agg_dict.keys())]

    plot_pixel_acc(
        args, mean, error_low, error_high, list(agg_dict.keys()), xkey,
        'Adversarial Learning Rate', 'Exploration of adversarial lr')
