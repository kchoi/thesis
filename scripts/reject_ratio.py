import matplotlib.pyplot as plt
import utils


def reject_ratio(args):
    datasets = ['AccidentalDataset', 'ChoiDataset_EarlyStopping20']
    value_column = 'PascalBoxes_Precision/mAP@0.75IOU'
    df = utils.preprocess(
        args.results, [[600, 610], [650, 660]], datasets,
        ['batch_size', 'name', 'dataset', 'expe', 'epoch'], value_column)
    df = df.copy()

    def batch_size2reject_ratio(x):
        a, b = [int(a) for a in x.strip('[]').split()]
        return a / (a + b)
    df['reject_ratio'] = df['batch_size'].apply(batch_size2reject_ratio)
    df = df.drop(columns=['batch_size'])

    df = utils.pivot_on_dataset(
        df, value_column, ['reject_ratio', 'name', 'expe', 'epoch'])

    # choose best epoch based on results on small validation dataset
    _, df = utils.early_stopping(
        df, ['reject_ratio', 'name', 'expe'], 'ChoiDataset_EarlyStopping20')

    # plot median and 1st & 3rd quantile of CD_ES20_max_epoch
    df1 = utils.pivot_on_name(
        df, 'AccidentalDataset', ['reject_ratio', 'expe'])
    df1 = df1.drop(columns='expe')

    results = utils.compute_boxplot(df1, 'reject_ratio')
    results['median'].plot(linestyle='--')
    for key in results['median'].columns:
        plt.fill_between(results['median'].index, results['1st_quantile'][key],
                         results['3rd_quantile'][key], alpha=0.3)
    plt.grid(True, which="both")
    plt.ylim(0, 1)
    plt.xlim(0, 1)
    plt.ylabel('mAP [IoU > 0.75]')
    plt.xlabel('Ratio of Symbols to Detect')
    plt.title(r"Impact of Rejection on Accidental Detection using \Method{}"
              r"\\Using Small Validation Dataset for Early Stopping")
    # plt.tight_layout()

    # choose best training based on results on small validation dataset
    _, df2 = utils.early_stopping(
        df, ['reject_ratio', 'name'], 'ChoiDataset_EarlyStopping20')

    # plot CD_ES20_max_expe as a point
    df2 = utils.pivot_on_name(df2, 'AccidentalDataset', ['reject_ratio'])

    # reset colors
    plt.gca().set_prop_cycle(None)
    plt.plot(df2['reject_ratio'], df2['Flat'], marker='.',
             label="Best trained model chosen using Early Stopping")
    plt.plot(df2['reject_ratio'], df2[['Natural', 'Sharp']], marker='.')
    plt.legend()

    def post_line_callback(line):
        if line.startswith('legend style='):
            return 'legend columns=2,'
        elif line == ']':
            return ['\\addlegendimage{empty legend}',
                    '\\addlegendentry{Labels}',
                    '\\addlegendimage{empty legend}',
                    '\\addlegendentry{Stats}']
        elif line == '\\addlegendentry{Flat}':
            return ['\\addlegendimage{no markers, color0, dashed}',
                    '\\addlegendentry{Median}']
        elif line == '\\addlegendentry{Natural}':
            return ['\\addlegendimage{no markers, color0, area legend, fill=color0}',
                    '\\addlegendentry{\\nth{1}/\\nth{3} Quartiles}']
        else:
            return None

    utils.save_tex(args.output, 0.85, 0.4,
                   post_line_callback=post_line_callback)
