import os
import glob
from copy import deepcopy
import argparse
import shutil
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.image as mpimage
import matplotlib.patches as mppatches
import matplotlib.colors as mpcolors
from tqdm import tqdm


def make_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("predictions")
    parser.add_argument("groundtruths")
    parser.add_argument("windows")
    parser.add_argument("prediction_images")
    parser.add_argument("groundtruth_page_images")
    parser.add_argument("--output")
    parser.add_argument("--legend", action='store_true', default=False)
    parser.add_argument("--image", action='store_true', default=False)
    args = parser.parse_args()
    return args


def box2int(box):
    return [int(x) for x in box.split('x')]


def box2rect(box, offset=(0, 0), color=(1, 0, 0, 0.75)):
    box = box2int(box)
    xy = (box[0] - offset[0], box[1] - offset[1])
    width = box[2] - box[0]
    height = box[3] - box[1]
    rect = mppatches.Rectangle(xy, width, height, linewidth=2,
                               edgecolor=color, facecolor='none')
    return rect


def load_csvs(args):
    predictions = pd.read_csv(args.predictions, comment='#')
    # print(predictions)
    groundtruths = pd.read_csv(args.groundtruths, comment='#')
    # print(groundtruths)
    windows = pd.read_csv(args.windows)
    # print(windows)
    return predictions, groundtruths, windows


def glob_images(args):
    prediction_images = glob.glob(
        os.path.join(args.prediction_images, "**/*.jpg"))
    groundtruth_page_images = glob.glob(
        os.path.join(args.groundtruth_page_images, "**/*.png"))
    return prediction_images, groundtruth_page_images


def get_image(names, window, box=True):
    if box:
        found_name = [
            name for name in names
            if window.page in name and window.box in name
        ]
    else:
        found_name = [name for name in names if window.page in name]
    if len(found_name) != 1:
        if box:
            print(f"missing {window.page} {window.box}")
        else:
            print(f"missing {window.page}")
        raise Exception()
    found_name = found_name[0]
    image = mpimage.imread(found_name)
    return image


def main(args):
    predictions, groundtruths, windows = load_csvs(args)

    if args.image:
        prediction_images, groundtruth_page_images = glob_images(args)

    empty = {
        key: pd.DataFrame().reindex(columns=value.columns)
        for key, value in zip(['gt', 'pred'], [groundtruths, predictions])}
    conditions = {
        "has_tp": lambda gt, pred: len(pred.query("tp_fp == True")) > 0,
        "all_tp": lambda gt, pred: len(gt) > 0 and len(pred) == len(gt) and
        False not in pred.tp_fp.to_list(),
        "all_tp_1_gt": lambda gt, pred: len(gt) == 1 and len(pred) == len(gt)
        and False not in pred.tp_fp.to_list(),
        "all_tp_2_gt": lambda gt, pred: len(gt) == 2 and len(pred) == len(gt)
        and False not in pred.tp_fp.to_list(),
        "all_tp_3_gt": lambda gt, pred: len(gt) == 3 and len(pred) == len(gt)
        and False not in pred.tp_fp.to_list(),
        "tp_fp_missing": lambda gt, pred:
        (len(gt) != len(pred.query("tp_fp == True"))
         or len(pred.query("tp_fp == False")) > 0) and
        len(pred.query("tp_fp == True")) > 0,
        "no_pred_no_gt": lambda gt, pred: len(pred) == 0 and len(gt) == 0,
        "no_pred_1_gt": lambda gt, pred: len(pred) == 0 and len(gt) == 1,
        "no_pred_2_gt": lambda gt, pred: len(pred) == 0 and len(gt) == 2,
        "has_fp": lambda gt, pred: False in pred.tp_fp.to_list(),
        "has_fp_iou_over_0": lambda gt, pred: False in pred.tp_fp.to_list() and
        len(pred.query("iou > 0")) > 0,
        "iou_at_0": lambda gt, pred: len(pred.query("iou == 0")) > 0,
        "multi_pred": lambda gt, pred: len(pred) > 1,
        "gt_not_found": lambda gt, pred: len(gt) > 0 and len(pred) < len(gt),
        "clipped": lambda gt, pred: True in gt.clipped.to_list(),
        "clipped_has_fp": lambda gt, pred:
        True in gt.clipped.to_list() and False in pred.tp_fp.to_list(),
        "clipped_not_found": lambda gt, pred:
        True in gt.clipped.to_list() and len(gt) > len(pred),
        "not_clipped_has_fp": lambda gt, pred:
        True not in gt.clipped.to_list() and False in pred.tp_fp.to_list(),
        "not_clipped_has_fp_iou_over_0": lambda gt, pred:
        True not in gt.clipped.to_list() and
        len(pred.query("iou > 0 and tp_fp == False")) > 0,
        "not_clipped_has_fp_iou_at_0": lambda gt, pred:
        True not in gt.clipped.to_list() and
        len(pred.query("iou == 0 and tp_fp == False")) > 0,
        "not_clipped_has_fp_iou_at_0_no_hallucinate": lambda gt, pred:
        True not in gt.clipped.to_list() and
        len(pred.query("iou == 0 and tp_fp == False and hallucinate == False")) > 0,
        "not_clipped_not_found": lambda gt, pred:
        True not in gt.clipped.to_list() and len(gt) > len(pred),
        "hallucinate": lambda gt, pred:
        len(pred.query("hallucinate == True")) > 0,
        "border": lambda gt, pred:
        len(pred.query("border == True")) > 0,
    }
    groups = {key: deepcopy(empty) for key in conditions}

    for key in conditions:
        d = os.path.join(args.output, key)
        if os.path.exists(d):
            shutil.rmtree(d)
        os.makedirs(d)

    for _, window in tqdm(windows.iterrows()):
        window_predictions = predictions.query(
            f"page == '{window.page}' and region_window == '{window.box}'")
        window_groundtruths = groundtruths.query(
            f"page == '{window.page}' and region_window == '{window.box}'")

        def concat(df, pred=None, gt=None):
            if not pred:
                pred = window_predictions
            if not gt:
                gt = window_groundtruths
            df['pred'] = pd.concat([df['pred'], pred])
            df['gt'] = pd.concat([df['gt'], gt])

        basename = f"{window.page}_{window.box}.pdf"
        output = os.path.join(args.output, basename)

        if args.image and not os.path.exists(output):
            save_image(window, window_predictions, window_groundtruths,
                       prediction_images, groundtruth_page_images, output,
                       args.legend)

        for key, condition in conditions.items():
            if condition(window_groundtruths, window_predictions):
                concat(groups[key])
                os.link(output, os.path.join(args.output, key, basename))

    for key, group in groups.items():
        group['pred'].to_csv(os.path.join(args.output, key, "predictions.csv"),
                             index=False)
        group['gt'].to_csv(os.path.join(args.output, key, "groundtruths.csv"),
                           index=False)
    return


def save_image(window, window_predictions, window_groundtruths,
               prediction_images, groundtruth_page_images, output, legend):
    bbox_props = dict(boxstyle="round", ec="black", lw=1, fc=(1, 1, 1, 0.5))
    window_prediction_image = get_image(prediction_images, window)

    window_groundtruth_page_image = get_image(
        groundtruth_page_images, window, box=False)

    window_box = box2int(window.box)
    window_groundtruth_image = window_groundtruth_page_image[
        window_box[1]:window_box[3], window_box[0]:window_box[2]]

    fig: plt.Figure = plt.figure()

    for i in range(1, 3):
        ax: plt.Axes = fig.add_subplot(1, 2, i)
        ax.tick_params(axis='both', which='both', bottom=False, left=False,
                       labelbottom=False, labelleft=False)
        if i == 1:
            image = window_groundtruth_image
            ax.set_title('Original Image')
            norm = mpcolors.Normalize(0, 1)
        if i == 2:
            image = window_prediction_image
            ax.set_title('Generated Image')
            norm = mpcolors.Normalize(0, 255)
        _ = plt.imshow(image, cmap='gray', norm=norm)
        for _, window_groundtruth in window_groundtruths.iterrows():
            rect = box2rect(
                window_groundtruth.box, (window_box[0], window_box[1]),
                (0, 1, 0, 0.75))
            ax.add_patch(rect)

        for _, window_prediction in window_predictions.iterrows():
            if window_prediction.tp_fp:
                color = (0, 0, 1, 0.75)
            else:
                color = (1, 0, 0, 0.75)
            rect = box2rect(
                window_prediction.box, (window_box[0], window_box[1]),
                color)
            ax.add_patch(rect)
            x, y = rect.get_xy()
            if y > 10:
                va = 'bottom'
                y -= 2
            else:
                va = 'top'
                y += rect.get_height() + 4
            ax.text(
                x, y - 2,
                f"{window_prediction.label} "
                f"{round(window_prediction.iou * 100)}% IoU",
                ha="left", va=va, size=10, color=color,
                bbox=bbox_props)
    if legend:
        patches = [
            mppatches.Patch(color=(0, 1, 0), label="Ground Truth"),
            mppatches.Patch(color=(0, 0, 1), label="True Positive"),
            mppatches.Patch(color=(1, 0, 0), label="False Positive")
        ]
        # put those patched as legend-handles into the legend
        plt.legend(handles=patches, bbox_to_anchor=(1.05, 1), loc=2,
                   borderaxespad=0.)
    plt.tight_layout()

    # if len(window_predictions) > 0 or len(window_groundtruths) > 0:
        # plt.show()
    plt.savefig(output, bbox_inches='tight')
    plt.close(fig)


if __name__ == "__main__":
    main(make_args())
