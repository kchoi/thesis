import itertools
import pandas as pd


def needed_data(args):
    df = pd.read_csv(args.results, low_memory=False)
    expes = [f"expe-{i}" for i in itertools.chain(
        range(10, 18), range(18, 19), range(361, 367), range(381, 386),
        range(391, 396), range(401, 406), range(411, 416), range(450, 460),
        range(460, 470), range(480, 490), range(490, 500), range(541, 547),
        range(550, 560), range(560, 570), range(590, 600), range(600, 610),
        range(610, 620), range(4900, 5000), range(620, 630), range(630, 640),
        range(640, 650), range(650, 660), range(660, 670), range(670, 680),
        range(680, 690), range(690, 700), range(700, 710))]
    df = df.query(f"expe in {expes}")
    datasets = ['AccidentalDataset', 'AccidentalDataset10',
                'AccidentalDataset20', 'Accidental200kSequence10',
                'Accidental200kSequence20', 'ChoiDataset_EarlyStopping20',
                'ChoiDataset_EarlyStopping20_NoBoot',
                'ChoiDataset_EarlyStopping10_NoBoot',
                'ChoiDataset_EarlyStopping10_Boot',
                'IMSLP00022_ChoiDatasetV3_EarlyStopping20_Boot',
                'IMSLP00022_ChoiDatasetV3_Valid',
                'IMSLP00022_ChoiDatasetV3_Test',
                'ChoiDatasetV3_Valid',
                'ChoiDatasetV3_EarlyStopping20_Boot',
                ]
    df = df.query(f"dataset in {datasets}")
    df = df[[
        'name', 'dataset', 'expe', 'run', 'train', 'epoch',
        'adv_lr', 'disc_lr', 'adv_num_train', 'disc_num_train',
        'sg_lr', 'sg_num_train', 'sg_num_train_freq', 'gen_num_train',
        'batch_size',
        'white_pixel_acc', 'black_pixel_acc',
        'BECARRE_pixel_acc', 'BEMOL_pixel_acc', 'DIESE_pixel_acc',
        'PascalBoxes_Precision/mAP@0.5IOU',
        'PascalBoxes_Precision/mAP@0.75IOU',
        "PascalBoxes_PerformanceByCategory/AP@0.75IOU/b'BECARRE'",
        "PascalBoxes_PerformanceByCategory/AP@0.75IOU/b'BEMOL'",
        "PascalBoxes_PerformanceByCategory/AP@0.75IOU/b'DIESE'",
    ]]
    if not args.output:
        print(df.expe.unique())
    else:
        df.to_csv(args.output, index=False)
