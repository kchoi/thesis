import itertools
import pandas as pd
import tikzplotlib


def check_df(df, param_columns, value_columns):
    assert not df.isnull().values.any(), df[df.isnull().any(axis=1)]
    df1 = df.groupby(by=param_columns).count()
    for value_column in value_columns:
        df2 = df1[value_column].unique()
        assert len(df2) == 1 and df2[0] == 1, df1


def preprocess(csv_file, expes, datasets, param_columns, value_column):
    if isinstance(value_column, str):
        value_columns = [value_column]
    else:
        value_columns = value_column
    df = pd.read_csv(csv_file, low_memory=False)
    expes = [f"expe-{i}" for i in itertools.chain(
        *[range(*expe_range) for expe_range in expes])]
    df = df.query(f"expe in {expes}")
    if datasets:
        df = df.query(f"dataset in {datasets}")
    df = df[[*param_columns, *value_columns]]
    check_df(df, param_columns, value_columns)
    return df


def save_tex(output, width, height=None, pre_line_callback=None,
             replace_callback=None, post_line_callback=None):
    code = tikzplotlib.get_tikz_code()
    new_code = []
    for line in code.split('\n'):
        if line.startswith('title='):
            new_code.append('title style={align=center},')
        if pre_line_callback:
            res = pre_line_callback(line)
            if res and isinstance(res, str):
                new_code.append(res)
            elif res and isinstance(res, list):
                new_code += res
        if replace_callback:
            new_code.append(replace_callback(line))
        else:
            new_code.append(line)
        if line == '\\begin{axis}[':
            new_code.append('scale only axis,')
            new_code.append(f'width=\\textwidth * {width},')
            if height:
                new_code.append(f'height=\\textwidth * {height},')
        if post_line_callback:
            res = post_line_callback(line)
            if res and isinstance(res, str):
                new_code.append(res)
            elif res and isinstance(res, list):
                new_code += res
    with open(output, 'w') as f:
        f.write('\n'.join(new_code))


def pivot_on(df, value_column, index, columns):
    df = pd.pivot_table(df, values=value_column, index=index, columns=columns)
    df = df.reset_index()
    return df


def pivot_on_dataset(df, value_column, index):
    df = pivot_on(df, value_column, index, 'dataset')
    return df


def pivot_on_name(df, value_column, index):
    df = pivot_on(df, value_column, index, 'name')
    df = df.rename(columns={
        'seg_gan_flat': 'Flat',
        'seg_gan_natural': 'Natural',
        'seg_gan_singleclass': 'Sharp',
    })
    return df


def early_stopping(df, by, idxmax_col, drop_col=None, metric=None):
    # specifying a metric means df is has multi level columns with multiple
    # metrics and for each metric, multiple datasets
    if metric:
        df1 = df[[*by, metric]]
        # flatten columns
        df1.columns = [cols[0] if cols[1] == '' else cols[1]
                       for cols in df1.columns]
    else:
        df1 = df
    # choose best epoch based on results on idxmax_col
    idxmax_epoch = df1.groupby(by=by)[idxmax_col].idxmax()
    max_epoch = df.iloc[idxmax_epoch]
    max_epoch = max_epoch.reset_index(drop=True)
    if drop_col:
        if metric:
            new_max_epoch = max_epoch
            for cols in max_epoch.columns:
                if drop_col in cols:
                    new_max_epoch = new_max_epoch.drop(columns=cols)
            max_epoch = new_max_epoch
            max_epoch.columns = [cols[0] for cols in max_epoch.columns]
        else:
            max_epoch = max_epoch.drop(columns=drop_col)
    return idxmax_epoch, max_epoch


def compute_boxplot(df, by):
    df1 = df.groupby(by=by)
    return {
        'max': df1.max(),
        '3rd_quantile': df1.quantile(0.75),
        'median': df1.median(),
        '1st_quantile': df1.quantile(0.25),
        'min': df1.min()
    }

