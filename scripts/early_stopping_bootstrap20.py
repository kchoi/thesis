from early_stopping import early_stopping_boxplot


def early_stopping_bootstrap20(args):
    early_stopping_boxplot(
        args, [[620, 630]], 'ChoiDataset_EarlyStopping20',
        r'Evaluation using Full Dataset and\\'
        'Small Validation Dataset of 20 Examples With Bootstrapping'
    )
